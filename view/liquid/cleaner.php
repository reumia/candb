<?php
    $theme = 'about-eyes cleaner';
    $title = 'Boston Advance 세척액';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">렌즈 관리 용액 ></span> <?=$title;?>
    </section>
    <section class="content-header clearfix">
        <img src="../../static/img/formula/formular_washer_top.png" alt=""/>
        <h3>
            RGP렌즈 표면의 침착물을 효과적으로 제거할 수 있는 <br class="only-pc"/>
            RGP렌즈 전용 세척액으로 완전한 세척 가능
        </h3>
        <p>
            산소가 통하는 RGP렌즈는 각막에 필요한 산소를 충분히 공급할 수 있지만, 이러한 렌즈 재질의 특성 때문에 더 많은 <br class="only-pc"/>
            이물질이 렌즈에 부착됩니다. 이때 렌즈관리가 소홀하면 세균번식 등으로 안질환이 유발될 수 있으며, 렌즈의 수명도 <br class="only-pc"/>
            짧아지게 됩니다.
        </p>
        <p>
            보스톤 세척액은 RGP 렌즈 표면의 침착물을 효과적으로 제거할 수 있는 제품으로 우수한 세척작용과 함께 마찰로<br class="only-pc"/>
            인한 렌즈 손상을 방지할 수 있도록 고안되었으며, 우윳빛 색조를 첨가하여 완전히 헹구어지지 않았을 경우 육안으로<br class="only-pc"/>
            쉽게 확인 할 수 있습니다.
        </p>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h2>세척기전</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_washer_step_1.png" alt=""/>
                    1. RGP 렌즈 재질에 포함된 실리콘은 렌즈 표면을 빨리 건조하게 하며 이렇게 노출된 렌즈의 표면은 음(-)이온을 갖게 됩니다.
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_washer_step_2.png" alt=""/>
                    2. 따라서 양(+)이온을 띤 눈물 속의 단백질이나 지방 성분과 강하게 결합하게 됩니다.
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_washer_step_3.png" alt=""/>
                    3. 보스톤 세척액은 보스톤 용액만의 독특한 연마제를 사용하여 세척 중 문지르는 과정에서 렌즈표면과 이물질의 결합을 물리적으로 깨뜨립니다.
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_washer_step_4.png" alt=""/>
                    4. 계면활성제는 이물질에 붙어 이물질과 렌즈 사이의 결합을 약하게 하며, 이때 물(H2O)이 계면활성제를 둘러싸서 이물질을 렌즈표면으로부터 떨어지게 하고 이물질이 렌즈표면에 다시 결합되는 것을 방지합니다.
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>상세정보</h2>
            <table class="table table-none-width">
                <tbody>
                <tr>
                    <th>적응 Indications</th>
                    <td>플루오로실리콘 아크릴레이트 및 실리콘 아크릴레이트 소재의 RGP 렌즈의 세척</td>
                </tr>
                <tr>
                    <th>효능 Action</th>
                    <td>RGP렌즈 표면의 이물질 및 단백질과 지방과 같은 묵은 때의 제거</td>
                </tr>
                <tr>
                    <th>성분 Ingredients</th>
                    <td>주성분 : 실리카, 트리데실에테르황산나트륨 30% 용액</td>
                </tr>
                <tr>
                    <th>용량 Volume</th>
                    <td>30ml. bottle</td>
                </tr>
                </tbody>
            </table>
        </section>
        <section class="content-section">
            <div class="row">
                <div class="col col-xs-6">
                    <h3>
                        제품에 대해 더 궁금한<br class="only-pc"/>
                        사항이 있습니까?
                    </h3>
                    <div class="btn-square">
                        <a href="">
                            <img src="../../static/img/formula/formular_washer_question.png" alt=""/>
                            질문과 답변
                        </a>
                    </div>
                </div>
                <div class="col col-xs-6">
                    <h3>
                        제품 구매를 위해 좀 더<br class="only-pc"/>
                        자세히 보시겠습니까?
                    </h3>
                    <div class="btn-square">
                        <a href="">
                            <img src="../../static/img/formula/formular_washer_shopping.png" alt=""/>
                            쇼핑몰로 가기
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>다른 렌즈관리용액에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="soaking-solution.php">
                    <img src="../../static/img/formula/formular_preserve_small.png" alt=""/>
                    보존액
                </a>
            </li>
            <li class="btn-square">
                <a href="multi-function.php">
                    <img src="../../static/img/formula/formular_simplus_small.png" alt=""/>
                    다목적 용액
                </a>
            </li>
            <li class="btn-square">
                <a href="protein-removal.php">
                    <img src="../../static/img/formula/formular_protain_small.png" alt=""/>
                    단백질 제거제
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>