<?php
    $theme = "liquid"; //궁금한 우리 눈과 일단은 같은 색상테마로 디자인되어 있음.
    $title = "렌즈 관리 용액";
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <?=$title;?>
    </section>
    <section class="content-header clearfix">
        <img src="../../static/img/formula/formular_1st_top.png" alt=""/>
        <h1><?=$title?></h1>
        <h3>
            보스톤 관리용액(Boston care system)은 미국 내 90% 이상 시장점유율을<br class="only-pc"/>
            가지고 있는 전세계적으로 가장 널리 쓰이고 있는 RGP Lens 전용 관리용액으로<br class="only-pc"/>
            RGP Lens 관리에 있어서 가장 탁월하고 뛰어난 제품입니다.
        </h3>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h2>보스톤 케어<br class="only-pc"/> 시스템</h2>
            <h3>RGP렌즈에 최적화된 관리 용액</h3>
            <p>
                특수재질의 산소투과성 하드렌즈(RGP Lens)는 편안한 착용감과 최상의 성능유지를 위해 특별한 관리가<br class="only-pc"/>
                필요합니다. 4종의 Boston Care System은 RGP Lens관리에 탁월한 효과를 발휘하도록 개발되었습니다.
            </p>
        </section>
        <section class="content-section">
            <ul class="list-component">
                <li>
                    <div class="list-component-img">
                        <img src="../../static/img/formula/formular_washer_big.png" alt=""/>
                    </div>
                    <div class="list-component-body">
                        <h3 class="list-component-title color-default">Boston Advance 세척액</h3>
                        RGP렌즈의 습윤성을 향상시키며 표면의 건조를 방지하여 편안한 착용감을
                        장시간 유지시켜주는 RGP 렌즈 전용 보존액입니다. 독특한 살균 시스템과 렌즈의
                        습윤성을 향상시키기 위한 코딩성분을 포함하고 있어서 렌즈 건조를 방지하고
                        장시간 습윤성이 유지되도록 해줍니다.
                    </div>
                    <a class="list-component-btn" href="cleaner.php">더 알아보기</a>
                </li>
                <li>
                    <div class="list-component-img">
                        <img src="../../static/img/formula/formular_preserve_big.png" alt=""/>
                    </div>
                    <div class="list-component-body">
                        <h3 class="list-component-title color-default">Boston Advance 보존액</h3>
                        RGP렌즈 표면의 침착물을 효과적으로 제거할 수 있는 RGP렌즈 전용
                        세척액입니다. 우수한 세척작용과 함께 마찰로 인한 렌즈 손상을 방지할 수
                        있도록 고안되었습니다.
                    </div>
                    <a class="list-component-btn" href="soaking-solution.php">더 알아보기</a>
                </li>
                <li>
                    <div class="list-component-img">
                        <img src="../../static/img/formula/formular_simplus_big.png" alt=""/>
                    </div>
                    <div class="list-component-body">
                        <h3 class="list-component-title color-default">Boston Advance 다기능 용액</h3>
                        단백질 제거기능을 포함, 렌즈관리에 필요한 다양한 기능을 갖추어 현대인의
                        생활방식에 적합한 렌즈관리용액입니다. 세척, 헹굼, 소독, 보존뿐만 아니라
                        단백질제거까지 매일 동시에 할 수 있는 최초이자 유일한 RGP렌즈
                        관리용액입니다.
                    </div>
                    <a class="list-component-btn" href="multi-function.php">더 알아보기</a>
                </li>
                <li>
                    <div class="list-component-img">
                        <img src="../../static/img/formula/formular_protain_big.png" alt="" style="margin-top: -55px;"/>
                    </div>
                    <div class="list-component-body">
                        <h3 class="list-component-title color-default">Boston 단백질 제거액</h3>
                        RGP 렌즈만을 위한 최초의 전문 효소 세척용액으로 미국 FDA의 공인을 받은
                        유일한 액상 단백질 제거제입니다. 일주일에 한 번 사용으로도 렌즈에 부탁된 효소
                        성분을 손쉽게 말끔히 제거할 수 있도록 고안 되었습니다.
                    </div>
                    <a class="list-component-btn" href="protein-removal.php">더 알아보기</a>
                </li>
            </ul>
        </section>
        <section class="content-section">
            <h2>렌즈관리용액<br class="only-pc"/> 바르게<br class="only-pc"/> 사용하기</h2>
            <h3>렌즈관리 용액의 사용법에 대해서</h3>
            <p>
                올바른 렌즈관리는 눈의 건강과 렌즈의 성능유지 및 수명관리를 위해 필수적입니다.<br class="only-pc"/> 용액 사용법을 정확히 알고 자신의 라이프 스타일에 맞는 관리 방법을 선택하여 편리하고 효과적으로 렌즈를 관리합니다.
            </p>
        </section>
        <section class="content-section">
            <h3 class="color-default">세척액 / 보존액 사용법</h3>
            <div class="image-wrap">
                <iframe width="748" height="400" src="https://www.youtube.com/embed/3a8fsxaZtW8?rel=0&vq=hd720&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
            </div>
        </section>
        <section class="content-section">
            <h3 class="color-default">심플러스 사용법</h3>
            <div class="image-wrap">
                <iframe width="748" height="400" src="https://www.youtube.com/embed/NVhdz8iFgXQ?rel=0&vq=hd720&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
            </div>
        </section>
    </section>
</article>
<?php
    include_once '../inc/footer.php';
?>