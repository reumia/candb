<?php
    $theme = 'about-eyes multi';
    $title = 'Boston Simplus 다목적용액';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">렌즈 관리 용액 ></span> <?=$title;?>
    </section>
    <section class="content-header clearfix">
        <img src="../../static/img/formula/formular_simplus_top.png" alt=""/>
        <h3>
            단백질 제거기능을 포함, 렌즈관리에 필요한 다양한<br class="only-pc"/>
            기능을 갖추어 현대인의 생활방식에 적합한 렌즈관리용액
        </h3>
        <p>
            보스톤 심플러스 다기능 용액은 세척, 헹굼, 소독, 보존뿐만 아니라 단백질제거까지 <br class="only-pc"/>
            매일 동시에 할 수 있는 최초이자 유일한 RGP렌즈 관리용액입니다. <br/>
            강력한 세척력으로 저녁에 문질러 닦을 필요 없이 용액에 담가두기만 하면 되므로 <br class="only-pc"/>
            렌즈표면의 손상을 막아주어 렌즈의 수명을 연장시키며, 또한 분실이나 파손의 위험도 적어집니다. <br/>
            이중의 소독시스템으로 눈의 건강과 안전함을 제공하며, 탁월한 습윤효과와 완충작용으로 렌즈 착용을 보다 편안하게 <br class="only-pc"/>
            해줍니다. 또한, 쉽고 간편하게 렌즈관리를 할 수 있어 바쁜 현대인의 생활방식에 가장 적합한 제품입니다.
        </p>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h2>세척효과</h2>
            <h3>폴록사민은 친수성과 소수성의 성질을 모두 포함하고 있습니다. <br/>
                이 상반되는 성질이 렌즈 표면의 이물질을 깨끗하게 제거하여 줍니다.</h3>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_simplus_step_1.png" alt=""/>
                    1. 폴록사민은 친수성과 소수성의 성질을 모두 포함하고 있습니다.
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_simplus_step_2.png" alt=""/>
                    2. 폴록사민은 물을 싫어하는 성질(소수성) 부분으로 인해 렌즈표면의 이물질에 달라붙게 됩니다.
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_simplus_step_3.png" alt=""/>
                    3. 물을 좋아하는 성질(친수성) 부분은 물 쪽으로 이물질을 끌어당기게 됩니다.
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_simplus_step_4.png" alt=""/>
                    4. 이물질이 위쪽 방향으로 끌려감에 따라 더욱 더 많은 폴록사민이 달라붙어 이물질은 완전히 떨어져 나갑니다. 이물질은 떨어져 나가고 친수성 입자에 둘러 싸여 헹구어집니다.
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>
                단백질 <br class="only-pac"/>
                제거 기능
            </h2>
            <h3>4개의 음이온으로 되어있는 HAP가 렌즈 표면으로부터 단백질을 제거하여줍니다.</h3>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_simplus_step_5.png" alt=""/>
            ￼￼￼￼1. 단백질 침착물은 양이온과 음이온의 성질을 모두 가지고 있습니다. 칼슘은 2개의 양이온을 가지고 있습니다.
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_simplus_step_6.png" alt=""/>
                    2. 단백질은 렌즈와 다른 단백질에 서로 달라붙고 칼슘은 이를 단단히 연결해 주는 다리 역할을 합니다.
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_simplus_step_7.png" alt=""/>
                    3. 4개의 음이온으로 되어있는 HAP는 이물질들을 연결해주는 다리의 역할을 하는 칼슘을 먼저 분리해 냅니다.
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_simplus_step_8.png" alt=""/>
                    4. HAP는 단백질에 달라붙어 반발력을 통하여 렌즈 표면으로부터 단백질을 완전히 분리해 냅니다.
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>상세정보</h2>
            <table class="table table-none-width">
                <tbody>
                <tr>
                    <th>적응 Indications</th>
                    <td>RGP렌즈의 세척, 헹굼, 소독, 보존, 단백질제거를 해 줍니다.</td>
                </tr>
                <tr>
                    <th>효능 Action</th>
                    <td>
                        심플러스만의 독특한 성분인 "Poloxamine"은 강력한 세척력으로 문지를 필요 없이 <br class="only-pc"/>
                        렌즈의 이물질 및 침착물을 제거해 주고 "HAP(Hydroxyalkyl phosphonate)"성분은 단백질 <br class="only-pc"/>
                        제거의 기능을 가지고 있습니다.
                    </td>
                </tr>
                <tr>
                    <th>성분 Ingredients</th>
                    <td>주성분: 에티드론산테트라나트륨30%용액, 20%염산폴리헥사메칠렌비구아니드, <br class="only-pc"/>글루콘산클로르헥시딘액</td>
                </tr>
                <tr>
                    <th>용량 Volume</th>
                    <td>120ml. bottle</td>
                </tr>
                </tbody>
            </table>
        </section>
        <section class="content-section">
            <div class="row">
                <div class="col col-xs-6">
                    <h3>
                        제품에 대해 더 궁금한 <br class="only-pc"/>
                        사항이 있습니까?
                    </h3>
                    <div class="btn-square">
                        <a href="">
                            <img src="../../static/img/formula/formular_simplus_question.png" alt=""/>
                            질문과 답변
                        </a>
                    </div>
                </div>
                <div class="col col-xs-6">
                    <h3>
                        제품 구매를 위해 좀 더 <br class="only-pc"/>
                        자세히 보시겠습니까?
                    </h3>
                    <div class="btn-square">
                        <a href="">
                            <img src="../../static/img/formula/formular_simplus_shopping.png" alt=""/>
                            쇼핑몰로 가기
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>다른 렌즈관리용액에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="cleaner.php">
                    <img src="../../static/img/formula/formular_washer_small.png" alt=""/>
                    세척액
                </a>
            </li>
            <li class="btn-square">
                <a href="soaking-solution.php">
                    <img src="../../static/img/formula/formular_preserve_small.png" alt=""/>
                    보존액
                </a>
            </li>
            <li class="btn-square">
                <a href="protein-removal.php">
                    <img src="../../static/img/formula/formular_protain_small.png" alt=""/>
                    단백질 제거제
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>