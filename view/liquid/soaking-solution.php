<?php
    $theme = 'about-eyes soaking';
    $title = 'Boston Advance 보존액';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">렌즈 관리 용액 ></span> <?=$title;?>
    </section>
    <section class="content-header clearfix">
        <img src="../../static/img/formula/formular_preserve_top.png" alt=""/>
        <h3>
            RGP렌즈의 습윤성을 향상시키며 표면의 건조를 방지하여 <br class="only-pc"/>
            편안한 착용감을 장시간 유지
        </h3>
        <p>
            보스톤 보존액은 독특한 살균시스템과 렌즈의 습윤성을 향상시키기 위해 렌즈를 촉촉하게 하는 코팅성분을 함유하고 <br class="only-pc"/>
            있어서 렌즈 건조를 방지하고, 장시간 습윤성이 유지되도록 해줍니다. <br/>
            또한 각막과 렌즈의 마찰을 줄여주어서 장시간 편안하고 부드러운 렌즈의 착용을 도와드립니다. 세척 후 소독과 보존의<br class="only-pc"/>
            역할을 함께 하기 때문에 렌즈 표면의 해로운 미생물까지도 완벽하게 제거해줍니다.
        </p>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h2>세척기전</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_preserve_step_1.png" alt=""/>
                    1. 보스톤 보존액에는 타사 제품과는 달리 쉽게 완충막(- Cushioning Layer)을 형성하여 RGP 렌즈 표면을 보존하는 독특한 성분이 포함되어 있습니다.
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_preserve_step_2.png" alt=""/>
                    2. 용액 속의 친수성 중합체(hydrophillic polymer)는 특허물질로 RGP렌즈 표면과 정전기적으로 결합하게 됩니다.
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_preserve_step_3.png" alt=""/>
                    3. 그 결과 렌즈표면의 습윤성이 향상되어 눈을 뜨고 있는 동안에도 지속적으로 습윤성을 유지시켜 줍니다. 눈물로 된 점액층이 렌즈를 덮어 렌즈 표면의 습윤성을 유지하도록 해주고 렌즈표면이 건조해 지는 것을 감소시켜주며, 이물질의 배출을 도와줍니다.
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/formula/formula_preserve_step_4.png" alt=""/>
                    4. 보스톤 보존액에 의해 형성되는 렌즈표면 위의 친수성막은 눈물 수분층 형성을 촉진하며 아울러 친수성이 향상시켜 렌즈의 착용시간을 늘려주고 착용감을 편안하게 해줍니다.
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>상세정보</h2>
            <table class="table table-none-width">
                <tbody>
                <tr>
                    <th>적응 Indications</th>
                    <td>RGP 렌즈의 세척, 헹굼 후의 소독 및 보존</td>
                </tr>
                <tr>
                    <th>효능 Action</th>
                    <td>RGP렌즈의 습윤성을 향상시키며, 렌즈와 각막사이의 마찰을 감소시켜 자극감과 <br class="only-pc"/>
                        불편함을 없게 합니다. 또한, 렌즈와 렌즈 케이스에 있는 세균을 괴사시키는 소독자용이 있습니다.</td>
                </tr>
                <tr>
                    <th>성분 Ingredients</th>
                    <td>주성분 : 글루콘산클로르헥시딘액, 20%염산폴리헥사메칠렌비구아니드</td>
                </tr>
                <tr>
                    <th>용량 Volume</th>
                    <td>120ml. bottle</td>
                </tr>
                </tbody>
            </table>
        </section>
        <section class="content-section">
            <div class="row">
                <div class="col col-xs-6">
                    <h3>
                        제품에 대해 더 궁금한 <br class="only-pc"/>
                        사항이 있습니까?
                    </h3>
                    <div class="btn-square">
                        <a href="">
                            <img src="../../static/img/formula/formular_preserve_question.png" alt=""/>
                            질문과 답변
                        </a>
                    </div>
                </div>
                <div class="col col-xs-6">
                    <h3>
                        제품 구매를 위해 좀 더<br class="only-pc"/>
                        자세히 보시겠습니까?
                    </h3>
                    <div class="btn-square">
                        <a href="">
                            <img src="../../static/img/formula/formular_preserve_shopping.png" alt=""/>
                            쇼핑몰로 가기
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>다른 렌즈관리용액에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="cleaner.php">
                    <img src="../../static/img/formula/formular_washer_small.png" alt=""/>
                    세척액
                </a>
            </li>
            <li class="btn-square">
                <a href="multi-function.php">
                    <img src="../../static/img/formula/formular_simplus_small.png" alt=""/>
                    다목적 용액
                </a>
            </li>
            <li class="btn-square">
                <a href="protein-removal.php">
                    <img src="../../static/img/formula/formular_protain_small.png" alt=""/>
                    단백질 제거제
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>