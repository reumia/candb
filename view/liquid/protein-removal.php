<?php
    $theme = 'about-eyes protein';
    $title = 'Boston One Step 단백질 제거제';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">렌즈 관리 용액 ></span> <?=$title;?>
    </section>
    <section class="content-header clearfix">
        <img src="../../static/img/formula/formular_protain_top.png" alt=""/>
        <h3>
            RGP 렌즈만을 위한 최초의 전문 효소 세척용액으로 <br class="only-pc"/>
            미국 FDA의 공인을 받은 유일한 액상 단백질 제거제
        </h3>
        <p>
            보스톤 단백질 제거액은 보스톤 용액과 함께 사용할 수 있는 RGP 렌즈만을 위한 최초의 전문효소 세척용액으로 <br class="only-pc"/>
            일주일 한 번 사용으로도 렌즈에 부탁된 효소 성분을 손쉽게 말끔히 제거할 수 있도록 고안 되었습니다. <br/>
            이 제품은 미국 FDA로 부터 승인 받은 유일한 액상 단백질제거용 세척용액으로 투명하고 냄새가 없으며 Smart Drop <br class="only-pc"/>
            용기를 사용하여 기존 정제 단백질 제거제에 비해 보다 경제적이며 효과적으로 정확한 용량을 사용하실 수 있습니다.
        </p>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h2>특징</h2>
            <h3>
                ONE STEP Liquid Enzymatic Cleaner는 Boston렌즈 용액과 함께 사용할 수
                있으며 RGP렌즈만을 위한 최초의 전문효소 세척용액입니다. <br/>
                렌즈 소독과 단백질제거는 소독용기나 효소알약을 따로 준비할 필요 없이, 하나의
                렌즈용기에서 바로 하실 수 있습니다.
            </h3>
        </section>
        <section class="content-section">
            <h2>
                사용 방법
            </h2>
            <div class="row">
                <div class="col col-xs-6">
                    <strong>1. 소독, 보존</strong>
                    세척,헹굼 과정 후 렌즈케이스에 Conditioning Solution(혹은 SIMPLUS)을 반정도 채웁니다.
                </div>
                <div class="col col-xs-6">
                    <strong>2. 단백질 제거</strong>
                    Enzymatic Cleaner를 각 2방울씩 떨어뜨린 후 4시간 이상(혹은 밤새) 담가둡니다.
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <strong>3. 헹굼</strong>
                    렌즈를 손바닥에 올려 놓고 Conditioning Solution(혹은 SIMPLUS)을 2~3방울 떨어뜨려 손가락으로 5회정도 가볍게 문질러 헹구어 냅니다.
                </div>
                <div class="col col-xs-6">
                    <strong>4. 착용</strong>
                    착용 전 Conditioning Solution(혹은 SIMPLUS)으로 렌즈 안쪽에 한방울 떨어뜨린 후 착용합니다.
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>상세정보</h2>
            <table class="table table-none-width">
                <tbody>
                <tr>
                    <th>적응 Indications</th>
                    <td>
                        Boston ONE STEP LEC는 일주일에 한번씩만 사용하면 렌즈에 묻은 단백질이 말끔히<br class="only-pc"/>
                        제거되도록 고안된, 사용이 아주 간편한 단백질제거 세척용액 입니다.
                    </td>
                </tr>
                <tr>
                    <th>효능 Action</th>
                    <td>
                        미국 FDA가 공인한 유일한 액성 단백질제거 효소용액으로, 투명하며 냄새가 없습니다. <br/>
                        기존의 정제 단백질효소제에 비해 보다 경제적이며 효과적입니다.
                    </td>
                </tr>
                <tr>
                    <th>성분 Ingredients</th>
                    <td>
                        A sterile, aqueous, solution containg proteolytic enzyme (subtilisin) as <br class="only-pc"/>
                        the active ingredient, and glycerol. Preservative-free.
                    </td>
                </tr>
                <tr>
                    <th>용량 Volume</th>
                    <td>5ml. bottle</td>
                </tr>
                </tbody>
            </table>
        </section>
        <section class="content-section">
            <div class="row">
                <div class="col col-xs-6">
                    <h3>제품에 대해 더 궁금한 사항이 있습니까?</h3>
                    <div class="btn-square">
                        <a href="">
                            <img src="../../static/img/formula/formular_protain_question.png" alt=""/>
                            질문과 답변
                        </a>
                    </div>
                </div>
                <div class="col col-xs-6">
                    <h3>제품 구매를 위해 좀 더 자세히 보시겠습니까?</h3>
                    <div class="btn-square">
                        <a href="">
                            <img src="../../static/img/formula/formular_protain_shopping.png" alt=""/>
                            쇼핑몰로 가기
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>다른 렌즈관리용액에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="cleaner.php">
                    <img src="../../static/img/formula/formular_washer_small.png" alt=""/>
                    세척액
                </a>
            </li>
            <li class="btn-square">
                <a href="soaking-solution.php">
                    <img src="../../static/img/formula/formular_preserve_small.png" alt=""/>
                    보존액
                </a>
            </li>
            <li class="btn-square">
                <a href="multi-function.php">
                    <img src="../../static/img/formula/formular_simplus_small.png" alt=""/>
                    다목적 용액
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>