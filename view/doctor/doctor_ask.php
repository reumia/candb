<?php
$theme = 'other';
$title = '의사전용페이지';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
        include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>안과의사 <br class="only-pc"/> 전용상담</h2>
                <h3 class="color-default">
                    <strong class="color-theme">홍길동</strong>님 무엇이 궁금하세요? <br/>
                    상담내용을 등록해 주시면, 이메일이나 입력해 주신 연락처로 연락을 드립니다. <br/>
                    정확하게 입력해 주세요.
                </h3>
            </section>
            <section class="content-section">
                <h2>상담 <br class="only-pc"/> 등록하기</h2>
                <form action="">
                    <table class="table-form" style="width: 100%;">
                        <tbody>
                        <tr>
                            <th class="color-theme">연락처</th>
                            <td><input class="form-control form-m" type="text"/></td>
                            <th class="color-theme">이메일</th>
                            <td><input class="form-control form-m" type="email"/></td>
                        </tr>
                        <tr>
                            <th class="color-theme">첨부파일1</th>
                            <td colspan="3">
                                <input type="file"/>
                            </td>
                        </tr>
                        <tr>
                            <th class="color-theme">첨부파일2</th>
                            <td colspan="3">
                                <input type="file"/>
                            </td>
                        </tr>
                        <tr>
                            <th class="color-theme">첨부파일3</th>
                            <td colspan="3">
                                <input type="file"/>
                            </td>
                        </tr>
                        <tr>
                            <th class="color-theme">첨부파일4</th>
                            <td colspan="3">
                                <input type="file"/>
                            </td>
                        </tr>
                        <tr>
                            <th class="color-theme">제목</th>
                            <td colspan="3">
                                <input class="form-control" type="text" style="width: 573px;"/>
                            </td>
                        </tr>
                        <tr>
                            <th>&nbsp;</th>
                            <td colspan="3">
                                <textarea class="form-control" name="" id="" cols="30" rows="10" style="width: 573px;"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th>&nbsp;</th>
                            <td colspan="3">
                                <button class="btn btn-default" type="submit">등록하기</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>