<section class="content-breadcrumb">
    <?=$title;?>
</section>
<section class="content-header content-header--sub clearfix">
    <h1><?=$title;?></h1>
    <ul class="tabs">
        <li class="tab"><a href="doctor_notice.php">C&amp;B에서 알립니다</a></li>
        <li class="tab"><a href="doctor_picture.php">사진자료실</a></li>
        <li class="tab"><a href="doctor_ask.php">안과의사전용상담</a></li>
    </ul>
</section>