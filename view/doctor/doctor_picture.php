<?php
$theme = 'other';
$title = '의사전용페이지';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
        include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    사진자료실
                </h2>
                <div class="board">
                    <table class="board-table">
                        <thead>
                        <tr>
                            <th class="color-theme">번호</th>
                            <th class="color-theme">사진</th>
                            <th class="color-theme">제목</th>
                            <th class="color-theme">작성자</th>
                            <th class="color-theme">날짜</th>
                            <th class="color-theme">조회</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        for( $i = 10; $i > 0; $i-- ){
                            ?>
                            <tr>
                                <td><?=$i;?></td>
                                <td>
                                    <!-- 이미지 샘플 : 아무 사이즈 넣어도 80 * 80 사이즈로 스타일링 되어 있습니다. -->
                                    <img src="../../static/img/lens/lens_achievement_ill1.png" alt=""/>
                                </td>
                                <td><a href="#">테스트입니다. 테스트입니다.</a></td>
                                <td>홍길동</td>
                                <td>0000.00.00</td>
                                <td>00</td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <ul class="board-pagination">
                        <li><a href="#">&lt;</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">&gt;</a></li>
                    </ul>
                    <hr/>
                    <div class="row">
                        <div class="col col-xs-6">
                            &nbsp;
                        </div>
                        <div class="col col-xs-6">
                            <div class="board-search text-right">
                                <select class="form-control" name="" id=""></select>
                                <input class="form-control" type="text"/>
                                <button class="btn btn-default">검색</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>