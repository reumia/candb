<?php
    $theme = 'main';
    $title = "Clean & Bright";
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-body main-box-wrap first">
        <article class="main-box only-pc">
            <h1 class="color-white">Best Shopping</h1>
            <a class="main-box__header" href="#">
                <img src="../../static/img/index/main_banner00.png" alt=""/>
            </a>
            <div class="main-box__content">
                본문내용요약 <br/>
                세 줄 정도만 나올 수 있도록 글자수 제한 <br/>
                세 줄 정도만 나올 수 있도록 글자수 제한
            </div>
            <div class="btn-wrap text-right">
                <a class="btn btn-default btn-xs" href="#">더보기</a>
            </div>
        </article>
        <article class="main-box only-pc" style="width: 410px;">
            <h1>What's New</h1>
            <table class="table">
                <tbody>
                <tr>
                    <td>2015.06.22</td>
                    <td><a href="#">제13회 Assistant Seminar 성황리에 개최</a></td>
                </tr>
                <tr>
                    <td>2015.06.22</td>
                    <td><a href="#">제13회 Assistant Seminar 성황리에 개최</a></td>
                </tr>
                <tr>
                    <td>2015.06.22</td>
                    <td><a href="#">제13회 Assistant Seminar 성황리에 개최</a></td>
                </tr>
                <tr>
                    <td>2015.06.22</td>
                    <td><a href="#">제13회 Assistant Seminar 성황리에 개최</a></td>
                </tr>
                <tr>
                    <td>2015.06.22</td>
                    <td><a href="#">제13회 Assistant Seminar 성황리에 개최</a></td>
                </tr>
                </tbody>
            </table>
            <div class="btn-wrap text-right">
                <a class="btn btn-default btn-xs" href="../cscenter/notice.php">더보기</a>
            </div>
        </article>
        <article class="main-box just-grid only-pc">
            <a class="btn btn-default btn-block" href="../other/search_rgp.php">
                <span class="icon-wrap">
                    <img src="../../static/img/index/main_subicon_1.png" alt=""/>
                </span>
                안과 검색
            </a>
            <a class="btn btn-default btn-block" href="../other/search_ok.php">
                <span class="icon-wrap">
                    <img src="../../static/img/index/main_subicon_2.png" alt=""/>
                </span>
                OK Lens 전문 시술 클리닉
            </a>
            <a class="btn btn-default btn-block" href="../doctor">
                <span class="icon-wrap">
                    <img src="../../static/img/index/main_subicon_3.png" alt=""/>
                </span>
                의사 전용 페이지
            </a>
            <a class="btn btn-default btn-block" href="../other/order.php">
                <span class="icon-wrap">
                    <img src="../../static/img/index/main_subicon_4.png" alt=""/>
                </span>
                수주장 페이지
            </a>
        </article>
    </section>
    <section class="content-body main-box-wrap second">
        <article class="main-box text-center">
            <a class="main-box__header" href="../lens-intro/contex.php">
                <img src="../../static/img/index/main_banner_0.png" alt=""/>
            </a>
            <a href="../other/search_rgp.php">
                교정렌즈의 효시, <br/>
                <strong>수면착용 시력교정렌즈</strong>
            </a>
        </article>
        <article class="main-box text-center">
            <a class="main-box__header" href="../lens-intro/kerasoft.php">
                <img src="../../static/img/index/main_banner_1.png" alt=""/>
            </a>
            <a href="../other/search_rgp.php">
                근시 및 원시, 난시의 해결을 위한 <br/>
                <strong>특수 맞춤형 소프트콘택트렌즈</strong>
            </a>
        </article>
        <article class="main-box text-center">
            <a class="main-box__header" href="../lens-intro/rgp_soclear.php">
                <img src="../../static/img/index/main_banner_2.png" alt=""/>
            </a>
            <a href="../other/search_rgp.php">
                편안한 착용감, 우수한 시력 교정 <br/>
                <strong>국내 최초의 각공막 렌즈</strong>
            </a>
        </article>
        <article class="main-box just-grid" style="width: 90px;">
            <a class="main-box__square" href="https://www.facebook.com/candbcorporation" target="_blank">
                <img src="../../static/img/index/main_banner_3.png" alt=""/>
            </a>
            <a class="main-box__square" href="#" target="_blank">
                <img src="../../static/img/index/main_banner_4.png" alt=""/>
            </a>
        </article>
    </section>
</article>
<?php
    include_once '../inc/footer.php';
?>