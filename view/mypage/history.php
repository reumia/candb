<?php
$theme = 'mypage';
$title = '마이 페이지';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
        include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    나의 쇼핑내역
                </h2>
                <h3 class="color-default">
                    <span class="color-theme">홍길동</span>님께서 C&amp;B 사이트를 통해 구매하신 내역입니다.
                </h3>
                <br/>
                <div class="table-wrap">
                    <table class="board-table">
                        <thead>
                        <tr>
                            <th class="color-theme">주문번호</th>
                            <th class="color-theme">주문일시</th>
                            <th class="color-theme">제품명</th>
                            <th class="color-theme">결제방법</th>
                            <th class="color-theme">결제금액</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        for ( $i=4; $i > 0; $i-- ){
                            ?>
                            <tr>
                                <td>320015004</td>
                                <td>0000.00.00</td>
                                <td>심플러스 용액 세트</td>
                                <td>신용카드</td>
                                <td>10,000 원</td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </section>
            <section class="content-section">
                <div class="row">
                    <div class="col col-xs-6">
                        <h3>배송확인</h3>
                        배송확인은 <strong>쇼핑몰 > 구매내역/배송조회</strong>에서 확인하실 수 있습니다.
                    </div>
                    <div class="col col-xs-6">
                        <h3>주문내용 취소/변경</h3>
                        <strong>쇼핑몰 > 구매내역/배송조회</strong>에서 취소/변경하실 수 있습니다.
                    </div>
                </div>
                <div class="row">
                    <div class="col col-xs-6">
                        <h3>온라인송금</h3>
                        제품대금을 온라인으로 입금하실 경우, 입금이 확인된 이후에 배송을 해 드립니다.
                    </div>
                    <div class="col col-xs-6">
                        <h3>신용카드</h3>
                        가장 안전한 SSL보안을 통하여 결제가 이루어지며 24시간 이내에 취소가 가능합니다.
                    </div>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>