<?php
$theme = 'mypage';
$title = '마이 페이지';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
        include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    나의<br class="only-pc"/>
                    상담내역
                </h2>
                <h3 class="color-default">
                    조현석님께서 <strong class="color-theme">고객센터 > 고객상담</strong> 을 통해 상담하신 내역입니다.
                </h3>
                <div class="board">
                    <table class="board-table">
                        <thead>
                        <tr>
                            <th class="color-theme">번호</th>
                            <th class="color-theme">제목</th>
                            <th class="color-theme">작성자</th>
                            <th class="color-theme">날짜</th>
                            <th class="color-theme">처리구분</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        for( $i = 10; $i > 0; $i-- ){
                            ?>
                            <tr>
                                <td><?=$i;?></td>
                                <td><a href="#">테스트입니다. 테스트입니다.</a></td>
                                <td>홍길동</td>
                                <td>0000.00.00</td>
                                <td></td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <ul class="board-pagination">
                        <li><a href="#">&lt;</a></li>
                        <li><a href="#" class="active">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">&gt;</a></li>
                    </ul>
                    <hr/>
                    <div class="row">
                        <div class="col col-xs-6">
                            <div class="board-btns">
                                <a class="btn btn-default" href="#">질문하기</a>
                            </div>
                        </div>
                        <div class="col col-xs-6">
                            <div class="board-search text-right">
                                <select class="form-control" name="" id=""></select>
                                <input class="form-control" type="text"/>
                                <button class="btn btn-default">검색</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>