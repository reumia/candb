<?php
$theme = 'mypage';
$title = '마이 페이지';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
        include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <!-- form 시작 -->
            <form action="">
                <section class="content-section">
                    <h2>회원 탈퇴</h2>
                    <p>
                        회원께서는 ID를 변경하시고자 할 경우, 혹은 서비스 이용 중지의 의사를 가진 경우 ID를 해지하실 수 있습니다. <br/>
                        해지하시는 경우 이메일 및 홈페이지 등 기존에 활용하시던 모든 서비스와 정보를 더 이상 이용하실 수 없습니다.<br/>
                        <strong class="color-theme">회원 탈퇴에 의해 해지된 ID 및 적립금은 재사용이 불가능하므로 신중하게 생각하시고 탈퇴 신청을 해 주시기 바랍니다.</strong>
                    </p>
                    <div class="table-wrap">
                        <table class="table-form">
                            <tbody>
                            <tr>
                                <th>아이디</th>
                                <td>
                                    <input type="text" class="form-control form-m">
                                </td>
                            </tr>
                            <tr>
                                <th>비밀번호</th>
                                <td>
                                    <input type="password" class="form-control form-m">
                                </td>
                            </tr>
                            <tr>
                                <th>E-mail</th>
                                <td>
                                    <input class="form-control form-m" type="email"/>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="btn-wrap">
                        <button class="btn btn-danger" type="submit">회원 탈퇴</button>
                    </div>
                </section>
            </form>
            <!-- form 끝 -->
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>