<?php
$theme = 'mypage';
$title = '마이 페이지';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
        include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <!-- form 시작 -->
            <form action="">
                <section class="content-section">
                    <h2>기본 정보</h2>
                    <h3>모든 항목이 필수항목입니다. 빠짐없이 입력해 주세요.</h3>
                    <table class="table-form">
                        <tbody>
                        <tr>
                            <th>아이디</th>
                            <td>
                                <input type="text" class="form-control form-m">
                                <button class="btn btn-default">중복확인</button>
                                4~10자의 영문 및 숫자로 써주세요.
                            </td>
                        </tr>
                        <tr>
                            <th>비밀번호</th>
                            <td>
                                <input type="password" class="form-control form-m">
                                4자 이상으로 써 주세요.
                            </td>
                        </tr>
                        <tr>
                            <th>비밀번호 확인</th>
                            <td><input type="password" class="form-control form-m"/></td>
                        </tr>
                        <tr>
                            <th>이름</th>
                            <td>
                                <input class="form-control form-m" type="text"/>
                            </td>
                        </tr>
                        <tr>
                            <th>전화번호</th>
                            <td>
                                <input class="form-control form-s" type="text"/>
                                <input class="form-control form-s" type="text"/>
                                <input class="form-control form-s" type="text"/>
                                &nbsp;&nbsp;
                                <label for="phone_chk">
                                    <input type="radio" name="phone_chk"/> 자택
                                </label>
                                &nbsp;
                                <label for="phone_chk">
                                    <input type="radio" name="phone_chk"/> 회사
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th>휴대전화</th>
                            <td>
                                <input class="form-control form-s" type="text"/>
                                <input class="form-control form-s" type="text"/>
                                <input class="form-control form-s" type="text"/>
                            </td>
                        </tr>
                        <tr>
                            <th>주소</th>
                            <td>
                                <input class="form-control form-s" type="text" readonly/>
                                <input class="form-control form-l" type="text" readonly/>
                                <button class="btn btn-default">우편번호 찾기</button>
                            </td>
                        </tr>
                        <tr>
                            <th>상세 주소</th>
                            <td><input class="form-control form-xl" type="text" /></td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td>
                                <input class="form-control form-m" type="email"/>
                                예) candb@candb.co.kr
                            </td>
                        </tr>
                        <tr>
                            <th>&nbsp;</th>
                            <td>한메일(hanmail.net)은 메일전달이 되지 않습니다. 가급적 다른 메일을 사용해 주세요.</td>
                        </tr>
                        <tr>
                            <th>직업</th>
                            <td>
                                <select class="form-control form-m" name="" id="">
                                    <?php
                                    $data_arr = array('선택', '중학생', '고등학생', '대학(원)생', '안경관련', '전문직', '주부', '컴퓨터/인터넷', '엔지니어', '디자인/의류', '연예/스포츠', '연구/개발', '금융/회계/법률', '방송/언론/출판/광고/홍보', '마케팅/영업/무역', '교육/교직', '공무원/군인', '일반관리/사무직', '제조업/생산직', '서비스업', '자영업', '무직', '기타');
                                    for( $i=0; $i<sizeof($data_arr); $i++ ){
                                        ?>
                                        <option value="<?=$i?>"><?=$data_arr[$i]?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>뉴스레터 수신</th>
                            <td>
                                <label for="newsletter_chk">
                                    <input type="radio" name="newsletter_chk"/> 예
                                </label>
                                &nbsp;
                                <label for="newsletter_chk">
                                    <input type="radio" name="newsletter_chk"/> 아니오
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th>&nbsp;</th>
                            <td>뉴스레터를 통해 이벤트 소식, 공지사항, 신제품 소식 등을 알려 드립니다.</td>
                        </tr>
                        </tbody>
                    </table>
                </section>
                <section class="content-section">
                    <h2>추가 정보</h2>
                    <h3 class="color-default">아래의 추가정보를 입력하시면 <span class="color-theme">쇼핑몰에서 사용하실 수 있는 적립금 1,000원</span>을 드립니다.</h3>
                    <table class="table-form">
                        <tbody>
                        <tr>
                            <th>방문경로</th>
                            <td>
                                <select class="form-control form-m" name="" id="">
                                    <?php
                                    $data_arr = array('선택', '검색사이트', '광고', '팜플릿', '친구소개', '기타');
                                    for( $i=0; $i<sizeof($data_arr); $i++ ){
                                        ?>
                                        <option value="<?=$i?>"><?=$data_arr[$i]?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>렌즈착용여부</th>
                            <td>
                                <label for="lens_chk">
                                    <input type="radio" name="lens_chk"/> 렌즈착용
                                </label>
                                &nbsp;
                                <label for="lens_chk">
                                    <input type="radio" name="lens_chk"/> 안경착용
                                </label>
                                &nbsp;
                                <label for="lens_chk">
                                    <input type="radio" name="lens_chk"/> 렌즈 + 안경착용
                                </label>
                                &nbsp;
                                <label for="lens_chk">
                                    <input type="radio" name="lens_chk"/> 미착용
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th>구입지역</th>
                            <td>
                                <select class="form-control form-m" name="" id="">
                                    <?php
                                    $data_arr = array('선택', '경기도', '인천광역시', '대전광역시', '대구광역시', '광주광역시', '울산광역시', '서울', '부산광역시', '제주도', '강원도', '경상남도', '전라남도', '경상북도', '전라북도', '충청남도', '충청북도' );
                                    for( $i=0; $i<sizeof($data_arr); $i++ ){
                                        ?>
                                        <option value="<?=$i?>"><?=$data_arr[$i]?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>구입처</th>
                            <td>
                                <select class="form-control form-m" name="" id="">
                                    <?php
                                    $data_arr = array('선택', '안과', '안경원');
                                    for( $i=0; $i<sizeof($data_arr); $i++ ){
                                        ?>
                                        <option value="<?=$i?>"><?=$data_arr[$i]?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <input class="form-control form-m" type="text"/>
                                안경원 혹은 안과명을 입력해 주세요.
                            </td>
                        </tr>
                        <tr>
                            <th>병력/신상정보</th>
                            <td>
                                <textarea class="form-control" name="" id="" cols="30" rows="10"></textarea>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </section>
                <section class="content-section">
                    <div class="btn-wrap">
                        <button class="btn btn-danger" type="submit">정보 수정</button>
                        <button class="btn btn-default" type="button">취소</button>
                    </div>
                </section>
            </form>
            <!-- form 끝 -->
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>