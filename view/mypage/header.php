<section class="content-breadcrumb">
    <?=$title;?>
</section>
<section class="content-header content-header--sub clearfix">
    <h1><?=$title;?></h1>
    <ul class="tabs">
        <li class="tab"><a href="asked.php">나의 상담내역</a></li>
        <!--

        쇼핑몰의 하위메뉴들과 중복되는 메뉴들로 일단 삭제

        <li class="tab"><a href="cart.php">나의 장바구니</a></li>
        <li class="tab"><a href="shopped.php">나의 쇼핑내역</a></li>

        -->
        <li class="tab"><a href="info.php">나의 정보관리</a></li>
        <li class="tab"><a href="history.php">나의 쇼핑내역</a></li>
        <li class="tab"><a href="reserve.php">적립금 확인</a></li>
        <li class="tab"><a href="withdraw.php">회원 탈퇴</a></li>
    </ul>
</section>