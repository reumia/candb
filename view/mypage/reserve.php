<?php
$theme = 'mypage';
$title = '마이 페이지';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
        include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    적립금 확인
                </h2>
                <h3>
                    가용 금액이란 현재 사용하실 수 있는 금액을 말합니다. <br/>
                    <small>적립금은 1,000원 단위로 사용이 가능하기 때문에 총 적립금액과 차이가 있을 수 있습니다.</small>
                </h3>
                <div class="table-wrap" style="margin-bottom: 60px;">
                    <table class="table-form">
                        <tbody>
                        <tr>
                            <th>총 적립금액</th>
                            <td class="text-right">10,000 원</td>
                        </tr>
                        <tr>
                            <th>가용 금액</th>
                            <td class="text-right">9,000 원</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="board">
                    <table class="board-table">
                        <thead>
                        <tr>
                            <th class="color-theme">변동일자</th>
                            <th class="color-theme">구매제품</th>
                            <th class="color-theme">적립금액</th>
                            <th class="color-theme">사용금액</th>
                            <th class="color-theme">잔액</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        for( $i = 4; $i > 0; $i-- ){
                            ?>
                            <tr>
                                <td>0000.00.00</td>
                                <td>보스톤 세정액</td>
                                <td>10,000</td>
                                <td>1,000</td>
                                <td>9,000</td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="4">합계</td>
                            <td>9,000</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>