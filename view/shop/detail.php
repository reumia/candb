<?php
$theme = 'shop';
$title = '쇼핑몰';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
            include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <section class="content-section content-section--bg color-default">
                <div class="media">
                    <div class="media-left">
                        <img class="media-object" src="../../static/img/others/shop_detail_productsample.png" alt=""/>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading color-default">
                            심플러스 다기능 용액
                            <br/>
                            Boston SIMPLUS Multi-Action Solution
                        </h3>
                        <table class="shop-detail-info">
                            <tbody>
                            <tr>
                                <th class="color-theme">제품코드</th>
                                <td>BS003</td>
                            </tr>
                            <tr>
                                <th class="color-theme">용량</th>
                                <td>120 ml</td>
                            </tr>
                            <tr>
                                <th class="color-theme">제조사</th>
                                <td>Polymer Technology Corp.</td>
                            </tr>
                            <tr>
                                <th class="color-theme">적립금</th>
                                <td>6%</td>
                            </tr>
                            <tr>
                                <th class="color-theme">가격</th>
                                <td>11,000 원</td>
                            </tr>
                            <tr>
                                <th class="color-theme">수량</th>
                                <td class="btn-wrap">
                                    <input class="shop-detail-quantity form-control form-inline" type="number"/>
                                    <button class="btn btn-default" type="button">담기</button>
                                    <button class="btn btn-default" type="button" onclick="history.back();">목록으로</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <ul class="shop-detail-thumb">
                            <li><a href="#"><img src="../../static/img/others/shop_detail_productsample.png" alt=""/></a></li>
                            <li><a href="#"><img src="../../static/img/lens/lens_achievement_ill1.png" alt=""/></a></li>
                            <li><a href="#"></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="content-section">
                <h2>
                    제품 상세 정보
                </h2>
                <div class="shop-content">
                    * 세척, 헹굼, 소독, 보존, 단백질 제거까지 한번에 <br/>
                    * 저녁에 문질러 닦아 줄 필요 없는 쉽고 간편한 렌즈 관리 <br/>
                    * 착용시 가볍게 문질러 헹구어 주세요. <br/>
                    <br/>
                    <b>용 도</b><br/>
                    <br/>
                    - 하나의 용액으로 세척, 헹굼, 소독, 보존, 단백질 제거를 합니다.<br/>
                    <br/>
                    <b>특 징</b><br/>
                    <br/>
                    - 이 제품은 RGP렌즈 성능을 효과적으로 유지해 주고, 간편히 관리하길 원하는 분들을 위해 고안되었습니다. 세척,헹굼,소독,보존,단백질제거를 동시에 함으로써 바쁜 현대인들이 편리하게 사용할 수 있습니다.<br/>
                    <br/>
                    <b>주의 사항</b><br/>
                    <br/>
                    - 개봉 후 90일이 지나면 용기를 폐기하십시요.<br/>
                </div>
            </section>
            <hr class="shop-detail-liner"/>
            <section class="content-section clearfix">
                <h2>
                    제품 사용 후기
                </h2>
                <div class="shop-comment">
                    <div class="shop-comment-write text-right">
                        <textarea class="form-control" name="" id="" cols="30" rows="10"></textarea>
                        <button class="btn btn-default btn-block" type="submit">작성하기</button>
                    </div>
                    <?php
                    for ( $i=0; $i<10; $i++ ){
                    ?>
                        <div class="shop-comment-list">
                            <div class="row">
                                <div class="col col-xs-3">
                                    <h3>이름</h3>
                                    <p class="color-theme">0000.00.00 00:00</p>
                                </div>
                                <div class="col col-xs-9">
                                    이벤트 행사라 덤으로 용량을더줘서 너무 기분 좋았습니다. 심플러스를 현재
                                    사용중인데. 눈에 들어가도 전혀 자극이 없고 기름처럼 윤활제 느낌이랄까.. <br/>
                                    부드럽고 좋구요. 렌즈도 잘 닦이고. 너무 마음에 듭니다. 가장좋았던점은
                                    한꺼번에 보존 세척 관리가 되서 저에겐 딱인듯 해요. 다음번엔 세척 보존 단백질
                                    제거도 따로 된거 구입해서 비교해볼게요
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                    <ul class="board-pagination">
                        <li><a href="#">&lt;</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">&gt;</a></li>
                    </ul>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>