<section class="content-breadcrumb">
    <?=$title;?>
</section>
<section class="content-header content-header--sub clearfix">
    <h1><?=$title;?></h1>
    <ul class="tabs">
        <li class="tab"><a href="list.php">제품 안내</a></li>
        <li class="tab"><a href="cart.php">장바구니</a></li>
        <li class="tab"><a href="history.php">구매내역 / 배송조회</a></li>
        <li class="tab"><a href="guide.php">쇼핑몰 이용안내</a></li>
    </ul>
</section>