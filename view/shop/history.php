<?php
$theme = 'shop';
$title = '쇼핑몰';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
        include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    구매내역 /<br class="only-pc"/>
                    배송조회
                </h2>
                <p>
                    주문번호를 클릭하시면 구매에 관한 상세한 내용을 보실 수 있습니다. <br/>
                    주문 취소 및 반품(환불) / 교환 신청은 주문번호를 클릭해 주세요. <br/>
                    주문 취소는 주문일로부터 7일 이내, 반품(환불)/교환신청은 배송 완료일부터 7일 이내에 가능합니다.
                </p>
                <br/>
                <div class="btn-wrap">
                    <label for="" style="margin-right: 20px;">주문일자별 조회</label>
                    <input class="form-control datepicker" type="text"/>
                    ~
                    <input class="form-control datepicker" type="text"/>
                    <button class="btn btn-default" type="submit">조회</button>
                </div>
                <br/>
                <div class="table-wrap">
                    <table class="board-table">
                        <thead>
                        <tr>
                            <th class="color-theme">주문번호</th>
                            <th class="color-theme">주문일시</th>
                            <th class="color-theme">제품명</th>
                            <th class="color-theme">결제금액</th>
                            <th class="color-theme">주문현황</th>
                            <th class="color-theme">배송조회</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        for ( $i=4; $i > 0; $i-- ){
                        ?>
                            <tr>
                                <td><a href="history_detail.php">320015004</a></td>
                                <td>0000.00.00</td>
                                <td>심플러스 용액 세트</td>
                                <td>10,000 원</td>
                                <td></td>
                                <td></td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>