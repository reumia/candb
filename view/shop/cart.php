<?php
$theme = 'shop';
$title = '쇼핑몰';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
        include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    장바구니
                </h2>
                <p>
                    구매가능한 최소 금액은 <span class="color-theme">20,000원</span>입니다. <br/>
                    배송은 우체국 택배에서 이루어지며, <span class="color-theme">배송료는 본사 부담</span>입니다. <br/>
                    결제 금액의 6%가 적립금으로 적립됩니다. <br/>
                    주문, 배송, 결제 등 쇼핑에 관해 보다 자세한 내용을 보시려면 <span class="color-theme">쇼핑몰 이용 안내</span>를 참고해 주세요.
                </p>
                <div class="table-wrap">
                    <table class="board-table">
                        <thead>
                        <tr>
                            <th class="color-theme">번호</th>
                            <th class="color-theme">제품명</th>
                            <th class="color-theme">제품코드</th>
                            <th class="color-theme">수량</th>
                            <th class="color-theme">금액</th>
                            <th class="color-theme">삭제</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        for ( $i=4; $i > 0; $i-- ){
                        ?>
                            <tr>
                                <td><?=$i;?></td>
                                <td>심플러스 용액 세트</td>
                                <td>????</td>
                                <td>
                                    <input class="form-control form-s" type="number"/>
                                    <button class="btn btn-default">적용</button>
                                </td>
                                <td>10,000 원</td>
                                <td><a class="btn btn-default" href="#">삭제하기</a></td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="3">합계</th>
                            <td><input class="form-control form-s" type="text" readonly/></td>
                            <td>40,000 원</td>
                            <td>&nbsp;</td>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="btn-wrap">
                        <a class="btn btn-primary" href="purchase.php">구매하기</a>
                        <a class="btn btn-default" href="list.php">계속 쇼핑하기</a>
                    </div>
                </div>
            </section>
            <section class="content-section">
                <h3>이용안내</h3>
                <ul class="btn-wrap">
                    <li class="btn-square disabled">
                        <a href="#">
                            <h1>1</h1>
                            <p>
                                쇼핑하신 제품을
                                구매하려면 구매하기
                                버튼을 누릅니다.
                            </p>
                        </a>
                    </li>
                    <li class="btn-square disabled">
                        <a href="#">
                            <h1>2</h1>
                            <p>
                                구매를 취소하고 싶은
                                제품은 삭제하기
                                버튼을 누르세요.
                            </p>
                        </a>
                    </li>
                    <li class="btn-square disabled">
                        <a href="#">
                            <h1>3</h1>
                            <p>
                                신용카드나
                                무통장입금 등의
                                결제가 가능합니다.
                            </p>
                        </a>
                    </li>
                </ul>
                <h3>구매 고객을 위한 다양한 혜택</h3>
                <p>
                    <strong>현금처럼 쓸 수 있는 적립금은 기본!</strong>
                    <br/>
                    적립금은 20,000원 이상 구매시 결제금액의 5%를 적립해드리며, 적립금이 3,000원 이상일 때 현금처럼 사용하실 수 있습니다.
                </p>
                <p>
                    <strong>전 구매 제품 무료 배송 서비스</strong>
                    <br/>
                    기본적으로 전 제품에 대해 무료 배송을 원칙으로 하고 있습니다. (단, 일부 제품, 일부 도서 산간 지역은 제외됩니다)
                </p>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>