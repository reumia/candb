<?php
$theme = 'shop';
$title = '쇼핑몰';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
        include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <!--form 시작-->
            <form action="">
                <section class="content-section">
                    <h2>
                        구매제품내역
                    </h2>
                    <div class="table-wrap">
                        <table class="board-table">
                            <thead>
                            <tr>
                                <th class="color-theme">번호</th>
                                <th class="color-theme">제품명</th>
                                <th class="color-theme">제품코드</th>
                                <th class="color-theme">수량</th>
                                <th class="color-theme">금액</th>
                                <th class="color-theme">삭제</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            for ( $i=3; $i > 0; $i-- ){
                                ?>
                                <tr>
                                    <td><?=$i;?></td>
                                    <td>심플러스 용액 세트</td>
                                    <td>????</td>
                                    <td>
                                        <input class="form-control form-s" type="number" readonly/>
                                    </td>
                                    <td>10,000 원</td>
                                    <td><a class="btn btn-default" href="#">삭제하기</a></td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="3">합계</th>
                                <td><input class="form-control form-s" type="text" readonly/></td>
                                <td>40,000 원</td>
                                <td>&nbsp;</td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </section>
                <section class="content-section">
                    <h2>주문고객정보</h2>
                    <div class="table-wrap">
                        <table class="table-form" style="width: 100%;">
                            <tbody>
                            <tr>
                                <th>고객명</th>
                                <td><input class="form-control form-m" type="text" value="홍길동" disabled/></td>
                                <th>이메일</th>
                                <td><input class="form-control form-m" type="email" value="candb@candb.co.kr" disabled/></td>
                            </tr>
                            <tr>
                                <th>전화번호</th>
                                <td><input class="form-control form-m" type="text" value="02-000-0000" disabled/></td>
                                <th>핸드폰</th>
                                <td><input class="form-control form-m" type="text" value="010-0000-0000" disabled/></td>
                            </tr>
                            <tr>
                                <th>주소</th>
                                <td colspan="3">
                                    <input class="form-control" type="text" value="주소" disabled/>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
                <section class="content-section">
                    <h2>배송지 정보</h2>
                    <div class="table-wrap">
                        <table class="table-form" style="width: 100%;">
                            <tbody>
                            <tr>
                                <th>받으시는 분</th>
                                <td>
                                    <input class="form-control form-m" type="text"/>
                                    <label for="">
                                        <input type="checkbox"/>
                                        주문하시는 분과 동일
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <th>전화번호</th>
                                <td>
                                    <input class="form-control form-s" type="text"/>
                                    <input class="form-control form-s" type="text"/>
                                    <input class="form-control form-s" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <th>핸드폰</th>
                                <td>
                                    <input class="form-control form-s" type="text"/>
                                    <input class="form-control form-s" type="text"/>
                                    <input class="form-control form-s" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <th>주소</th>
                                <td colspan="3">
                                    <input class="form-control form-s" type="text"/>
                                    <input class="form-control form-l" type="text"/>
                                    <button class="btn btn-default" type="button">우편번호 찾기</button>
                                </td>
                            </tr>
                            <tr>
                                <th>&nbsp;</th>
                                <td><input class="form-control form-xl" type="text"/></td>
                            </tr>
                            <tr>
                                <th>배송방법</th>
                                <td>
                                    <label for="">
                                        <input type="radio"/>
                                        우체국 택배
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <th>배송시 요구사항</th>
                                <td>
                                    <input class="form-control form-xl" type="text"/>
                                    30자 이내로 작성해 주세요.
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
                <section class="content-section">
                    <h2>결제 정보</h2>
                    <div class="board">
                        <table class="table-form" style="width: 100%;">
                            <tbody>
                            <tr>
                                <th>구매금액</th>
                                <td><input class="form-control form-m" type="text" value="11,000 원" disabled/></td>
                            </tr>
                            <tr>
                                <th>적립금 사용</th>
                                <td>
                                    <input class="form-control form-m" type="text" value="0"/>
                                    <button class="btn btn-default" type="button">적립금 확인</button>
                                    적립금은 1,000원 단위로 사용가능합니다.
                                </td>
                            </tr>
                            <tr>
                                <th>결제액</th>
                                <td><input class="form-control form-m" type="text" value="11,000 원" disabled/></td>
                            </tr>
                            <tr>
                                <th>결제방법</th>
                                <td>
                                    <label for="">
                                        <input type="radio" checked/>
                                        신용카드
                                    </label>
                                    &nbsp;
                                    <label for="">
                                        <input type="radio"/>
                                        무통장 입금
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <th>입금자명</th>
                                <td><input class="form-control form-m" type="text"/></td>
                            </tr>
                            <tr>
                                <th>입금계좌번호</th>
                                <td>국민은행 829-25-0018-065 / ㈜씨엔비 코퍼레이션</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
                <section class="content-section">
                    <div class="btn-wrap">
                        <a class="btn btn-primary" href="confirm.php">결제하기</a>
                        <button class="btn btn-default" type="reset">다시 입력</button>
                    </div>
                </section>
            </form>
            <!--form 끝-->
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>