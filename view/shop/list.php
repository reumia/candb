<?php
$theme = 'shop';
$title = '쇼핑몰';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
        include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>제품 안내</h2>
                <p>
                    구매가능한 최소 금액은 <span class="color-theme">20,000원</span>입니다. <br/>
                    배송은 우체국 택배에서 이루어지며, <span class="color-theme">배송료는 본사 부담</span>입니다. <br/>
                    결제 금액의 6%가 적립금으로 적립됩니다. <br/>
                    주문, 배송, 결제 등 쇼핑에 관해 보다 자세한 내용을 보시려면 <span class="color-theme">쇼핑몰 이용 안내</span>를 참고해 주세요.
                </p>
            </section>
            <section class="content-section">
                <h2>
                    렌즈 관리 용액
                </h2>
                <ul class="shop-list">
                    <?php
                    for( $i=0; $i<8; $i++ ){
                    ?>
                        <li>
                            <div class="shop-list-img">
                                <img src="../../static/img/others/shop_detail_productsample.png" alt=""/>
                            </div>
                            <h3 class="text-center">
                                <span class="color-default">심플러스 다기능 용액</span>
                                <br/>
                                <span>11,000 원</span>
                            </h3>
                            <div class="btn-wrap text-center">
                                <a class="btn btn-default" href="#">담기</a>
                                <a class="btn btn-default" href="detail.php">상세보기</a>
                            </div>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </section>
            <section class="content-section">
                <h2>
                    렌즈 보조 용품
                </h2>
                <ul class="shop-list">
                    <?php
                    for( $i=0; $i<2; $i++ ){
                    ?>
                        <li>
                            <div class="shop-list-img">
                                <img src="../../static/img/others/shop_detail_productsample.png" alt=""/>
                            </div>
                            <h3 class="text-center">
                                <span class="color-default">심플러스 다기능 용액</span>
                                <br/>
                                <span>11,000 원</span>
                            </h3>
                            <div class="btn-wrap text-center">
                                <a class="btn btn-default" href="#">담기</a>
                                <a class="btn btn-default" href="#">상세보기</a>
                            </div>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>