<?php
$theme = 'shop';
$title = '쇼핑몰';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
        include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <!--form 시작-->
            <form action="">
                <section class="content-section">
                    <h2>
                        구매내역 상세
                    </h2>
                    <div class="table-wrap">
                        <table class="table-form" style="width: 100%;">
                            <thead>
                            <tr>
                                <th class="color-theme" colspan="2">주문고객정보</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>주문번호</th>
                                <td>201503100006</td>
                            </tr>
                            <tr>
                                <th>주문일자</th>
                                <td>2015.03.10</td>
                            </tr>
                            <tr>
                                <th>주문현황</th>
                                <td>입금확인중</td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="btn-wrap">
                            <button class="btn btn-default" type="button">반품/환불신청</button>
                            <button class="btn btn-default" type="button" onclick="open_pop('../popup/exchange.php', '', '650')">교환신청</button>
                            <button class="btn btn-default" type="button" onclick="open_pop('../popup/cancel.php', '590')">주문취소/반품/교환처리현황</button>
                        </div>
                        <table class="table-form" style="width: 100%;">
                            <thead>
                            <tr>
                                <th class="color-theme" colspan="3">주문제품목록</th>
                            </tr>
                            <tr>
                                <th>제품명</th>
                                <th>수량</th>
                                <th>가격</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>심플러스 여행용 세트</td>
                                <td>1</td>
                                <td>30,000 원</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table-form" style="width: 100%;">
                            <thead>
                            <tr>
                                <th class="color-theme" colspan="8">주문고객정보</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>받으시는 분</th>
                                <td>홍길동</td>
                            </tr>
                            <tr>
                                <th>전화번호</th>
                                <td>02-000-0000</td>
                            </tr>
                            <tr>
                                <th>휴대전화</th>
                                <td>010-0000-0000</td>
                            </tr>
                            <tr>
                                <th>주소</th>
                                <td>000-000 서울특별시 마포구 서교동</td>
                            </tr>
                            <tr>
                                <th>배송방법</th>
                                <td>한진택배</td>
                            </tr>
                            <tr>
                                <th>배송시 요구사항</th>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table-form" style="width: 100%;">
                            <thead>
                            <tr>
                                <th class="color-theme" colspan="2">결제 정보</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>구매금액</th>
                                <td>30,000 원</td>
                            </tr>
                            <tr>
                                <th>적립금 사용여부</th>
                                <td>1,000 원</td>
                            </tr>
                            <tr>
                                <th>결제금액</th>
                                <td>29,000 원</td>
                            </tr>
                            <tr>
                                <th>결제방법</th>
                                <td>신용카드</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
                <section class="content-section">
                    <div class="btn-wrap">
                        <button class="btn btn-default" type="submit" onclick="history.back();">이전화면</button>
                    </div>
                </section>
            </form>
            <!--form 끝-->
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>