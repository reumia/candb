<?php
    $theme = 'about-eyes function';
    $title = '눈의 기능';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">궁금한 우리 눈 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1><?=$title;?></h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section clearfix">
            <h3 class="color-default">
                눈의 기능은 카메라와 매우 유사합니다. <br/>
                카메라에서 상(image)이 렌즈와 줌(zoom) 기능을 이용하여 망막에
                선명한 상을 맺히듯이 우리 눈에서는 빛이 각막과 수정체를 통과해서
                망막에 초점을 맞추게 됩니다.
            </h3>
            <h3 class="color-default">
                이때 각막의 형태, 수정체의 굴절력, 그리고 안구의 앞뒤 길이 3가지가
                시력을 결정하는 중요한 요소가 됩니다. 이렇게 눈에 들어오는 빛이 망막에
                초점을 정확히 맺지 못하면 상이 흐려지는데 이를 "굴절이상" 이라 하며
                근시, 난시 및 원시가 이에 해당됩니다.
            </h3>
        </section>
        <section class="content-section">
            <h2>
                눈의 구조
            </h2>
            <div class="image-wrap" style="margin-bottom: 40px;">
                <img src="../../static/img/eye/eye_function_ill.png" alt=""/>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>1. 각막 (Comea)</h3>
                    <p>
                        눈의 가장 바깥쪽에 있는 투명한 무혈관 조직으로
                        흔히 검은동자라고 합니다. 각막의 기능은 안구를
                        보호하는 방어막의 역할과 광선을 굴절시켜
                        망막으로 도달시키는 창의 역할을 합니다.
                        시력에 대해서 굴절이상이란?
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>2. 홍채 (Iris)</h3>
                    <p>
                        각막과 수정체 사이에 위치하며 홍채의 색은
                        인종별, 개인적으로 차이가 있습니다.
                        홍채의 기능은 빛의 양을 조절하는 조리개 역할을
                        합니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>3. 수정체</h3>
                    <p>
                        수정체는 카메라의 렌즈를 연상하면 됩니다.
                        빛을 굴절시켜 망막에 상이 맺히도록 하며 탄력성이
                        있습니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>4. 공막</h3>
                    <p>
                        눈 가장 바깥쪽을 감싸는 막으로 우리가 일반적으로
                        알고있는 흰자위를 말합니다.
                        눈의 형태를 유지하고 보호하는 기능을 합니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>5. 시신경 (Optic Nerve)</h3>
                    <p>
                        60만~80만개의 신경섬유로 되어있으며 망막의
                        신경섬유는 시신경유두를 향해 모여서 시신경을
                        통해 뇌에 전달됩니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>6. 황반부 (Macula)</h3>
                    <p>
                        망막의 가장 중심부위이며 우리눈의 중심시력을
                        관장하고 있는 곳입니다. 흔히 시력이 1.0이니 0.3이니
                        하는 것은 이 황반부에 맺히는 상의 식별 능력입니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>7. 망막 (Retina)</h3>
                    <p>
                        안구 뒤쪽 2/3를 덮고 있는 투명한 신경조직으로
                        카메라의 필름에 해당합니다.
                        눈으로 들어온 빛이 최종적으로 도달하는 곳이며
                        망막의 시세포들이 시신경을 통해 뇌로 신호를
                        보내는 기능을 합니다.
                    </p>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>
                눈물에 대해서
            </h2>
            <h3 class="color-default">
                눈물은 기름층, 수분층, 점액층 총 3층으로 이루어져 있으며 광학적으로
                안구의 매끈한 표면 유지, 안구표면 세척, 항균작용, 각막에 영양을
                공급하는 역할을 합니다.
            </h3>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>기름층</h3>
                    <p>
                        지질층은 눈물층의 가장 바깥쪽의 층으로
                        여러가지 종류의 지질로 구성되어 있으며 meibomian선에서
                        분비됩니다. 기름과 같은 성질을
                        가진 지질층은 수분의 증발과 흘러넘침을 막아
                        눈물층 표면의 형태를 유지시켜 주고, 각막과
                        눈꺼풀 사이에 윤활 역할을 해 주어 마찰을
                        방지합니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>수분층</h3>
                    <p>
                        수분층은 눈물의 90% 이상을 차지하고 중간에
                        위치하는 가장 두꺼운 층으로 물이 주성분입니다.
                        용해성이 강한 수분층은 주누선과 부누선에서
                        분비되어 각막이 필요로 하는 산소와 영양분을
                        공급하고, 눈물의 흐름과 배수작용으로 외부의
                        이물질을 제거합니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>점액층</h3>
                    <p>
                        점액층은 각막상피에 가장 가깝게 위치하고 있는
                        층이며, 이 층을 구성하는 뮤신(mucin)은 결막의
                        배상새포 (goblet cell)에서 분비된 수용성
                        단백질을 함유한 탄수화물류(glycoproteins)를
                        말하며 일반적으로 세포막이나 표면을 코팅하는
                        성분입니다. 점액층은 소수성(hydrophobic)의
                        각막상피를 덮어 친수성(hydrophilic)으로
                        변환시키고, 이로 인하여 흡수성(wettability)이
                        증가되어눈물이 각막을 촉촉하게 적셔주게
                        됩니다. 또한 불규칙한 각막상피를 매끄럽게 하여
                        좋은 광학표면을 갖도록 해 줍니다.
                    </p>
                </div>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>
            눈과 시력에 대해서 더 알아보세요!
        </h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="eye_sight.php">
                    <img src="../../static/img/eye/eye_1st_icon2.png" alt=""/>
                    시력에 대해서
                </a>
            </li>
            <li class="btn-square">
                <a href="eye_curve.php">
                    <img src="../../static/img/eye/eye_1st_icon3.png" alt=""/>
                    굴절이상이란?
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>