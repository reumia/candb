<?php
    $theme = 'about-eyes keeping';
    $title = 'RGP 렌즈 보관법';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">궁금한 우리 눈 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1><?=$title;?></h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h3 class="color-default">
                콘택트렌즈를 건강하게 사용하기 위해서는 콘택트렌즈 보관법을 잘 알고 실천하도록
                해야합니다. 콘택트렌즈는 온도변화가 심한 너무 뜨거운 곳이나 차가운 곳은 피하는 것이
                좋고, 견고한 케이스에 청결하게 관리해 주어야 합니다.
            </h3>
        </section>
        <section class="content-section">
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/eye/eye_preserve_ill1.png" alt=""/>
                    <p>
                        눈에서 렌즈를 빼내어 세척액으로 닦은 다음,
                        헹굼액으로 충분히 헹군 뒤
                        콘택트렌즈 케이스에 보관해야 합니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/eye/eye_preserve_ill2.png" alt=""/>
                    <p>
                        렌즈 케이스는 서너 개 정도를 교대로 사용하는
                        게 이상적이며, 매일 물로 닦아 주고 일광소독을
                        하는 게 좋습니다.
                    </p>
                </div>
            </div>
        </section>
        <section class="content-section">
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/eye/eye_preserve_ill3.png" alt=""/>
                    <p>
                        그리고 렌즈 케이스의 보존액은 매일 갈아 주는
                        것이 좋습니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/eye/eye_preserve_ill4.png" alt=""/>
                    <p>
                        양안의 시력 차이가 있을 때에는 렌즈 케이스를
                        좌우가 뚜렷이 구별되게 보관하여야 합니다.
                    </p>
                </div>
            </div>
        </section>
        <section class="content-section">
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/eye/eye_preserve_ill5.png" alt=""/>
                    <p>
                        또한 렌즈 케이스는 콘택트렌즈가 외부의 압력을
                        받지 않도록 견고하고 휴대가 간편해야 합니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/eye/eye_preserve_ill6.png" alt=""/>
                    <p>
                        렌즈를 케이스에 담았을 때는 마른 상태보다 젖어
                        있는 것이 친수성을 높여 주고
                        콘택트렌즈 변형을 방지하며, 렌즈에 흡착한
                        이물질을 어느 정도 없애 주게 됩니다.
                    </p>
                </div>
            </div>
        </section>
        <section class="content-section">
            <div class="row">
                <div class="col col-xs-6">
                    <h3>장기간 렌즈 보관법</h3>
                    <p>
                        렌즈를 장기간 착용하지 않을 때에는 렌즈를 깨끗이
                        세척하여 건조한 상태로 납작한 케이스에
                        보관하시면 반영구적입니다. 다음에 시력의 변화만
                        없다면 세척하여 다시 착용하셔도 됩니다.
                        건조한 상태로 보관 시 중간중간 세척할 필요는
                        없으며, 렌즈 착용 하루 전 보존액에 담궈 두신 후
                        다시 착용하시면 됩니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                </div>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>
            콘택트렌즈의 사용과 관리에 대해서 더 알아보세요!
        </h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="lens_makeup.php">
                    <img src="../../static/img/eye/eye_1st_icon5.png" alt=""/>
                    안전한 화장법
                </a>
            </li>
            <li class="btn-square">
                <a href="lens_warning.php">
                    <img src="../../static/img/eye/eye_1st_icon6.png" alt=""/>
                    주의해야할 안질환
                </a>
            </li>
            <li class="btn-square">
                <a href="lens_qna.php">
                    <img src="../../static/img/eye/eye_1st_icon7.png" alt=""/>
                    궁금증과 오해
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>