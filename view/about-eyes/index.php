<?php
    $theme = 'about-eyes';
    $title = '궁금한 우리 눈';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <?=$title;?>
    </section>
    <section class="content-header clearfix">
        <img src="../../static/img/eye/eye_1st_top.png" alt="" style="margin-top: -70px;"/>
        <h1><?=$title;?></h1>
        <h3>
            세상을 볼수 있게 해주는 소중한 우리눈.
            <br/>
            눈과 시력에 대한 자세한 정보와, 콘택트 렌즈를 건강하게 사용하는 방법에<br class="only-pc"/> 대한 정보를 이해하기 쉽게 알려드립니다.
        </h3>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h2>눈과 시력에<br class="only-pc"/> 대해서</h2>
            <h3>우리의 눈은 어떻게 이루어져 있을까요?</h3>
            <p>
                눈은 여러 단계의 구조를 통해 정확한 시각 정보를 뇌에 전달하는 우리의 시각을 담당하는 주요<br class="only-pc"/>
                감각기관으로 일상생활을 하는데 매우 중요한 기관입니다. 우리가 미쳐 알지 못했던 눈의 구조와<br class="only-pc"/>
                기능, 또한 굴절이상으로 인한 시성의 종류에 대하여 알아보도록 하겠습니다.
            </p>
            <ul class="btn-wrap">
                <li class="btn-square">
                    <a href="eye_function.php">
                        <img src="../../static/img/eye/eye_1st_icon1.png" alt=""/>
                        눈의 기능
                    </a>
                </li>
                <li class="btn-square">
                    <a href="eye_sight.php">
                        <img src="../../static/img/eye/eye_1st_icon2.png" alt=""/>
                        시력에 대해서
                    </a>
                </li>
                <li class="btn-square">
                    <a href="eye_curve.php">
                        <img src="../../static/img/eye/eye_1st_icon3.png" alt=""/>
                        굴절이상이란?
                    </a>
                </li>
            </ul>
        </section>
        <section class="content-section">
            <h2>컨텍트렌즈의<br class="only-pc"/> 사용과 관리</h2>
            <h3>콘택트 렌즈 관리, 어렵지 않아요!</h3>
            <p>
                우리의 눈 건강과 올바른 콘택트렌즈의 착용을 위하여 렌즈 보관법, 렌즈 착용시 유의할 점과<br class="only-pc"/>
                주의해야 할 안질환, 그 외 콘택트렌즈가 아직 생소하고 두려우신 분들을 위해 콘택렌즈착용 시<br class="only-pc"/>
                궁금했던 여러 가지 사항들에 대하여 알아보도록 하겠습니다.
            </p>
            <ul class="btn-wrap">
                <li class="btn-square">
                    <a href="lens_keeping.php">
                        <img src="../../static/img/eye/eye_1st_icon4.png" alt=""/>
                        렌즈 보관법
                    </a>
                </li>
                <li class="btn-square">
                    <a href="lens_makeup.php">
                        <img src="../../static/img/eye/eye_1st_icon5.png" alt=""/>
                        안전한 화장법
                    </a>
                </li>
                <li class="btn-square">
                    <a href="lens_warning.php">
                        <img src="../../static/img/eye/eye_1st_icon6.png" alt=""/>
                        주의해야할 안질환
                    </a>
                </li>
                <li class="btn-square">
                    <a href="lens_qna.php">
                        <img src="../../static/img/eye/eye_1st_icon7.png" alt=""/>
                        궁금증과 오해
                    </a>
                </li>
            </ul>
        </section>
        <section class="content-section only-pc">
            <h2>C&amp;B가<br class="only-pc"/> 알려드리는<br class="only-pc"/> 눈 이야기</h2>
            <h3>눈과 콘택트 렌즈에 대한 유용한 정보</h3>
            <p>
                맑고 밝은 눈을 위하여 눈과 콘택트렌즈에 대한 여러 가지 유용한 정보를 알려드리고 있습니다. <br/>
                다양한 정보를 얻어가시고 우리의 소중한 눈을 건강하게 지켜주세요.
            </p>
            <div class="row">
                <div class="col col-xs-6">
                    <h3 class="color-default"><a href="#">게시판 최근글 제목</a></h3>
                    <a href="#" style="display:block; background: #999; height: 180px;">

                    </a>
                </div>
                <div class="col col-xs-6">
                    <h3 class="color-default"><a href="#">게시판 최근글 제목</a></h3>
                    <a href="#" style="display:block; background: #999; height: 180px;">

                    </a>
                </div>
            </div>
            <div class="btn-wrap text-right">
                <a class="btn btn-default" href="story.php">게시판 전체 보기</a>
            </div>
        </section>
    </section>
</article>
<?php
    include_once '../inc/footer.php';
?>