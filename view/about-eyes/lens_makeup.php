<?php
    $theme = 'about-eyes makeup';
    $title = '렌즈 착용시 안전한 화장법';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">궁금한 우리 눈 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1><?=$title;?></h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h3 class="color-default">
                콘택트렌즈를 건강하게 사용하기 위해서는 콘택트렌즈 보관법을 잘 알고 실천하도록
                해야합니다. 콘택트렌즈는 온도변화가 심한 너무 뜨거운 곳이나 차가운 곳은 피하는 것이
                좋고, 견고한 케이스에 청결하게 관리해 주어야 합니다.
            </h3>
        </section>
        <section class="content-section">
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/eye/eye_makeup_ill1.png" alt=""/>
                    <h3>자극이 적은 화장품 선택하기</h3>
                    <p>
                        알레르기 반응을 적게 유발하는 화장품을
                        선택합니다. 저알레르기성 'hypoallergenic',
                        콘택트렌즈 착용자용 'for contact lens wearers',
                        민감한 눈에 사용 'for sensitive eyes' 등의
                        문구가 포함되어 있는 잘 알려지고 신뢰할 수
                        있는 회사의 제품을 선택합니다. 또한 유분이
                        많은 크림타입의 화장품과 클리너는 피하고,
                        워터베이스나 저자극성 리퀴드 타입의 화장품을
                        사용하는 것이 안전합니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/eye/eye_makeup_ill2.png" alt=""/>
                    <h3>콘택트렌즈를 착용하기 전 화장 마치기</h3>
                    <p>
                        콘택트렌즈를 먼저 끼고 나서 화장을 해야
                        화장을 정확히 할 수 있고, 콘택트렌즈를 낄 때
                        눈물이 흘러 화장을 얼룩지게 하는 것을 막을 수
                        있습니다. 또한 화장품의 가루가 눈에 들어가기
                        쉽기 때문에 콘택트렌즈를 먼저 끼고 나서
                        화장을 하는 것이 좋습니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/eye/eye_makeup_ill3.png" alt=""/>
                    <h3>화장을 지우기 전에 반드시 렌즈 제거</h3>
                    <p>
                        세안 시 깨끗하게 손을 씻은 후 렌즈를 먼저 제거하고
                        세척, 보관한 후에 화장을 지워야 합니다. <br/>
                        유성화장품이나 진한 화장은 눈 화장 전용 로션 및
                        리무버를 이용하는데 조금씩 사용해야 하며 절대
                        눈에 들어가지 않도록 조심해야 합니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/eye/eye_makeup_ill4.png" alt=""/>
                    <h3>드라이를 할 때는 눈을 자주 깜빡일 것</h3>
                    <p>
                        눈이 건조하면 렌즈가 뻑뻑해 지고 불편해지기
                        마련입니다. 드라이기의 바람이 눈에 바로 닿게
                        되면 콘텐트렌즈가 건조해 지기 때문에 눈을 자주
                        깜박여 적당한 수분공급에 신경 써야 합니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/eye/eye_makeup_ill5.png" alt=""/>
                    <h3>화장 후 인공눈물로 화장품을 씻어내야!</h3>
                    <p>
                        화장의 마지막 단계에서 머리를 옆으로 돌리고
                        눈꼬리 부분에 휴지를 댄 후 인공눈물 5~6 방울을
                        안구에 흘려 넣어 안구를 세척 하는 것도 좋은
                        방법입니다. <br/>
                        이렇게 하면 화장 도중 안구에 들어간 화장성분이
                        세척되기 때문에 결막 및 각막의 자극을 줄일 수
                        있습니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/eye/eye_makeup_ill6.png" alt=""/>
                    <h3>헤어스프레이도 렌즈 착용 전에</h3>
                    <p>
                        머리의 볼륨을 살려주기 위한 헤어스프레이도
                        눈에 들어가기 쉽습니다. 헤어 스프레이와 같이
                        냄새가 강한 화장품(향수, 아세톤 등)은
                        콘택트렌즈를 착용하기 전에 사용하며,
                        화학물질이 렌즈에 닿지 않도록 주의 해야
                        합니다. 또한 염색이나 퍼머 약 또한 의학용품의
                        샴푸를 사용할 때에도 렌즈착용은 피하는 것이
                        좋습니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/eye/eye_makeup_ill7.png" alt=""/>
                    <h3>아이메이크업은 예쁘게! 제품은 청결하게!</h3>
                    <p>
                        눈에 가장 가까이 사용하는 아이메이크업 제품 중,
                        마스카라나 아이라이너는 세균의 증식이 쉽습니다.
                        항상 청결하게 사용하는 것은 물론, 3-4 개월 안에 다
                        사용하거나, 바꾸어 주는 것이 좋습니다. <br/>
                        아이라인과 마스카라를 바를 때는 점막부분을 피해
                        속눈썹 바깥쪽에 바르며, 물에 잘 녹지 않는 제품을
                        사용하는 것이 좋습니다. 눈꺼풀 안쪽에 그리면
                        염증과 오염을 초래하는 원인이 되므로 눈썹의
                        외부에 사용하세요. <br/>
                        아이쉐도우는 과다한 파우더 입자가 눈에 들어가는
                        것을 피하기 위해 펄이 강하고 가루날림이 있는
                        제품보다, 압축 분으로 되어있는 제품을 선택하고,
                        소량씩 여러 번 사용하는 하세요.
                    </p>
                </div>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>
            콘택트렌즈의 사용과 관리에 대해서 더 알아보세요!
        </h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="lens_keeping.php">
                    <img src="../../static/img/eye/eye_1st_icon4.png" alt=""/>
                    렌즈 보관법
                </a>
            </li>
            <li class="btn-square">
                <a href="lens_warning.php">
                    <img src="../../static/img/eye/eye_1st_icon6.png" alt=""/>
                    주의해야할 안질환
                </a>
            </li>
            <li class="btn-square">
                <a href="lens_qna.php">
                    <img src="../../static/img/eye/eye_1st_icon7.png" alt=""/>
                    궁금증과 오해
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>