<?php
    $theme = 'about-eyes warning';
    $title = '렌즈 착용시 주의해야할 안질환';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">궁금한 우리 눈 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1><?=$title;?></h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <div class="row">
                <div class="col col-xs-6">
                    <h3>
                        거대유두 결막염 <br/>
                        giant papillary conjunctivitis
                    </h3>
                    <p>
                        소프트렌즈 착용자에게서 많이 나타나나,
                        하드렌즈와 의안을 착용하는 사람에게서도 나타날
                        수 있습니다. 거대유두결막염의 원인은 확실하지
                        않지만 복합적 요소에 의해 발생한다고
                        생각됩니다. 그 증상으로 콘택트렌즈가 상안검
                        위쪽으로 올라가며 충혈이나 염증, 안분비물,
                        희미한 시력, 눈물, 광선, 현기증 등이 나타나기도
                        합니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>
                        접촉성 각결막염 <br/>
                        contact keratoconjunctivitis
                    </h3>
                    <p>
                        보존액, 클리너(세척액), 생리식염수, 단백질
                        제거제 효소, 즉 클로드핵시딘이나 티메로살 등의
                        화학적인 물질은 렌즈 조직 내에서 결합반응을
                        일으킬 수 있으며, 이러한 결합반응은
                        콘택트렌즈에 대한 과민반응을 일으키는 원인이
                        될 수도 있습니다. 이에 대한 증상으로
                        각막(홍채)에 충혈과 염증이 발생합니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>
                        안건조증 <br/>
                        xerophthalmia
                    </h3>
                    <p>
                        렌즈 속으로 흡수된 화학약품이 축적되는
                        악순환은 건조 증상을 유발합니다. <br/>
                        이런 경우에는 감각과민증이 나타나는 것이
                        일반적이며, 감각과민증이 이미 형성되어 있으면
                        소프트렌즈는 착용할 수 없습니다. 이에 따른
                        증상으로 각결막염을 유발할 수 있습니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>
                        각막 신생혈관 <br/>
                        corneal neovascularization
                    </h3>
                    <p>
                        각막신생혈관이 심한 경우에는 외상이나
                        산소결핍증, 감각과민증의 원인이 됩니다. <br/>
                        일반적으로 각막과 결막의 경계선상(각막윤부)에서
                        잘 나타납니다. 심한 신생혈관은 시력에 장애를 줄
                        수도 있습니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>
                        각막상피외상 <br/>
                        corneal epithelium trauma
                    </h3>
                    <p>
                        각막상피외상은 콘택트렌즈를 잘못 착용했을 때
                        발생합니다. 특히 콘택트렌즈를 뒤집어 착용했거나,
                        착용된 콘택트렌즈가 심하게 움직이면 각막상피층에
                        외상이 생기게 됩니다. <br/>
                        만약 계속적으로 각막에 상처를 주게 되면
                        보우만막에까지 상처를 입힐 수 있습니다. 따라서 그
                        상처는 없어지지 않으므로 주의해야 합니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>
                        각막부종 <br/>
                        corneal edema
                    </h3>
                    <p>
                        각막부종은 산소결핍이 원인이 되어 발생합니다.
                        콘택트렌즈가 너무 단단하게 착용되었거나, 장시간
                        콘택트렌즈를 착용해서 오는 경우가 대부분입니다.
                        이 병의 증상으로는 각막 커브가 변하고, 각막의
                        굴절력이 약해지며 시력이 침침해집니다. 그리고
                        빛을 보게 되면 빛 주위에 테두리 같은 섬광이 보이게
                        되고, 콘택트렌즈 대신 안경을 착용해도 빛 주위에
                        테두리 같은 섬광현상이 없어지지 않습니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>
                        급성 폐쇄증
                    </h3>
                    <p>
                        급성폐쇄증은 하드렌즈를 연속 착용할 경우, 실명에
                        이르게 되는 주된 원인이 되기도 합니다. <br/>
                        이 병의 증상은 극심한 통증과 충혈을 유발합니다
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>
                        세균, 녹농균, 바이러스, 진균 등에 의한 감염 <br/>
                        giant papillary conjunctivitis
                    </h3>
                    <p>
                        콘택트렌즈 착용으로 인해 발생하는 부작용 중 가장
                        심각한 것이 아칸타메바 각막염(acanthameoba
                        keratitis), 결막충혈 및 중심부 각막궤양이며, 이로
                        인해 실명을 유발할 수도 있으니 각별히 유의해야
                        합니다.
                    </p>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>
                콘택트렌즈<br class="only-pc"/>
                부작용<br class="only-pc"/>
                예방법
            </h2>
            <div class="row">
                <div class="col col-xs-6">
                    <div class="btn-square disabled">
                        <a href="#">
                            <h1>1</h1>
                            장시간 착용 금지
                        </a>
                    </div>
                    <p class="clearboth">
                        모든 콘택트렌즈는 12시간 이상 착용하지 말고 안경과
                        병행해서 착용하는 것이 눈의 보호에 좋습니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <div class="btn-square disabled">
                        <a href="#">
                            <h1>2</h1>
                            렌즈의 위생관리 철저히
                        </a>
                    </div>
                    <p class="clearboth">
                        렌즈는 세척과 소독을 철저히 해 주어야 합니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <div class="btn-square disabled">
                        <a href="#">
                            <h1>3</h1>
                            렌즈 사용전 철저한 사전 검사 필요
                        </a>
                    </div>
                    <p class="clearboth">
                        안질환의 유무 검사, 사후관리와 사후검사를 실행할 수
                        있는지의 여부, 알레르기 체질이 있는지의 여부 등
                        콘택트렌즈를 착용할 수 있는지에 대한 철저한 사전
                        검사를 받아야 합니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <div class="btn-square disabled">
                        <a href="#">
                            <h1>4</h1>
                            올바른 제품 선택
                        </a>
                    </div>
                    <p class="clearboth">
                        믿을 수 있는 회사의 제품을 선택하여 알맞은 처방을
                        실행해야 합니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <div class="btn-square disabled">
                        <a href="#">
                            <h1>5</h1>
                            렌즈 사용시 주의사항 숙지
                        </a>
                    </div>
                    <p class="clearboth">
                        콘택트렌즈 사용 시 주의사항을 숙지하여 위생적으로
                        착용하되 장시간 착용하지 않으며, 이상이 있을 때는
                        즉시 안과전문의와 상의해야 합니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <div class="btn-square disabled">
                        <a href="#">
                            <h1>6</h1>
                            정기적으로 안과 방문
                        </a>
                    </div>
                    <p class="clearboth">
                        정기적으로 안과를 방문하여 검사를 받습니다.
                    </p>
                </div>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>
            콘택트렌즈의 사용과 관리에 대해서 더 알아보세요!
        </h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="lens_keeping.php">
                    <img src="../../static/img/eye/eye_1st_icon4.png" alt=""/>
                    렌즈 보관법
                </a>
            </li>
            <li class="btn-square">
                <a href="lens_makeup.php">
                    <img src="../../static/img/eye/eye_1st_icon5.png" alt=""/>
                    안전한 화장법
                </a>
            </li>
            <li class="btn-square">
                <a href="lens_qna.php">
                    <img src="../../static/img/eye/eye_1st_icon7.png" alt=""/>
                    궁금증과 오해
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>