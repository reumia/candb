<?php
    $theme = 'about-eyes curve';
    $title = '굴절이상이란?';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">궁금한 우리 눈 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1><?=$title;?></h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h3 class="color-default">
                조절을 하지 않은 상태에서 외부에서 들어오는 평행한 광선이 눈의 각막과
                수정체를 지나 망막에 정확한 상을 맺는 것을 말합니다. ‘카메라등의 초점이 잘
                맞다’ 라는 말과 같다고 할 수 있는데, 이것을 정상시력 정시안 도는 정시라고
                부릅니다.눈에 들어오는 빛이 망막에 초점을 정확히 맺지 못하면 상이
                흐려지는데 이를 ‘굴절이상’이라 하며, 근시, 난시 및 원시가 이에 해당합니다.
            </h3>
            <div class="image-wrap text-center">
                <img src="../../static/img/eye/eye_reflect_ill0.png" alt=""/>
            </div>
        </section>
        <section class="content-section">
            <h2>근시</h2>
            <h3 class="color-default">
                근시란 안구의 앞뒤길이가 정상보다 길거나, 각막이나 수정체의 굴절력이
                강해 상이 망막보다 앞에 맺어지는 굴절이상을 말합니다. 굴절이상의 약
                50~70%를 차지하며 오목렌즈로 교정을 하게 됩니다.
            </h3>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>교정</h3>
                    <p>
                        오목렌즈는 대상을 잘보이게 하지만 원래 크기보다
                        작게 보이고 RGP렌즈는 사물이 잘보이면서 원래의
                        크기로 보입니다.
                    </p>
                    <img src="../../static/img/eye/eye_reflect_ill1.png" alt=""/>
                </div>
                <div class="col col-xs-6">
                    <h3>예방</h3>
                    <p>
                        근시의 예방은 주위환경개선이 가장 중요합니다.
                        책상에서의 작업이나 독서 시 적절한 조명 아래
                        바른자세를 유지하고, 눈과 책과의 거리는 30~40cm
                        정도를 유지해야 합니다. <br/>
                        잦은 TV시청, 스마트폰, 컴퓨터작업 등도 눈의 피로를
                        가중하게 하여 근시진행이 빨라지게 할 수 있으므로
                        장시간 보지 않는 것이 좋으며, 적당한 운동과 휴식,
                        고른 영양섭취로 근시의 예방 및 진행을 억제토록
                        하여야 합니다. <br/>
                        또한 시력검사는 정기적으로 받아 이상 유무를 조기
                        발견함으로써 더 큰 진행을 막는 것이 좋습니다.
                    </p>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>원시</h2>
            <h3 class="color-default">
                원시란 안구의 전후 길이가 정상보다 짧거나 각막이나 수정체의 굴절력이
                약해 망막의 뒤쪽에 물체의 상이 맺히기 때문에 먼 곳은 잘 보이나 가까운
                것은 잘 보이지 않는 굴절이상을 말하는 것으로 볼록렌즈로 교정을 하게
                됩니다.
            </h3>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>교정</h3>
                    <p>
                        볼록렌즈는 대상을 잘 보이게 하나 원래 크기보다
                        크게 보이고 RGP렌즈는 사물이 잘 보이면서 원래의
                        크기로 잘 보입니다.
                    </p>
                    <img src="../../static/img/eye/eye_reflect_ill2.png" alt=""/>
                </div>
                <div class="col col-xs-6">
                    <h3>예방</h3>
                    <p>
                        원시는 근시의 경우보다 가까운 거리에서의 작업 시
                        시력감퇴, 안통, 두통 등이 나타날 수 있으며 노년이
                        되면 조절력이 더욱 부족해지므로 노안이 다른 사람보다
                        더 빨리 나타날 수 있습니다. <br/>
                        그러나 원시라 하더라도 시력이 좋고 초점 조절을
                        담당하는 모양체 근육의 균형이 정상일때는 안경이 필요
                        없습니다.
                    </p>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>난시</h2>
            <h3 class="color-default">
                정상각막은 그 모양이 농구공처럼 둥근 모양인데 난시가 있으면 각막이
                마치 럭비공처럼 되어 서로 다른 각막의 굴절력으로 한 점에 초점을 맺지
                못해 상이 흐려지게 되며, 이때는 근거리 원거리 모두에서 상이 흐려지게
                되고, 눈이 피로하기 쉬우며, 물건이 이중으로 보이기도 하고 두통, 충혈이
                생기기도 합니다.
            </h3>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>교정</h3>
                    <p>
                        근시성 난시의 경우는 오목렌즈를 사용하여 교정합니다.
                        오목렌즈는 원래 크기보다 작게 보이지만 RGP렌즈는
                        원래의 크기로 보입니다.
                    </p>
                    <img src="../../static/img/eye/eye_reflect_ill3.png" alt=""/>
                    <p>
                        원시성 난시인 경우 볼록 렌즈를 사용하여 교정합니다.
                        볼록렌즈는 원래 크기보다 크게 보이지만 RGP렌즈는
                        원래의 크기로 보입니다.
                    </p>
                    <img src="../../static/img/eye/eye_reflect_ill4.png" alt=""/>
                </div>
                <div class="col col-xs-6">
                    <h3>예방</h3>
                    <p>
                        난시를 예방하기 위해서는 돗수에 맞지 않는 안경을
                        쓰거나, 너무 어두운 곳에서의 작업, 흔들리는 차
                        안에서의 독서 등은 피하고 만 3세가 되면 안과검진을
                        받아 굴절 이상 여부를 보는 것 이 좋습니다.
                    </p>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>노안</h2>
            <h3 class="color-default">
                나이가 들면서 우리 인체의 모든 세포 또한 노화가 되는데, 특히 안구 내의
                수정체가 딱딱해지고 탄력이 떨어지게 됩니다. 이로 인해 조절력이
                감소되어 근거리 작업이 장애를 받게 되어 먼 거리는 잘 보이고 가까운 곳에
                있는 상이 흐리게 보이게 됩니다.
            </h3>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>교정</h3>
                    <p>
                        근거리 작업시 볼록렌즈를 착용하며, 일반적으로
                        가장 낮은 굴절력으로 선명하고 편안한 근거리
                        시력을 갖도록 처방합니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>예방</h3>
                    <p>
                        노안의 경우에는 다른 신체 기관에 비해 관리하기가
                        쉽지는 않기에 노안이 오기 전 미리 눈 관리를 해 주는
                        것이 더 중요합니다. <br/>
                        장시간 독서나 스마트폰, 컴퓨터 사용을 피하고
                        중간중간 눈을 깜빡여 눈물을 보충해 주는것이
                        좋습니다. 무엇보다 중요한 것은 주기적으로 안과검진을
                        받아보는 것이 큰 도움이 됩니다.
                    </p>
                </div>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>
            눈과 시력에 대해서 더 알아보세요!
        </h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="eye_function.php">
                    <img src="../../static/img/eye/eye_1st_icon1.png" alt=""/>
                    눈의 기능
                </a>
            </li>
            <li class="btn-square">
                <a href="eye_sight.php">
                    <img src="../../static/img/eye/eye_1st_icon2.png" alt=""/>
                    시력에 대해서
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>