<?php
    $theme = 'about-eyes';
    $title = 'C&B가 알려드리는 눈 이야기';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">궁금한 우리 눈 > </span><?=$title;?>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h2>
                C&amp;B가 <br class="only-pc"/>
                알려드리는 <br class="only-pc"/>
                눈 이야기
            </h2>
            <div class="board">
                <table class="board-table">
                    <thead>
                    <tr>
                        <th class="color-theme">번호</th>
                        <th class="color-theme">제목</th>
                        <th class="color-theme">날짜</th>
                        <th class="color-theme">조회</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    for( $i = 10; $i > 0; $i-- ){
                        ?>
                        <tr>
                            <td><?=$i;?></td>
                            <td><a href="#">테스트입니다. 테스트입니다.</a></td>
                            <td>0000.00.00</td>
                            <td>00</td>
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
                <ul class="board-pagination">
                    <li><a href="#">&lt;</a></li>
                    <li><a href="#" class="active">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">&gt;</a></li>
                </ul>
                <hr/>
                <div class="row">
                    <div class="col col-xs-6 col-xs-offset-6">
                        <div class="board-search text-right">
                            <select class="form-control" name="" id=""></select>
                            <input class="form-control" type="text"/>
                            <button class="btn btn-default">검색</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</article>
<?php
    include_once '../inc/footer.php';
?>