<?php
    $theme = 'about-eyes sight';
    $title = '시력에 대해서';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">궁금한 우리 눈 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1><?=$title;?></h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h3 class="color-default">
                시력이란 물체의 존재나 형상을 식별할 수 있는 능력을 수치로 나타낸 것으로
                우리나라에서는 보통 한천석 시력표와 Snelletn E 시력표를 많이 사용합니다.
            </h3>
        </section>
        <section class="content-section">
            <p>
                시력은 보통 0.1부터 2.0까지 나타내며, 0.1이 안 보이는 시력이라면 0.05, 0.025 등으로 나타내고 그래도 안
                보이면 눈 앞 30cm 앞에서 손가락을 셀 수 있는 시력(안전수지), 눈 앞에서 물체가 움직이는 것을 감지할 수 있는
                시력(안전수동), 눈 앞에서 불빛을 감지할 수 있는 시력(광각변별) 등으로 나타냅니다.
            </p>
            <p>
                흔히, 근시가 심하면 "마이너스 시력" 이라고 하는데 잘못된 오해이며, 근시의 굴절력은 -D(마이너스 디옵터)로,
                원시의 굴절력은 +D(플러스 디옵터)로 표시하는 것을 혼돈하여 생긴 것입니다.
            </p>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>
            눈과 시력에 대해서 더 알아보세요!
        </h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="eye_function.php">
                    <img src="../../static/img/eye/eye_1st_icon1.png" alt=""/>
                    눈의 기능
                </a>
            </li>
            <li class="btn-square">
                <a href="eye_curve.php">
                    <img src="../../static/img/eye/eye_1st_icon3.png" alt=""/>
                    굴절이상이란?
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>