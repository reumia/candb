<?php
    $theme = 'about-eyes qna';
    $title = '렌즈 착용에 대한 궁금증&amp;오해';
    include_once '../inc/header.php';
    include_once 'lens_qna_arr.php'
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">궁금한 우리 눈 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1><?=$title;?></h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h3 class="color-default">
                콘택트 렌즈에 대해서 많은 분들이 궁금해 하시는 질문들과, 잘못 알려진 오해들에 대해서
                C&B에서 자세히 알려드립니다!
            </h3>
        </section>
        <section class="content-section">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php
                $i = 1;
                foreach( $qna_arr as $key => $value ):
                    ?>
                    <div class="panel panel-default panel-candb">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?=$i?>" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                    <?=$key;?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse_<?=$i?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <div class="img-wrap" style="margin-bottom: 20px;">
                                    <img src="../../static/img/eye/eye_qna_ill<?= $i ?>.png" alt=""/>
                                </div>
                                <?=$value;?>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
                endforeach;
                ?>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>
            콘택트렌즈의 사용과 관리에 대해서 더 알아보세요!
        </h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="lens_keeping.php">
                    <img src="../../static/img/eye/eye_1st_icon4.png" alt=""/>
                    렌즈 보관법
                </a>
            </li>
            <li class="btn-square">
                <a href="lens_makeup.php">
                    <img src="../../static/img/eye/eye_1st_icon5.png" alt=""/>
                    안전한 화장법
                </a>
            </li>
            <li class="btn-square">
                <a href="lens_warning.php">
                    <img src="../../static/img/eye/eye_1st_icon6.png" alt=""/>
                    주의해야할 안질환
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>