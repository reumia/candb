<?php
$theme = 'cscenter';
$title = '고객센터';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
        include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    고객상담
                </h2>
                <h3 class="color-default">
                    각 분야별 전문 담당자가 운영하고 있는 상담실입니다.<br/>
                    상담 전에 <span class="color-theme">FAQ</span>를 살펴보시면 좀 더 편하게 이용하실 수 있습니다.
                </h3>
                <div class="board">
                    <div class="board-category">
                        <label>
                            <input type="radio" value="" name="category" checked/>
                            전체
                        </label>
                        <label>
                            <input type="radio" value="" name="category"/>
                            개인회원 상담
                        </label>
                        <label>
                            <input type="radio" value="" name="category"/>
                            쇼핑몰 상담
                        </label>
                        <label>
                            <input type="radio" value="" name="category"/>
                            렌즈 상담
                        </label>
                        <label>
                            <input type="radio" value="" name="category"/>
                            관리용액 상담
                        </label>
                    </div>
                    <table class="board-table">
                        <thead>
                        <tr>
                            <th class="color-theme">번호</th>
                            <th class="color-theme">제목</th>
                            <th class="color-theme">날짜</th>
                            <th class="color-theme">조회</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        for( $i = 10; $i > 0; $i-- ){
                            ?>
                            <tr>
                                <td><?=$i;?></td>
                                <td><a href="qna_view.php">테스트입니다. 테스트입니다.</a></td>
                                <td>0000.00.00</td>
                                <td>00</td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <ul class="board-pagination">
                        <li><a href="#">&lt;</a></li>
                        <li><a href="#" class="active">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">&gt;</a></li>
                    </ul>
                    <hr/>
                    <div class="row">
                        <div class="col col-xs-6">
                            <div class="board-btns">
                                <a class="btn btn-default" href="qna_write.php">질문하기</a>
                            </div>
                        </div>
                        <div class="col col-xs-6">
                            <div class="board-search text-right">
                                <select class="form-control" name="" id=""></select>
                                <input class="form-control" type="text"/>
                                <button class="btn btn-default">검색</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>