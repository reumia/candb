<section class="content-breadcrumb">
    <?=$title;?>
</section>
<section class="content-header content-header--sub clearfix">
    <h1><?=$title;?></h1>
    <ul class="tabs">
        <li class="tab"><a href="notice.php">공지사항 &amp; 이벤트</a></li>
        <li class="tab"><a href="qna.php">고객상담</a></li>
        <li class="tab"><a href="faq.php">FAQ</a></li>
    </ul>
</section>