<?php
$theme = 'cscenter';
$title = '고객센터';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
        include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    고객상담
                </h2>
                <div class="board">
                    <form action="">
                        <div class="board-theme">
                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col col-xs-2 color-theme">
                                    <label for="">제목</label>
                                </div>
                                <div class="col col-xs-10">
                                    <input class="form-control" type="text"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-xs-2 color-theme">
                                    <label for="">카테고리</label>
                                </div>
                                <div class="col col-xs-10">
                                    <select class="form-control" name="" id=""></select>
                                </div>
                            </div>
                            <hr/>
                            <textarea class="form-control" name="" id="" cols="30" rows="10"></textarea>
                        </div>
                        <hr/>
                        <div class="board-btns">
                            <button class="btn btn-default" type="submit">작성</button>
                            <a class="btn btn-default" href="#" onclick="history.back();">취소</a>
                        </div>
                    </form>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>