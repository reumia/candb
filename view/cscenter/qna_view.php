<?php
$theme = 'cscenter';
$title = '고객센터';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <?php
        include_once 'header.php';
        ?>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    고객상담
                </h2>
                <div class="board">
                    <table class="board-table">
                        <thead>
                        <tr>
                            <th colspan="8">테스트입니다. 테스트입니다.</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th class="color-theme">번호</th>
                            <td>1</td>
                            <th class="color-theme">작성자</th>
                            <td>김진우</td>
                            <th class="color-theme">날짜</th>
                            <td>0000.00.00</td>
                            <th class="color-theme">처리구분</th>
                            <td>답변완료</td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="board-content">
                        <p>않는 놀이 웅대한 그들은 살았으며, 청춘의 그들에게 그리하였는가? 위하여, 우리 우는 청춘 간에 아니다. 그들은 얼마나 넣는 것이다. 그들의 타오르고 커다란 것은 내려온 얼마나 것이다. 별과 천고에 꽃이 하였으며, 방황하여도, 그들의 기관과 있는가? 찬미를 피는 더운지라 그들을 가슴이 것이다. 풀밭에 꽃이 없으면, 사랑의 내는 소담스러운 그들의 이것이다. 천고에 사는가 이 산야에 칼이다. 모래뿐일 인생을 풍부하게 사막이다. 뼈 우리 튼튼하며, 있으며, 것이다. 기쁘며, 같은 스며들어 하는 우리 되려니와, 뛰노는 그들의 약동하다.</p>
                        <p>풀밭에 것은 불어 찾아다녀도, 청춘을 어디 고동을 황금시대다. 인간이 가슴에 보이는 역사를 약동하다. 어디 별과 전인 있으며, 황금시대를 청춘의 바이며, 것이다. 온갖 청춘 전인 보라. 가치를 불어 힘차게 두손을 그들은 교향악이다. 아니한 청춘의 봄바람을 쓸쓸한 용감하고 싸인 것이다. 곳으로 얼음에 길지 인생에 ? 무엇을 사랑의 그것을 얼음과 산야에 어디 거친 것이다. 아름답고 청춘을 것은 되려니와, 위하여, 황금시대다. 황금시대를 없으면 바이며, 하는 사람은 스며들어 커다란 듣는다. 간에 그러므로 목숨을 만물은 발휘하기 위하여, 있으랴?</p>
                        <p>별과 우리의 구하기 모래뿐일 노년에게서 무한한 아름다우냐? 안고, 보내는 밝은 귀는 내려온 가지에 청춘은 때문이다. 수 따뜻한 구하지 방황하였으며, 청춘은 소담스러운 인생에 사막이다. 인간의 그림자는 넣는 유소년에게서 아니더면, 못할 착목한는 가는 아름다우냐? 꽃 얼마나 청춘 얼음이 품으며, 찾아 칼이다. 불어 옷을 꽃이 것이다.보라, 많이 동산에는 않는 약동하다. 되는 없으면 물방아 듣는다. 하여도 우리의 창공에 간에 그들의 황금시대다. 그것을 맺어, 그들의 미인을 청춘의 못할 따뜻한 칼이다. 실로 있음으로써 새 보이는 듣는다.</p>
                    </div>
                    <div class="board-theme color-theme">
                        <h3>답변</h3>
                        <p>않는 놀이 웅대한 그들은 살았으며, 청춘의 그들에게 그리하였는가? 위하여, 우리 우는 청춘 간에 아니다. 그들은 얼마나 넣는 것이다. 그들의 타오르고 커다란 것은 내려온 얼마나 것이다. 별과 천고에 꽃이 하였으며, 방황하여도, 그들의 기관과 있는가? 찬미를 피는 더운지라 그들을 가슴이 것이다. 풀밭에 꽃이 없으면, 사랑의 내는 소담스러운 그들의 이것이다. 천고에 사는가 이 산야에 칼이다. 모래뿐일 인생을 풍부하게 사막이다. 뼈 우리 튼튼하며, 있으며, 것이다. 기쁘며, 같은 스며들어 하는 우리 되려니와, 뛰노는 그들의 약동하다.</p>
                    </div>
                    <hr/>
                    <div class="board-btns">
                        <a class="btn btn-default" href="qna.php">목록으로</a>
                    </div>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>