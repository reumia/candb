<?php
$theme = 'other popup';
$title = '교환신청';
include_once '../inc/header_popup.php';
?>
    <article class="site-content common-width">
    <section class="content-breadcrumb">
        <?=$title;?>
    </section>
    <section class="content-body clearfix">

    <!-- form 시작 -->
    <form action="">
        <section class="content-section">
            <table class="table-form">
                <tbody>
                <tr>
                    <th>주문번호</th>
                    <td>
                        <input type="text" class="form-control" placeholder="00000000" disabled>
                    </td>
                </tr>
                <tr>
                    <th>제품명</th>
                    <td>
                        <input type="text" class="form-control" placeholder="심플러스 여행용 세트">
                    </td>
                </tr>
                <tr>
                    <th>이름</th>
                    <td>
                        <input type="text" class="form-control" placeholder="홍길동" disabled>
                    </td>
                </tr>
                <tr>
                    <th>아이디</th>
                    <td>
                        <input type="text" class="form-control" placeholder="fatherisfather" disabled>
                    </td>
                </tr>
                <tr>
                    <th>이메일</th>
                    <td>
                        <input type="email" class="form-control" placeholder="son@father.com">
                    </td>
                </tr>
                <tr>
                    <th>연락처</th>
                    <td>
                        <input type="text" class="form-control" placeholder="000-0000-0000" disabled>
                    </td>
                </tr>
                <tr>
                    <th>교환사유</th>
                    <td>
                        <textarea class="form-control" name="" id="" cols="30" rows="10"></textarea>
                    </td>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <td>
                        위의 주문번호에 해당하는 주문을 교환 신청합니다. <br/>
                        반품은 배송완료 후 7일 이내에 가능합니다.
                    </td>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <td>
                        <button class="btn btn-success" type="submit">확인</button>
                        <button class="btn btn-default" type="button" onclick="window.close()">닫기</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </section>
    </form>
    <!-- form 끝 -->

    </section>
    </article>
<?php
include_once '../inc/footer_popup.php';
?>