<?php
$theme = 'other popup';
$title = '우편번호 찾기';
include_once '../inc/header_popup.php';
?>
    <article class="site-content common-width">
    <section class="content-breadcrumb">
        <?=$title;?>
    </section>
    <section class="content-body clearfix">

    <!-- form 시작 -->
    <form action="">
        <section class="content-section">
            <h3>찾으시고자 하는 주소의 동을 입력하세요.</h3>
            <table class="table-form">
                <tbody>
                <tr>
                    <td>
                        <input type="text" class="form-control" placeholder="예) 삼성동, 역삼동, 방배2동">
                    </td>
                </tr>
                <tr>
                    <td>
                        <!--
                        <button class="btn btn-success" type="submit">검색</button>
                        -->
                        <a class="btn btn-success" href="zipcode_result.php">검색</a>
                        <button class="btn btn-default" type="button" onclick="window.close()">닫기</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </section>
    </form>
    <!-- form 끝 -->

    </section>
    </article>
<?php
include_once '../inc/footer_popup.php';
?>