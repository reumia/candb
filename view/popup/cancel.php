<?php
$theme = 'other popup cancel';
$title = '주문취소/반품/교환처리현황';
include_once '../inc/header_popup.php';
?>
    <article class="site-content common-width">
    <section class="content-breadcrumb">
        <?=$title;?>
    </section>
    <section class="content-body clearfix">

    <!-- form 시작 -->
    <form action="">
        <section class="content-section">
            <table class="board-table">
                <thead>
                <tr>
                    <th class="color-theme">주문</th>
                    <th class="color-theme">접수일</th>
                    <th class="color-theme">주문번호</th>
                    <th class="color-theme">취소/반품/교환제품</th>
                    <th class="color-theme">처리여부</th>
                    <th class="color-theme">처리일</th>
                </tr>
                </thead>
                <tbody>
                <?php
                for ( $i=0; $i<4; $i++ ){
                    ?>
                    <tr>
                        <td>교환</td>
                        <td>0000.00.00</td>
                        <td>00000000</td>
                        <td>심플러스 여행용 세트</td>
                        <td>처리중</td>
                        <td>0000.00.00</td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <ul class="board-pagination">
                <li><a href="#">&lt;</a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">&gt;</a></li>
            </ul>
        </section>
    </form>
    <!-- form 끝 -->

    </section>
    </article>
<?php
include_once '../inc/footer_popup.php';
?>