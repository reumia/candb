<?php
$theme = 'other popup';
$title = '아이디 중복확인';
include_once '../inc/header_popup.php';
?>
    <article class="site-content common-width">
    <section class="content-breadcrumb">
        <?=$title;?>
    </section>
    <section class="content-body clearfix">

    <!-- form 시작 -->
    <form action="">
        <section class="content-section">
            <p>
                <strong class="color-theme">입력한 아이디</strong>는 사용가능한 아이디입니다. <br/>
                사용하시려면 사용하기 버튼을, 다른 아이디를 검색하시려면 검색 버튼을 이용하세요.
            </p>
            <p>
                영문소문자 및 수자만을 사용하여 4~10자를 입력해 주세요. <br/>
                공백 및 특수문자는 사용하실 수 없습니다.
            </p>
            <table class="table-form">
                <tbody>
                <tr>
                    <td>
                        <input type="text" class="form-control" placeholder="아이디 입력">
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-success" type="submit">검색</button>
                        <button class="btn btn-success" type="button">사용하기</button>
                        <button class="btn btn-default" type="button" onclick="window.close()">닫기</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </section>
    </form>
    <!-- form 끝 -->

    </section>
    </article>
<?php
include_once '../inc/footer_popup.php';
?>