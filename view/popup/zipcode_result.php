<?php
$theme = 'other popup';
$title = '우편번호 찾기 결과';
include_once '../inc/header_popup.php';
?>
    <article class="site-content common-width">
    <section class="content-breadcrumb">
        <?=$title;?>
    </section>
    <section class="content-body clearfix">

    <!-- form 시작 -->
    <form action="">
        <section class="content-section">
            <h3>찾으시고자 하는 주소의 동을 입력하세요.</h3>
            <table class="table-form">
                <tbody>
                <tr>
                    <td>
                        <input type="text" class="form-control" placeholder="예) 삼성동, 역삼동, 방배2동">
                    </td>
                </tr>
                <tr>
                    <td>
                        <select class="form-control" name="" id="" multiple size="4">
                            <option value="">결과 1</option>
                            <option value="">결과 2</option>
                            <option value="">결과 3</option>
                            <option value="">결과 4</option>
                            <option value="">결과 5</option>
                            <option value="">결과 6</option>
                            <option value="">결과 7</option>
                            <option value="">결과 8</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-success" type="submit">다시 검색</button>
                        <button class="btn btn-success" type="submit">확인</button>
                        <button class="btn btn-default" type="button" onclick="window.close()">닫기</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </section>
    </form>
    <!-- form 끝 -->

    </section>
    </article>
<?php
include_once '../inc/footer_popup.php';
?>