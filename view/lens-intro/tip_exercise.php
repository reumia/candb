<?php
    $theme = 'lens-intro exercise';
    $title = '순목 운동';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">C&amp;B 렌즈소개 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1><?=$title;?></h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h3 class="color-default">
                눈깜박임운동(Blinking Exercise)은 렌즈착용 중 눈을 깜박일 때 마다 느껴지는 눈꺼풀의 자극감과 렌즈의 움직임(이물감)에 대한 적응으로 가장 보편적이며 쉬운 렌즈적응 훈련방법입니다.
            </h3>
            <p>
                RGP렌즈는 소프트렌즈에 비해 월등한 장점이 있는 반면에 반드시 초기 1~2주 정도의 적응기간을 필요로 하는
                불편함이 있습니다. 그러나 적응대상을 잘 알고 효과적인 적응훈련을 해 주면 훨씬 수월하게 적응하실 수 있게
                됩니다. 초기 적응대상은 렌즈착용 중 눈을 깜박일 때마다 느껴지는 윗 눈꺼풀의 자극감 극복과 렌즈의 움직임에
                대한 적응입니다.
            </p>
            <p>
                RGP렌즈를 처음 착용할 때 이물감 또는 불편을 느끼게 되는 것은 렌즈가 딱딱 하기 때문이 아니라 렌즈의
                가장자리와 눈꺼풀 주변부(속눈썹 뿌리 부분)에서 닿을 때 느끼는 자극감 때문입니다. <br/>
                특히 속 눈썹 뿌리 부분은 눈 중에서도 가장 예민한 부위입니다. 속 눈썹 끝에 조그만 티끌이 있어도 속 눈썹 뿌리
                부분으로 전달되어 반사적으로 눈을 깜박 거리게 됩니다.
            </p>
            <p>
                소프트렌즈는 렌즈의 직경이 크기 때문에 렌즈의 가장자리가 눈꺼풀 속으로 들어가서 이물감이 거의 느껴지지
                않으나, 그 대신 눈물 순환이 안되고 노폐물이 빠져 나가지 못해 병을 유발하게 되는 것입니다.
            </p>
        </section>
        <section class="content-section">
            <h2>
                순목 운동<br class="only-pc"/>
                5 Steps
            </h2>
            <h3 class="color-default">
                눈깜박임운동(Blinking Exercise)에 대해서 설명해 드리겠습니다.
            </h3>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_blinking_ill1.png" alt=""/>
                    <h3>1. Relax</h3>
                    <p>
                        전신 특히 눈 주위 와 이마 근육의 긴장을 풉니다.
                    </p>
                    <p class="color-theme" style="margin-bottom: 0"><b>주의</b></p>
                    <p class="color-theme">
                        <small>
                            미간에 주름지게 눈을 뜨지 않습니다. <br/>
                            이마가 들리게 눈을 뜨지 않습니다.
                        </small>
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_blinking_ill2.png" alt=""/>
                    <h3>2. Close</h3>
                    <p>
                        잠이 올 때처럼 한번에 눈을 감습니다.
                    </p>
                    <p class="color-theme" style="margin-bottom: 0"><b>주의</b></p>
                    <p class="color-theme">
                        <small>
                            너무 힘을 줘서 눈을 감지 않습니다.
                        </small>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_blinking_ill3.png" alt=""/>
                    <h3>3. Pause</h3>
                    <p>
                        눈을 감은 상태로 잠시 멈춥니다. 너무 오래 멈출 필요는
                        없지만 반드시 멈춤이 있어야 합니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_blinking_ill4.png" alt=""/>
                    <h3>4. Open</h3>
                    <p>
                        눈을 자신 있게 뜹니다. 이 때 머뭇거리지 말고 한 번에
                        뜨도록 노력합니다. 이 때 렌즈가 위에서 각막 한
                        가운데로 내려오는 것이 느껴집니다.
                    </p>
                    <p class="color-theme" style="margin-bottom: 0"><b>주의</b></p>
                    <p class="color-theme">
                        <small>
                            눈을 다 뜨지 않아 렌즈가 눈꺼풀 가운데에 걸려있게 하지 않습니다.<br/>
                            렌즈를 의식해서 눈을 너무 크게 뜨지 않습니다.
                        </small>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_blinking_ill5.png" alt=""/>
                    <h3>5. Pause</h3>
                    <p>
                        눈을 감은 상태로 잠시 멈춥니다. 너무 오래 멈출 필요는
                        없지만 반드시 멈춤이 있어야 합니다.
                    </p>
                </div>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>렌즈를 바르고 안전하게 사용하는 방법에 대해 더 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="tip_usage.php">
                    <img src="../../static/img/lens/lens_1st_icon4.png" alt=""/>
                    렌즈 사용 방법
                </a>
            </li>
            <li class="btn-square">
                <a href="tip_management.php">
                    <img src="../../static/img/lens/lens_1st_icon5.png" alt=""/>
                    렌즈 관리 방법
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>