<?php
    $theme = 'lens-intro contex';
    $title = '각막굴절 교정렌즈';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">C&amp;B 렌즈소개 ></span> <?=$title;?>
    </section>
    <section class="content-header clearfix">
        <div class="lens-header">
            <p>
                Contex, Inc.
            </p>
            <img class="lens-header-logo" src="../../static/img/lens/lens_ok_logo.png" alt=""/>
            <hr/>
            <h1>
                교정렌즈의 효시, 수면착용 시력교정렌즈
            </h1>
            <h3>
                Contex OK 렌즈는 미국 Contex사에서 개발한 수면착용 시력교정용 렌즈로 <br class="only-pc"/>
                잠자는 동안 시력을 교정해주어 일상생활 중 별도의 안경이나 렌즈를 착용하지 않고도 <br class="only-pc"/>
                자유롭게 활동할 수 있도록 해줍니다.
            </h3>
            <div class="lens-header-icons row">
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_ok_icon1.png" alt=""/>
                    <p>
                        수면 착용<br class="only-pc"/>
                        시력 교정 렌즈
                    </p>
                </div>
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_ok_icon2.png" alt=""/>
                    <p>
                        탁월한 안정성과<br class="only-pc"/>
                        편의성
                    </p>
                </div>
            </div>
            <img class="lens-header-lens" src="../../static/img/lens/lens_ok_lens.png" alt=""/>
        </div>
    </section>
    <section class="content-body clearfix">
        <section class="content-section content-section--bg clearfix">
            <h2>이런 분에게 <br class="only-pc"/>필요합니다.</h2>
            <div class="row">
                <div class="col col-xs-6">
                    • 초등학생에서 성인까지 근시 / 난시가 있으신 모든 분 <br/>
                    • 근시진행이 빠른 성장기 학생 <br/>
                    • 기존의 콘택트렌즈가 불편하신 분 <br/>
                    • 불편함 때문에 하드렌즈 착용을 중단하신 분 <br/>
                    • 안경착용이 어려운 특수직업인(연예인, 운동선수,
                </div>
                <div class="col col-xs-6">
                    조종사, 소방관, 비행승무원, 예술인 등) <br/>
                    • 먼지나 바람이 많은 환경에서 일하시는 분 <br/>
                    • 각막이 얇아 레이저 시력교정술을 받을 수 없거나 수술로 <br class="only-pc"/>
                    인한 합병증이 우려되시는 분 <br/>
                    • 고도근시로 근시교정수술 후 시력이 불완전한 경우
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>교정 원리</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>Contex OK렌즈의 교정 원리</h3>
                    <p>
                        엑시머레이저나 라식 등의 시력교정수술은 각막의
                        볼록한 부분을 평평하게 절삭하여 시력을 교정하나
                        Contex OK 렌즈를 이용한 각막굴절교정술은
                        원하는 도수만큼 각막의 중심부를 눌러
                        줌으로써(Sphericalization, 각막을 구와 같은
                        모양으로 변화시킴) 근시 및 난시를 교정하게
                        됩니다.
                    </p>
                    <p>
                        <br/>
                        <a class="btn btn-danger" href="contex_about.php">각막굴절교정술 더 알아보기</a>
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_ok_ill1.png" alt=""/>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>디자인</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>렌즈의 구조</h3>
                    <p>
                        OK렌즈는 일반 RGP렌즈와는 달리 복잡하고
                        흥미로운 구조를 가지고 있습니다.
                        일반 RGP렌즈는 중심부와 주변부 2개의 커브를
                        가지고 있지만 OK렌즈는 광학중심부인 BC와
                        렌즈를 각막에 밀착시키는 FC, 렌즈의 중심위치를
                        잡이주는 AC, 눈물순환을 도와주는 PC등 4개의
                        커브로 이루어져있습니다.
                        이 4개의 커브가 상호 작용하여 시력교정효과를
                        극대화 합니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_ok_ill2.png" alt=""/>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <p class="color-theme">① BC (Base Curve)</p>
                    <p class="color-theme">
                        <small>
                            각막의 중심부를 눌러 주어(각막의 볼록한 부분을 평평하게
                            눌러줍니다.) 시력을 교정하는 역할을 합니다.
                        </small>
                    </p>
                </div>
                <div class="col col-xs-6">
                    <p class="color-theme">② AC (Alignment)</p>
                    <p class="color-theme">
                        <small>
                            3세대 OK렌즈의 특징적인 커브로서 기존의 교정렌즈와 달리 렌즈의
                            위치를 주변부에서 중심부 쪽으로 잡아주어 렌즈의 중심이탈을
                            방지하고 렌즈의 착용을 편안하게 할뿐만 아니라 교정효과를
                            극대화하는 기능을 합니다.
                        </small>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <p class="color-theme">③ FC (Fitting Curve)</p>
                    <p class="color-theme">
                        <small>
                            렌즈의 음압을 형성하여 렌즈를 각막 중심부에 밀착시키는
                            기능을 합니다. (렌즈 착용 중 렌즈가 각막에 붙어 있게 하는
                            역할을 합니다.) 이 부분에는 눈물이 고여 있어 눈물주머니를
                            형성하여 더 많은 산소를 전달하고, 렌즈의 착용감을 좋게
                            합니다.
                        </small>
                    </p>
                </div>
                <div class="col col-xs-6">
                    <p class="color-theme">④ PC (Perlpheral Curve)</p>
                    <p class="color-theme">
                        <small>
                            주변부 커브로 원활한 눈물순환과 렌즈 착용시 편안함을 제공합니다.
                        </small>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>렌즈의 착용 모습</h3>
                    <p>
                        오른쪽 그림은 각막염색검사(Fluorescein Pattern)
                        후의 OK렌즈의 이상적인 Fitting Pattern을
                        보여주고 있습니다.OK렌즈의 움직임은 항상 렌즈가
                        각막중심에 위치하여 눈을 깜박일때마다 정상적으로
                        움직이고,각막에 밀착된 렌즈의 모습도 4개의
                        커브가 이상적으로 나타나고 있습니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_ok_ill3.png" alt=""/>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>장점</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>탁월한 안정성</h3>
                    <ul>
                        <li>
                            도수에 맞춰 주문 생산된 Contex OK 렌즈가 수술
                            없이 원하는 도수만큼 각막중심부를 눌러줌으로써
                            시력을 교정하기 때문에 각막의 손상이나 조직이
                            파괴될 염려가 없는 안전한 시술법 입니다.
                        </li>
                        <li>
                            가역적으로 좋지 못한 결과가 예상되거나
                            만족하지 못할 경우에는 시술을 중단하면 원래의
                            상태로 돌아가기 때문에 시술과정에서의 부작용에
                            대한 염려가 없습니다.
                        </li>
                        <li>
                            세극등현미경검사, 각막곡률검사, 각막형태검사,
                            각막염색검사등 철저한 정밀검사를 거쳐 맞춤
                            제작되므로 시술과정에서 각막에 무리가 따르지
                            않습니다.
                        </li>
                        <li>
                            소아인 경우에는 부모님의 통제가 가능하며, 렌즈
                            분실의 위험이 적고 마모될 확률이 적어 렌즈의
                            수명이 오래 갑니다.
                        </li>
                    </ul>
                </div>
                <div class="col col-xs-6">
                    <h3>뛰어난 편의성</h3>
                    <ul>
                        <li>시술 후 안경이나 렌즈를 착용할 필요없이 종일 자유롭게 활동할 수 있습니다.</li>
                        <li>Contex OK 렌즈는 맞춤렌즈로써 미국 본사에서 개개인의 눈의 특성에 맞게 주문 생산되므로 시술시간이 단축됩니다.</li>
                        <li>시력의 호전상태를 수일 내에 느낄 수 있습니다.</li>
                        <li>근시의 진행억제에 특히 효과적입니다.</li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>시술 과정</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>1. 철저한 안과적 정밀검사</h3>
                    <p>세극등현미경검사, 각막곡률검사, 각막형태검사, 각막염색검사 등</p>
                </div>
                <div class="col col-xs-6">
                    <h3>2. 미국 Contex 사에 렌즈 주문 및 제작</h3>
                    <p>필요시에는 모든 data, 및 컴퓨터 각막형태검사 등
                        입체적인 화상이 인터넷을 통하여 미국 Contex
                        사와 직접 연결되어 있어 철저한 맞춤제작을
                        합니다.</p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>3. 맞춤 렌즈 및 test 렌즈로 시술 시행</h3>
                    <p>각막형태 및 근시, 난시 도수가 평균에서 많이
                        벗어나지 않을 경우 치료시기를 단축하고 성공률을
                        높이기 위하여 test렌즈로 시술 할 수 있습니다.</p>
                </div>
                <div class="col col-xs-6">
                    <h3>4. 유지 렌즈 착용으로 시술 완료</h3>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h3>
                제품에 대해 더 궁금한<br class="only-pc"/>
                사항이 있습니까?
            </h3>
            <div class="btn-square">
                <a href="contex_qna.php">
                    <img src="../../static/img/lens/lens_ok_question.png" alt=""/>
                    질문과 답변
                </a>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>C&amp;B의 다른 렌즈들에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="rgp.php">
                    <img src="../../static/img/lens/lens_1st_icon1.png" alt=""/>
                    RGP 렌즈
                </a>
            </li>
            <li class="btn-square">
                <a href="kerasoft.php">
                    <img src="../../static/img/lens/lens_1st_icon3.png" alt=""/>
                    맞춤형 소프트 렌즈
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>