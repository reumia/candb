<?php
    $theme = 'lens-intro kerasoft';
    $title = '맞춤형 소프트 렌즈';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">C&amp;B 렌즈소개 ></span> <?=$title;?>
    </section>
    <section class="content-header clearfix">
        <div class="lens-header">
            <p>
                Contex, Inc.
            </p>
            <img class="lens-header-logo" src="../../static/img/lens/lens_kerasoft_logo.png" alt=""/>
            <hr/>
            <h1>
                특수 맞춤형 소프트 콘택트 렌즈
            </h1>
            <h3>
                Kerasoft IC는 원추각막과 같은 불규칙한 각막으로 인한 근시 및 원시, 난시의 해결을 위한 <br class="only-pc"/>
                실리콘하이드로겔 재질의 특수 맞춤형 소프트콘택트렌즈입니다.
            </h3>
            <div class="lens-header-icons row">
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_kerasoft_icon1.png" alt=""/>
                    <p>
                        완전 맞춤형<br class="only-pc"/>
                        디자인
                    </p>
                </div>
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_kerasoft_icon2.png" alt=""/>
                    <p>
                        실리콘하이드로겔<br class="only-pc"/>
                        재질
                    </p>
                </div>
            </div>
            <img class="lens-header-lens" src="../../static/img/lens/lens_kerasoft_lens.png" alt=""/>
        </div>
    </section>
    <section class="content-body clearfix">
        <section class="content-section content-section--bg clearfix">
            <h2>이런 분에게 <br class="only-pc"/>필요합니다.</h2>
            <div class="row">
                <div class="col col-xs-6">
                    • 원추각막이 있는 경우 <br/>
                    • 각막이 불규칙한 경우 <br/>
                    • 시력교정술 후 시력이 불완전한 분
                </div>
                <div class="col col-xs-6">
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>디자인</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>완전 맞춤형 구조</h3>
                    <p>
                        Kerasoft IC는 미국 Bausch&Lomb사의 특허
                        받은 디자인으로 렌즈의 중심부와 주변부의
                        곡률반경을 각각 개별적으로 조정할 수 있으며
                        개개인의 각막의 특성에 맞추어 주문 제작 됩니다.
                    </p>
                    <p>
                        가령, 원추각막 등으로 각막의 중심부가 뾰족한
                        반면 주변부가 상대적으로 평평한 경우 Kerasoft
                        IC는 이에 맞추어 렌즈 중심부와 주변부의
                        높낮이를 서로 개별적으로 조절하여 개개인의 각막
                        형태에 맞는 가장 최적의 형태로 제작되며,
                        실리콘하이드로겔 렌즈 특유의 편안함과 함께
                        우수한 시력 보정 효과를 제공합니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_kerasoft_ill1.png" alt=""/>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>장점</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>부드럽고 편안한 착용감</h3>
                    <p>
                        기존의 원추각막 등 불규칙각막을 위한 렌즈들은
                        대개 RGP렌즈로, 착용 시 이물감 등으로 적응기가
                        필요하였습니다. 이에 반해, Kerasoft IC는
                        소프트콘택트렌즈에 사용되는 재질의 한 종류인
                        실리콘하이드로겔 재질을 사용하여 적응기 없이
                        특유의 편안한 착용감을 제공합니다.
                    </p>
                    <p>
                        또한 일반적인 소프트콘택트렌즈에 비해
                        산소투과성이 월등히 좋아 보다 장시간 착용이
                        가능합니다. (DK 60 ISO/FATT)
                    </p>
                </div>
                <div class="col col-xs-6"></div>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>C&amp;B의 다른 렌즈들에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="rgp.php">
                    <img src="../../static/img/lens/lens_1st_icon1.png" alt=""/>
                    RGP 렌즈
                </a>
            </li>
            <li class="btn-square">
                <a href="contex.php">
                    <img src="../../static/img/lens/lens_1st_icon2.png" alt=""/>
                    각막굴절 교정렌즈
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>