<?php
    $theme = 'lens-intro soclear';
    $title = 'So Clear';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">C&amp;B 렌즈소개 > RGP 렌즈 ></span> <?=$title;?>
    </section>
    <section class="content-header clearfix">
        <div class="lens-header">
            <p>
                ART Optical Contact Lens, Inc.
            </p>
            <img class="lens-header-logo" src="../../static/img/lens/lens_soclear_logo.png" alt=""/>
            <hr/>
            <h1>
                국내 최초의 각공막렌즈
            </h1>
            <h3>
                국내 최초의 각공막렌즈(Semi-sclera Lens)인 So Clear는 눈의 각막 및 공막에 압력이  <br class="only-pc"/>
                균등하게 분포될 수 있도록 제작되어 편안한 착용감과 함께  <br class="only-pc"/>
                우수한 시력보정 효과를 제공합니다.
            </h3>
            <div class="lens-header-icons row">
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_soclear_icon1.png" alt=""/>
                    <p>
                        공막까지 적용되는<br class="only-pc"/>
                        큰 직경
                    </p>
                </div>
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_soclear_icon2.png" alt=""/>
                    <p>
                        탁월한<br class="only-pc"/>
                        산소 투과성
                    </p>
                </div>
            </div>
            <img class="lens-header-lens" src="../../static/img/lens/lens_soclear_lens.png" alt=""/>
        </div>
    </section>
    <section class="content-body clearfix">
        <section class="content-section content-section--bg clearfix">
            <h2>이런 분에게 <br class="only-pc"/>필요합니다.</h2>
            <div class="row">
                <div class="col col-xs-6">
                    • 렌즈 피팅이 까다로운 경우 <br/>
                    • 각막이식수술 후 <br/>
                    • 난시가 있는 경우 <br/>
                    • 기타 각막의 형태가 불규칙한 경우
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>디자인</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>일반 소프트렌즈와 유사한 큰 직경</h3>
                    <p>
                        일반적인 RGP Lens가 9mm 내외의 직경에 각막
                        부분에만 적용되는 것과는 달리 So Clear는 일반
                        소프트렌즈와 유사한 13~15mm의 직경을 가지며,
                        각막뿐만 아니라 눈의 공막(흰자위)부위까지
                        적용되게 됩니다.
                    </p>
                    <p>
                        이러한 큰 직경으로 일반적인 RGP Lens로는
                        처방이 어려운 환자나 불규칙한 각막으로 시력
                        교정이 어려운 경우 권장되며, 적절하게 처방된 So
                        Clear는 우수한 시력보정 효과와 편안한 착용감,
                        우수한 안정성을 제공하고 탁월한 산소투과성으로
                        건강한 렌즈 착용을 가능하게 해줍니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_soclear_ill1.png" alt=""/>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>장점</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>탁월한 산소 투과성</h3>
                    <p>각막은 신체의 다른 부위와 달리 혈관이 없는 조직이므로
                        산소 공급이 가장 중요 합니다. So Clear는 Boston XO2
                        재질을 사용하여 산소 투과성(DK)이 탁월하며 (Hyper DK,
                        DK 140 - ISO/FATT) 산소분압(EOP)이 높아 (19.5%,
                        맨눈 일 때 EOP 21%) 각막이 필요로 하는 산소를
                        완벽하게 공급해주므로 산소부족으로 인한 각막 질환 등이
                        생기지 않는 안전한 렌즈입니다.</p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_soclear_ill2.png" alt=""/>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>적절히 피팅된 So Clear는 :</h3>
                    <p>
                        • 우수한 시력 보정 효과 <br/>
                        • 편안한 착용감 <br/>
                        • 뛰어난 안정성 <br/>
                        • 탁월한 산소투과성을 제공합니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>특징</h2>
            <div class="row">
                <div class="col col-xs-7">
                    <table class="table table-none-width">
                        <tbody>
                        <tr>
                            <th>재질</th>
                            <td>아주 좋다</td>
                        </tr>
                        <tr>
                            <th>디자인</th>
                            <td>비구면렌즈(Aspheric)</td>
                        </tr>
                        <tr>
                            <th>산소투과성</th>
                            <td>매우 우수(DK 140, ISO/FATT)</td>
                        </tr>
                        <tr>
                            <th>특징</th>
                            <td>
                                국내최초 각공막렌즈(Semi-sclear lens),<br/>
                                불규칙한 각막 등으로 일반적인 렌즈로는 <br/>
                                피팅이 어려운 경우 권장
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>다른 RGP 렌즈들에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="rgp_supervision.php">
                    <img src="../../static/img/lens/lens_supervision_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_envision.php">
                    <img src="../../static/img/lens/lens_envision_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_thinsite.php">
                    <img src="../../static/img/lens/lens_thinsite_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_achievement.php">
                    <img src="../../static/img/lens/lens_achievement_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_supervisiont.php">
                    <img src="../../static/img/lens/lens_supervisiont_big.png" alt=""/>
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>