<?php
    $theme = 'lens-intro rgp';
    $title = 'RGP 렌즈';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">C&amp;B 렌즈소개 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1><?=$title;?></h1>
        <h3>
            (Rigid Gas Permeable Contact Lens, 산소 투과성 하드렌즈)
        </h3>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <p>
                눈은 우리 인체의 가장 중요한 감각 기관 중의 하나이며 그 중 각막(검은 눈동자)은 외부에서 발생하는 정보를 가장 먼저
                받아들입니다. 이렇게 중요한 각막은 신체의 다른 조직과는 달리 혈관이 없는 것이 특징입니다. 결국 혈관이 공급해 주어야 하는
                산소를 각막은 거의 대부분 대기 중에서 공급받아야만 합니다.
            </p>
            <p>
                PMMA재질의 하드렌즈는 딱딱한 아크릴 플라스틱 재질로 만들어져 산소가 투과되지 않아 오래 착용하면 부작용이 많고 적응이
                어려웠습니다. 소프트렌즈가 개발되어 좋은 착용감으로 선풍적인 인기를 얻었으나 마찬가지로 재질의 산소투과성이 매우 낮아
                장시간 착용 시 눈에 부작용이 생겨 산소투과성이 높은 새로운 재질의 렌즈에 대한 필요성이 요구 되었습니다.
            </p>
            <p>
                이러한 문제를 해결하기 위해 개발된 RGP렌즈는 실리콘과 불소 화합물로서 각막에 필요한 산소가 재질을 통하여 직접 공급되는
                숨쉬는 렌즈라 할 수 있습니다. 각막에 충분한 산소가 공급됨으로 기존의 하드렌즈(PMMA렌즈) 나 소프트렌즈 착용 시 나타나는
                충혈이나 각막염 같은 부작용이 생기지 않는 안전한 렌즈입니다.
            </p>
            <a class="btn btn-default" href="rgp_about.php">더 알아보기</a>
        </section>
        <section class="content-section">
            <h2>
                C&amp;에서<br class="only-pc"/>
                소개하는<br class="only-pc"/>
                RGP 렌즈
            </h2>
            <ul class="list-component">
                <li>
                    <div class="list-component-img">
                        <img src="../../static/img/lens/lens_supervision_big.png" alt="Supervision II"/>
                    </div>
                    <div class="list-component-body">
                        <h3 class="list-component-title color-default">Supervision II</h3>
                        SupervisionⅡ는 기존 Supervision의 디자인적 특성은 그대로 유지하면서도
                        국내 최초로 산소투과성이 탁월하고 습윤성이 우수한 Boston XO2 재질을
                        사용하여 장시간 건강하고 편안한 렌즈 착용을 가능하게 해줍니다.
                    </div>
                    <a class="list-component-btn" href="rgp_supervision.php">더 알아보기</a>
                </li>
                <li>
                    <div class="list-component-img">
                        <img src="../../static/img/lens/lens_envision_big.png" alt="Boston Envision"/>
                    </div>
                    <div class="list-component-body">
                        <h3 class="list-component-title color-default">Boston Envision</h3>
                        Boston Envision은 특허 받은 이중 비구면 디자인의 RGP렌즈로 각막의
                        해부학적 구조와 일치하는 이상적인 디자인의 렌즈입니다. Boston Envision
                        으로 건강하고 편안한 눈, 난시로부터 벗어난 눈을 체험해보세요.
                    </div>
                    <a class="list-component-btn" href="rgp_envision.php">더 알아보기</a>
                </li>

                <li>
                    <div class="list-component-img">
                        <img src="../../static/img/lens/lens_thinsite_big.png" alt="Thinsite"/>
                    </div>
                    <div class="list-component-body">
                        <h3 class="list-component-title color-default">Thinsite</h3>
                        Thinsite는 “하드렌즈는 딱딱하고 아프다.” 는 기존의 관념을 완전히 바꾸어
                        놓은, 새로운 개념의 부드럽고 가벼운 RGP렌즈입니다. 하드렌즈에 대한
                        고정관념을 떨쳐버리고 ThinSite만의 새로운 착용감을 느껴 보세요.
                    </div>
                    <a class="list-component-btn" href="rgp_thinsite.php">더 알아보기</a>
                </li>
                <li>
                    <div class="list-component-img">
                        <img src="../../static/img/lens/lens_achievement_big.png" alt="Achievement"/>
                    </div>
                    <div class="list-component-body">
                        <h3 class="list-component-title color-default">Achievement</h3>
                        맞춤형 특수비구면렌즈 Achievement는 기존의 렌즈로는 교정이 불가능했던
                        각막질환을 해결할 수 있는 콘택트렌즈 영역의 새로운 장을 열어갑니다. 80년
                        이상의 역사를 지닌 Art Optical사의 기술력, 우수한 산소투과성과 내구성의
                        Boston EO재질이 만나 만들어낸 마지막 걸작입니다.
                    </div>
                    <a class="list-component-btn" href="rgp_achievement.php">더 알아보기</a>
                </li>
                <li>
                    <div class="list-component-img">
                        <img src="../../static/img/lens/lens_supervisiont_big.png" alt="Supervision T"/>
                    </div>
                    <div class="list-component-body">
                        <h3 class="list-component-title color-default">Supervision T</h3>
                        국내 최초의 Toric RGP렌즈인 Supervision T는 안경이나 기존의 RGP렌즈
                        또는 소프트렌즈로는 해결되기 어렵거나 완전하게 교정되기 어려운
                        고도난시와 수정체난시의 교정에 탁월한 특수 RGP렌즈입니다.
                    </div>
                    <a class="list-component-btn" href="rgp_supervisiont.php">더 알아보기</a>
                </li>
                <li>
                    <div class="list-component-img">
                        <img src="../../static/img/lens/lens_soclear_big.png" alt="So Clear" />
                    </div>
                    <div class="list-component-body">
                        <h3 class="list-component-title color-default">So Clear</h3>
                        국내 최초의 각공막렌즈(Semi-scleral Lens)인 So Clear는 눈의 각막 및
                        공막에 압력이 균등하게 분포될 수 있도록 제작되어 편안한 착용감과 함께
                        우수한 시력보정 효과를 제공합니다.
                    </div>
                    <a class="list-component-btn" href="rgp_soclear.php">더 알아보기</a>
                </li>
            </ul>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>C&amp;B의 다른 렌즈들에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="contex.php">
                    <img src="../../static/img/lens/lens_1st_icon2.png" alt=""/>
                    각막굴절 교정렌즈
                </a>
            </li>
            <li class="btn-square">
                <a href="kerasoft.php">
                    <img src="../../static/img/lens/lens_1st_icon3.png" alt=""/>
                    맞춤형 소프트 렌즈
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>