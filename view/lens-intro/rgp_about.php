<?php
    $theme = 'lens-intro about';
    $title = '더 알아보기';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">C&amp;B 렌즈소개 > RGP 렌즈 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1>RGP 렌즈에 대해 더 알아봅시다</h1>
        <h3>(Rigid Gas Permeable Contact Lens, 산소 투과성 하드렌즈)</h3>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <p>
                RGP렌즈는 일반 하드렌즈, 소프트렌즈에 비해 높은 산소 투과성과 안전성 / 내구성 등 여러가지 측면에서
                장점을 가지고 있습니다. 또한 구면 / 비구면렌즈는 물론, 토릭 렌즈나 다중초점 렌즈, 역기하 렌즈 등 각기 장점이 다른 다양한 종류의 렌즈로 구성되어 있어,
                각막의 상태에 맞는 렌즈를 선택하여 높은 시력 교정효과는 물론 근시 진행 억제나 중증도 이상의 난시교정 등 다양한 효과를 기대할 수 있습니다.
            </p>
        </section>
        <section class="content-section">
            <h2>RGP렌즈의 <br class="only-pc"/>장점</h2>
            <ul class="btn-wrap" style="margin-top: 0;">
                <li class="btn-square disabled">
                    <a href="#">
                        <h1>1</h1>
                        <small>시력 교정 효과가 탁월합니다.</small>
                    </a>
                </li>
                <li class="btn-square disabled">
                    <a href="#">
                        <h1>2</h1>
                        <small>산소 투과성이 높아 눈에 가장 안전합니다.</small>
                    </a>
                </li>
                <li class="btn-square disabled">
                    <a href="#">
                        <h1>3</h1>
                        <small>안구 건조증이 잘 생기지 않습니다.</small>
                    </a>
                </li>
                <li class="btn-square disabled">
                    <a href="#">
                        <h1>4</h1>
                        <small>중등도 이상의 난시에도 교정 효과가 탁월합니다.</small>
                    </a>
                </li>
                <li class="btn-square disabled">
                    <a href="#">
                        <h1>5</h1>
                        <small>렌즈관리가 편리합니다.</small>
                    </a>
                </li>
                <li class="btn-square disabled">
                    <a href="#">
                        <h1>6</h1>
                        <small>오래 착용하여도 편안합니다.</small>
                    </a>
                </li>
                <li class="btn-square disabled">
                    <a href="#">
                        <h1>7</h1>
                        <small>렌즈 재질의 안정성과 내구성이 좋아 경제적입니다.</small>
                    </a>
                </li>
                <li class="btn-square disabled">
                    <a href="#">
                        <h1>8</h1>
                        <small>불규칙한 각막에서나 수술 후에도 시력 교정이 가능합니다.</small>
                    </a>
                </li>
                <li class="btn-square disabled">
                    <a href="#">
                        <h1>9</h1>
                        <small>근시의 진행을 억제합니다.</small>
                    </a>
                </li>
            </ul>
        </section>
        <section class="content-section">
            <h2>RGP렌즈의 <br class="only-pc"/>종류</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>구면(Spheric) 렌즈</h3>
                    <p>
                        가장 일반적인 형태의 렌즈로 렌즈의 바깥쪽과 안쪽 모두
                        구형의 면으로 이루어진 콘택트렌즈를 말합니다. 구형의
                        면(구면)이란, 둥근 수박의 한 쪽 면을 잘랐을 때 얻을 수 있는
                        조각의 면을 생각하면 됩니다. <br/>
                        구면렌즈는 일반적인 근시(Myopia) 및 원시(Hyperopia)의
                        교정에 사용되며 렌즈의 피팅이 비교적 쉬우면서도 교정효과가
                        좋다는 장점이 있습니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>비구면(Aspheric) 렌즈</h3>
                    <p>
                        우리 눈의 각막은 중심부 부분은 가파르고 주변부로 갈수록
                        편평해지는 형태를 지니고 있습니다. 즉, 정확한 구면이 아닌
                        비구면의 형태를 가지는데 비구면렌즈는 이러한 각막의 해부
                        학적 형태에 근접한 모양을 가져 보다 편안한 착용감과 함께
                        우수한 시력교정효과를 제공합니다. <br/>
                        비구면렌즈는 구면렌즈에 비해 제작 및 피팅이 상대적으로
                        까다롭지만, 다양한 표현이 가능하여 여러 가지 형태의
                        굴절이상에 적용할 수 있습니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>토릭(Toric) 렌즈</h3>
                    <p>
                        둥근 수박의 한 쪽 면을 자르게 되면 구형의 한 쪽 면을 얻을 수
                        있는데 반해, 도넛과 같은 원환체의 한 쪽 면을 자르게 되면
                        가로와 세로의 길이가 다른 타원형의 한 쪽 면을 얻을 수
                        있습니다. 전자의 둥근 구형의 면을 가진 렌즈를 면렌즈라하며,
                        가로와 세로의 길이가 다른 타원형의 면을 가진 렌즈를
                        토릭렌즈라 합니다. 토릭렌즈는 렌즈의 가로 방향과 세로
                        방향에 각기 다른 굴절률을 생성하여 각막의 서로 다른
                        곡률로부터 발생하는 난시를 교정할 수 있습니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>
                        이중초점(Bifocal) 혹은 <br class="only-pc"/>
                        다중초점(Multifocal) 렌즈
                    </h3>
                    <p>
                        이중초점 렌즈와 다중초점렌즈는 모두 노안 교정을 위한
                        렌즈입니다. 이중 초점 렌즈의 경우 하나의 렌즈에 근거리
                        보기를 위한 부분과 원거리 보기를 위한 부분이 같이 있는
                        렌즈로 책이나 신문을 볼 때는 근거리 보기를 위한 부분을 통해,
                        멀리 있는 사물을 볼 때는 원거리 보기를 위한 부분을 사용하게
                        됩니다. <br/>
                        다중초점렌즈는 누진다초점렌즈와 비슷한 개념의 렌즈로
                        이중초점 렌즈를 포함해 하나의 렌즈에 하나 이상의 도수가
                        들어가 있는 렌즈를 모두 포함하여 지칭합니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>역기하(Reverse Geometry) 렌즈</h3>
                    <p>
                        기하학적인 구조가 일반적인 렌즈와는 정반대인 렌즈로 렌즈의 중심부 부분이 편평한 형태를 보입니다. 이러한 디자인적인 특성으로 주로 각막의 중심부를 눌러 시력을 교정하는 각 막굴절교정술(Orthokeratology에 사용되며, 각막굴절교정술에 사용되는 무수술 시력교정렌즈
                        인 Contex OK Lens도 이러한 역기하렌즈에 속합니다.
                    </p>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>RGP 렌즈와 <br class="only-pc"/>소프트 렌즈</h2>
            <table class="table-custom">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th class="color-theme">RGP 렌즈</th>
                    <th>소프트 렌즈</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>시력교정효과</td>
                    <td class="td-theme">탁월하다</td>
                    <td>좋다</td>
                </tr>
                <tr>
                    <td>산소투과성</td>
                    <td class="td-theme">아주 높다</td>
                    <td>낮다 (간접공급)</td>
                </tr>
                <tr>
                    <td>재질변화</td>
                    <td class="td-theme">거의 없다</td>
                    <td>있다</td>
                </tr>
                <tr>
                    <td>렌즈수명</td>
                    <td class="td-theme">2년 이상 가능</td>
                    <td>1년 미만</td>
                </tr>
                <tr>
                    <td>제조방법</td>
                    <td class="td-theme">개인에 따라 주문제작</td>
                    <td>기성품</td>
                </tr>
                <tr>
                    <td>이물질 침착</td>
                    <td class="td-theme">잘 안됨</td>
                    <td>잘됨</td>
                </tr>
                <tr>
                    <td>난시교정</td>
                    <td class="td-theme">완전</td>
                    <td>불완전</td>
                </tr>
                <tr>
                    <td>근시진행억제</td>
                    <td class="td-theme">좋다</td>
                    <td>없다</td>
                </tr>
                </tbody>
            </table>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>C&amp;B의 다른 렌즈들에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="contex.php">
                    <img src="../../static/img/lens/lens_1st_icon2.png" alt=""/>
                    각막굴절 교정렌즈
                </a>
            </li>
            <li class="btn-square">
                <a href="kerasoft.php">
                    <img src="../../static/img/lens/lens_1st_icon3.png" alt=""/>
                    맞춤형 소프트렌즈
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>