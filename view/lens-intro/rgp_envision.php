<?php
    $theme = 'lens-intro envision';
    $title = 'Boston Envision';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">C&amp;B 렌즈소개 > RGP 렌즈 ></span> <?=$title;?>
    </section>
    <section class="content-header clearfix">
        <div class="lens-header">
            <p>
                ART Optical Contact Lens, Inc.
            </p>
            <img class="lens-header-logo" src="../../static/img/lens/lens_envision_logo.png" alt=""/>
            <hr/>
            <h1>우리 눈을 닮은 완벽한 난시교정 렌즈</h1>
            <h3>
                Boston Envision은 특허 받은 이중 비구면 디자인의 RGP렌즈로 <br class="only-pc"/>
                각막의 해부학적 구조와 일치하는 이상적인 디자인의 렌즈입니다. <br class="only-pc"/>
                Boston Envision으로 건강하고 편안한 눈, 난시로부터 벗어난 눈을 체험해보세요!
            </h3>
            <div class="lens-header-icons row">
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_envision_icon1.png" alt=""/>
                    <p>
                        각막의 구조와 일치하는<br class="only-pc"/>
                        이중 비구면 디자인
                    </p>
                </div>
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_envision_icon2.png" alt=""/>
                    <p>
                        탁월한 난시교정 효과
                    </p>
                </div>
            </div>
            <img class="lens-header-lens" src="../../static/img/lens/lens_envision_lens.png" alt=""/>
        </div>
    </section>
    <section class="content-body clearfix">
        <section class="content-section content-section--bg clearfix">
            <h2>이런 분에게 <br class="only-pc"/>필요합니다.</h2>
            <div class="row">
                <div class="col col-xs-6">
                    • 중등도 이상의 난시가 있는 분 <br/>
                    • 기존의 RGP렌즈 적응에 실패 하신 분 <br/>
                    • 소프트렌즈 착용 시 잦은 부작용으로 고생하시는 분
                </div>
                <div class="col col-xs-6">
                    • 눈이 예민하여 기존의 렌즈로 실패 하신 분 <br/>
                    • 원추각막, 각막이식수술 후 불규칙 난시가 있으신 분 <br/>
                    • 장시간 렌즈착용을 필요로 하시는 분 <br/>
                    • 눈이 자주 건조하신 분
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>디자인</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>각막의 해부학적 구조와 일치하는 RGP 렌즈</h3>
                    <p>
                        각막은 중심부, 중간 주변부 그리고 서로 다른 편심률로 되어 있습니다. Boston Envision은 중심부와 주변부가 서로 다른 편심률을 가진 이중비구면(Bi-Aspheric Curve)으로 되어 있어 서 각막의 해부학적 구조와 일치하는 이상적인 RGP렌즈입니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_envision_ill1.png" alt=""/>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <p>
                        ￼렌즈의 Fitting 상태를 검사하기 위한 눈물염색검사 사진을 보면, 기존 구면 RGP 렌즈에 비해 Boston Envison은 각막에 넓게 접촉하므로 눈물분포가 골고루 퍼져 있음을 알 수 있습니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_envision_ill2.png" alt=""/>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>장점</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>독특한 구조</h3>
                    <p>
                        Boston Envision은 중심부와 주변부가 서로 다른 편심률을 가진 이중비구면(Bi-Aspheric)렌즈로서 각막의 해부학적 구조와 일치하는 이상적인 RGP 렌즈이기 때문에 렌즈착용으로 인한 각막의 변형이나 부작용이 생기지 않습니다. 또한 중심부와 주변부의 경계가 없기(junctionless) 때문에 눈물순환이 잘됨은 물론 야간에 달무리 현상도 거의 나타나지 않습니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>안전한 재질</h3>
                    <p>
                        Boston Envision은 최고의 산소투과성(DK140)과 산소분압(EOP 18%)으로 연속착용이 가능하며 또한 탁월한 내구성으로 렌즈에 변형이 생기지 않아 오랜 수명을 유지하며 수명이 다 할 때까지 원래의 형태를 유지하는 Boston XO 재질로 만들어진 안전한 렌즈입니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>최상의 편안함</h3>
                    <p>
                        Boston Envision은 각막의 해부학적 구조에 일치하여 각막에 압력이 골고루 미치기 때문에 편안한 착용감을 느낄 수 있습니다. 기존의 렌즈보다는 직경이 크기(9.6mm) 때문에 중심이탈이 되지 않고 움직임이 적어 초기 적응이 쉬울 뿐 아니라 착용 중에서도 늘 편안함을 느낄 수 있습니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>탁월한 난시교정</h3>
                    <p>
                        난시는 각막 중심부의 불규칙성으로 인하여 생기는 현상이며 이러한 불규칙한 각막을 정상과 비슷하게 만드는 과정이 난시교정 원리입니다. Boston Envision은 정상적인 각막의 해부학적 구조와 일치하기(Bi-Aspheric) 때문에 기존의 구면렌즈 또는 비구면렌즈로 잘 교정되지 않는 고도의 난시도 완벽하게 교정될 뿐 아니라 추각막이나 각막이식수술 후의 불규칙 난시에도 탁월한 교정효과가 있습니다.
                    </p>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>특징</h2>
            <table class="table-custom">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th class="color-theme">Boston Envision</th>
                    <th>비구면 렌즈</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>근시교정효과</td>
                    <td class="td-theme">탁월하다</td>
                    <td>보통</td>
                </tr>
                <tr>
                    <td>시교정효과</td>
                    <td class="td-theme">탁월하다</td>
                    <td>좋다</td>
                </tr>
                <tr>
                    <td>산소투과성(DK)</td>
                    <td class="td-theme">높다(DK140)</td>
                    <td>보통</td>
                </tr>
                <tr>
                    <td>편안함</td>
                    <td class="td-theme">아주 편안하다</td>
                    <td>편안하다</td>
                </tr>
                <tr>
                    <td>눈물순환</td>
                    <td class="td-theme">아주 좋다</td>
                    <td>좋다</td>
                </tr>
                </tbody>
            </table>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>다른 RGP 렌즈들에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="rgp_supervision.php">
                    <img src="../../static/img/lens/lens_supervision_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_thinsite.php">
                    <img src="../../static/img/lens/lens_thinsite_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_achievement.php">
                    <img src="../../static/img/lens/lens_achievement_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_supervisiont.php">
                    <img src="../../static/img/lens/lens_supervisiont_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_soclear.php">
                    <img src="../../static/img/lens/lens_soclear_big.png" alt=""/>
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>