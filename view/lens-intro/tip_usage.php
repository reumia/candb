<?php
    $theme = 'lens-intro usage';
    $title = '렌즈 사용 방법';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">C&amp;B 렌즈소개 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1><?=$title;?></h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h3>
                렌즈를 사용하는 방법은 어렵지 않습니다. <br/>
                동영상을 통해 올바른 렌즈 착용 / 빼는 방법을 알아봅니다.
            </h3>
        </section>
        <section class="content-section">
            <h2>
                렌즈<br class="only-pc"/>
                착용 방법
            </h2>
            <div class="image-wrap">
                <iframe width="748" height="400" src="https://www.youtube.com/embed/ztaEXgddURc?rel=0&vq=hd720&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
            </div>
            <h3 class="color-default" style="margin-bottom: 0;">Tips</h3>
            <ol style="margin-top: 0;">
                <li><small>착용할 때는 가능한 한 눈을 크게 벌려줍니다.</small></li>
                <li><small>렌즈를 눈동자 위에 올려놓은 다음에는 손가락을 너무 빨리 떼거나, 눈을 급히 깜박이지 않도록 합니다. 이때 렌즈가 눈에서 떨어지거나 눈 안에서 움직이는 경우가 생길 수도 있습니다.</small></li>
                <li><small>착용 후 눈을 가능한 한 적게 움직이며 천천히 깜박거립니다. 불편할 때는 눈을 비비지 말고 잠시 동안 아래를 내려다 보면 편안해 집니다.</small></li>
                <li><small>렌즈 착용은 책상이나 화장대 같은 평평한 곳에서 하얀색 수건을 깔고 합니다.</small></li>
            </ol>
        </section>
        <section class="content-section">
            <h2>
                렌즈 <br class="only-pc"/>
                빼는 방법
            </h2>
            <h3>한 손 사용법</h3>
            <div class="image-wrap">
                <iframe width="748" height="400" src="https://www.youtube.com/embed/XOR7ispmWzk?rel=0&vq=hd720&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
            </div>
            <h3>두 손 사용법</h3>
            <div class="image-wrap">
                <iframe width="748" height="400" src="https://www.youtube.com/embed/mTNF-KaljL4?rel=0&vq=hd720&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
            </div>
            <h3>흡입봉 사용법</h3>
            <div class="image-wrap">
                <iframe width="748" height="400" src="https://www.youtube.com/embed/4_2UcIwn8RA?rel=0&vq=hd720&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
            </div>
            <h3 class="color-default" style="margin-bottom: 0;">Tips</h3>
            <ol style="margin-top: 0;">
                <li><small>렌즈가 빠지지 않았으면 손가락을 떼고 처음부터 다시 시작합니다.</small></li>
                <li>
                    <small>
                        빼는 도중 렌즈가 흰자위로 갔을 경우에는 렌즈를 눈동자에 바로 위치시킨 후 처음부터 다시 시작합니다.<br/>
                        (렌즈의 잘못된 위치에 대해서는 렌즈의 위치조정 참조)
                    </small>
                </li>
            </ol>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>렌즈를 바르고 안전하게 사용하는 방법에 대해 더 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="tip_management.php">
                    <img src="../../static/img/lens/lens_1st_icon5.png" alt=""/>
                    렌즈 관리 방법
                </a>
            </li>
            <li class="btn-square">
                <a href="tip_exercise.php">
                    <img src="../../static/img/lens/lens_1st_icon6.png" alt=""/>
                    순목 운동
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>