<?php
    $theme = 'lens-intro achievement';
    $title = 'Achievement';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">C&amp;B 렌즈소개 > RGP 렌즈 ></span> <?=$title;?>
    </section>
    <section class="content-header clearfix">
        <div class="lens-header">
            <p>
                ART Optical Contact Lens, Inc.
            </p>
            <img class="lens-header-logo" src="../../static/img/lens/lens_achievement_logo.png" alt=""/>
            <hr/>
            <h1>완전 맞춤형 특수 비구면 렌즈</h1>
            <h3>
                기존 편심률의 비구면렌즈로는 교정이 불가능했던 각막질환을 해결할 수 있는 <br class="only-pc"/>
                콘택트렌즈 영역의 새로운 장을 열어갑니다. <br/>
                Achievement는 80년 이상의 역사를 지닌 Art Optical사의 기술력과 <br class="only-pc"/>
                산소투과성과 내구성의 Boston EO재질이 만나 만들어진 우수한 제품입니다. <br/>
                원추각막, 각막이식수술 후와 그 외 비구면 렌즈가 필요하다면 <br class="only-pc"/>
                Achievement만의 맑고 밝은 세상을 느껴보세요.
            </h3>
            <div class="lens-header-icons row">
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_achievement_icon1.png" alt=""/>
                    <p>
                        완전 맞춤형<br class="only-pc"/>
                        디자인
                    </p>
                </div>
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_achievement_icon2.png" alt=""/>
                    <p>
                        특수한 편심률로<br class="only-pc"/>
                        안전하게
                    </p>
                </div>
            </div>
            <img class="lens-header-lens" src="../../static/img/lens/lens_achievement_lens.png" alt=""/>
        </div>
    </section>
    <section class="content-body clearfix">
        <section class="content-section content-section--bg clearfix">
            <h2>이런 분에게 <br class="only-pc"/>필요합니다.</h2>
            <div class="row">
                <div class="col col-xs-6">
                    • 원추각막 또는 각막확장증 <br/>
                    • 근시교정수술 후 각막확장증 <br/>
                    • 백내장 수술 후 무수정체안 <br/>
                    • 각막이식수술 후
                </div>
                <div class="col col-xs-6">
                    • 불규칙 난시 <br/>
                    • 고도원시 <br/>
                    • 노안
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>디자인</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>
                        개개인의 눈의 특성에 맞게 <br class="only-pc"/>
                        완전맞춤형으로 디자인
                    </h3>
                    <p>
                        특수한 편심률(0.65, 0.8, 0.9, 1.0, negative e)을
                        사용하여 기존의 편심률로는 교정이 어려웠
                        던 각막질환 및 노안을 해결할 수 있는 혁신적인
                        콘택트렌즈 입니다. 각막질환의 특성에 따
                        른 편심률 뿐만 아니라 곡률반경과 직경 또한
                        다양하며, 필요시에는 개개인의 눈의 특성에
                        맞추어 완전 맞춤형으로 제작되는 렌즈입니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_achievement_ill1.png" alt=""/>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <p class="color-theme">렌즈의 편심률이란?</p>
                    <p class="color-theme">
                        <small>
                            비구면렌즈는 중심부에서 주변부로 갈수록 편평해지는
                            타원을 말합니다. 이때, 중심부에서 주변으로 편평해지는
                            정도 또는 주변에서 중심부로 뾰족해지는 정도를 말하는
                            것입니다. 편심률이 높아질수록 오른쪽 그림처럼 렌즈
                            중심부는 오목하여 눈물이 많이 고이게 되고 가장자리
                            들림은 높아지게 됩니다.
                        </small>
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_achievement_ill2.png" alt=""/>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>장점</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>특수한 편심률로 안전하게</h3>
                    <p>
                        편심률이 높아질수록 렌즈의 후면이 오목하게 되고
                        가장자리 들림(edge lift)은 높아지게 됩니다.
                        원추각막과 같은 각막 확장성 질환에는 병의 진행 정도와
                        일치하는 편심율의 렌즈를 착용하게 되어 렌즈가
                        각막중심에 위치하기 쉽고 원추에 압력이 거의 미치지 않아
                        각막에 상처가 생기지 않으며 장시간 착용이 가능합니다.
                        또한 가장자리 들림(edge lift)이 좋아 눈물순환이 잘 되어
                        각막에 충분한 산소공급이 되기 때문에 각막부종이나
                        상처가 생기지 않습니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_achievement_ill3.png" alt=""/>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>시력 보장</h3>
                    <p>원추각막이나 각막확장이 진행될수록 원추의 위치는
                        중심에서 아주 벗어나는 경향이 있어 심한 난시가 생겨
                        심각한 시력저하가 오며 또한 원추의 크기와 모양이 아주
                        다양하여 기존의 편심율과 작은 직경의 렌즈로는 렌즈의
                        중심잡기가 어려워 난시교정이 완벽하지 않을 뿐 아니라
                        움직임이 커져 시력이 불안정하였습니다.</p>
                </div>
                <div class="col col-xs-6">
                    <h3>원, 근거리를 동시에</h3>
                    <p>Achievement의 독특한 구조(후면비구면 또는
                        전면비구면)와 눈물을 이용하여 렌즈의 중심부와 주변부가
                        서로 다른 굴절력이 생겨 원거리와 근거리 시력이 동시에
                        좋아지는 다초점 기능이 있어 노안이 있는 경우에 원·
                        근거리를 동시에 잘 볼 수 있습니다.</p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <p>
                        Achievement는 다양한 편심율과 큰 직경으로 렌즈의
                        중심잡기가 쉽고 움직임이 적절하여 난시의 교정이 우수할
                        뿐 아니라 시력이 안정적입니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>특징</h2>
            <table class="table-custom">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th class="color-theme">Achievement</th>
                    <th>기존 비구면 렌즈</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>움직임</td>
                    <td class="td-theme">적절하다</td>
                    <td>많거나 적다</td>
                </tr>
                <tr>
                    <td>중심잡기</td>
                    <td class="td-theme">쉽다</td>
                    <td>어렵다</td>
                </tr>
                <tr>
                    <td>난시교정</td>
                    <td class="td-theme">아주 좋다</td>
                    <td>좋다</td>
                </tr>
                <tr>
                    <td>편안함</td>
                    <td class="td-theme">아주 편하다</td>
                    <td>때로 불편할 수 있다</td>
                </tr>
                <tr>
                    <td>눈물순환</td>
                    <td class="td-theme">아주 좋다</td>
                    <td>너무 많거나 적다</td>
                </tr>
                <tr>
                    <td>중심부눌림</td>
                    <td class="td-theme">가볍고 적절하다</td>
                    <td>강하다</td>
                </tr>
                <tr>
                    <td>가장자리 들림</td>
                    <td class="td-theme">적절하다</td>
                    <td>너무 높거나 낮다</td>
                </tr>
                </tbody>
            </table>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>다른 RGP 렌즈들에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="rgp_supervision.php">
                    <img src="../../static/img/lens/lens_supervision_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_envision.php">
                    <img src="../../static/img/lens/lens_envision_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_thinsite.php">
                    <img src="../../static/img/lens/lens_thinsite_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_supervisiont.php">
                    <img src="../../static/img/lens/lens_supervisiont_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_soclear.php">
                    <img src="../../static/img/lens/lens_soclear_big.png" alt=""/>
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>