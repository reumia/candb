<?php
    $theme = 'lens-intro supervisiont';
    $title = 'Supervision T';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">C&amp;B 렌즈소개 > RGP 렌즈 ></span> <?=$title;?>
    </section>
    <section class="content-header clearfix">
        <div class="lens-header">
            <p>
                ART Optical Contact Lens, Inc.
            </p>
            <img class="lens-header-logo" src="../../static/img/lens/lens_supervisiont_logo.png" alt=""/>
            <hr/>
            <h1>
                국내최초 Toric RGP Lens, <br/>
                고도난시와 수정체 난시를 위한 렌즈
            </h1>
            <h3>
                Supervision T는 기존의 렌즈로는 해결되기 어렵거나 완전하게 교정되기 어려운 고도난시와 <br class="only-pc"/>
                수정체난시를 위한 특수 RGP 렌즈입니다.
            </h3>
            <div class="lens-header-icons row">
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_supervisiont_icon1.png" alt=""/>
                    <p>
                        난시 교정을 위한<br class="only-pc"/>
                        독특한 디자인
                    </p>
                </div>
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_supervisiont_icon2.png" alt=""/>
                    <p>
                        Boston XO2<br class="only-pc"/>
                        재질 사용
                    </p>
                </div>
            </div>
            <img class="lens-header-lens" src="../../static/img/lens/lens_supervisiont_lens.png" alt=""/>
        </div>
    </section>
    <section class="content-body clearfix">
        <section class="content-section content-section--bg clearfix">
            <h2>이런 분에게 <br class="only-pc"/>필요합니다.</h2>
            <div class="row">
                <div class="col col-xs-6">
                    • 기존의 렌즈착용 시 렌즈가 중심에 잘 오지 않거나 <br class="only-pc"/>움직임이 많아 불편한 경우 <br/>
                    • 난시가 심하여 기존의 렌즈가 만족스럽지 못한 경우 <br/>
                    • 기존의 렌즈 착용 시 3시 9시 방향에 충혈이 <br class="only-pc"/>잘되어 불편한 경우
                </div>
                <div class="col col-xs-6">
                    • 착용 중인 렌즈를 빼고 안경을 착용했을 때 <br class="only-pc"/>충분한 시력이 나오지 않는 경우 <br/>
                    • 각막의 뒤틀림이 심한 경우 <br/>
                    • 렌즈 착용 후에도 잔여난시가 많이 남을 경우
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>디자인</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>난시 교정을 위한 독특한 디자인</h3>
                    <p>
                        일반적인 RGP렌즈는 평평한 경선에서 난시를
                        가감하여 한 개의 곡률반경으로만 맞추어지기
                        때문에 고도난시에 있어서는 완벽한 교정을 할 수
                        없습니다. 고도의 각막난시는 각막이 구형(좌)이
                        아니고 타원체(우)이므로 수평과 수직의 두 개의
                        경선을 가진 렌즈라야 완벽한 피팅이 가능합니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_supervisiont_ill1.png" alt=""/>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>장점</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>난시에 적합한 구조</h3>
                    <p>
                        일반적으로 각막 난시가 -2.00D가 넘어갈 경우 일반적인
                        구면 RGP 렌즈로는 교정이 어려우며 만족할만한 시력을
                        얻지 못할 수 있습니다. 또한 난시가 증가함에 따라 착용 시
                        불편함과 함께 렌즈의 피팅상태가 좋지 않을 수 있습니다.
                    </p>
                    <p>
                        Supervision T는 난시가 있는 각막에 적용되어 편안한
                        착용감을 제공하며 난시와 함께 근시 및 원시로 인한 시력
                        문제를 해결해 줍니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_supervisiont_ill2.png" alt=""/>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>최상의 편안함</h3>
                    <p>고도난시에 있어서는 수평과 수직경선의 높낮이가 다르기
                        때문에 기존의 렌즈로는 수평과 수직의 경선에 동일한
                        압력이 가해지지 않아 각막의 변형이나 불편함이 있습니다.
                        Supervision T는 수평과 수직경선에 압력이 동일하게
                        가해지므로 편안한 착용이 가능합니다.</p>
                </div>
                <div class="col col-xs-6">
                    <h3>안전한 재질</h3>
                    <p>재질의 안전성과 우수한 산소투과성, 뛰어난 내구성으로
                        최근까지 세계적으로 가장 널리 사용되는 Boston XO 및
                        Boston XO2 재질을 사용하여 장기간 렌즈착용 시
                        고도난시에서 오기 쉬운 각막변형이 생기지 않습니다.</p>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>특징</h2>
            <table class="table-custom">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th class="color-theme">Supervision T</th>
                    <th>기존 비구면 렌즈</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>근시교정효과</td>
                    <td class="td-theme">아주 좋다</td>
                    <td>좋다</td>
                </tr>
                <tr>
                    <td>난시교정효과</td>
                    <td class="td-theme">아주 좋다</td>
                    <td>좋다</td>
                </tr>
                <tr>
                    <td>이상적난시교정범위</td>
                    <td class="td-theme">2.50D 이상</td>
                    <td>1.50 ~ 2.50D</td>
                </tr>
                <tr>
                    <td>편안함</td>
                    <td class="td-theme">아주 좋다</td>
                    <td>좋다</td>
                </tr>
                </tbody>
            </table>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>다른 RGP 렌즈들에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="rgp_supervision.php">
                    <img src="../../static/img/lens/lens_supervision_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_envision.php">
                    <img src="../../static/img/lens/lens_envision_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_thinsite.php">
                    <img src="../../static/img/lens/lens_thinsite_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_achievement.php">
                    <img src="../../static/img/lens/lens_achievement_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_soclear.php">
                    <img src="../../static/img/lens/lens_soclear_big.png" alt=""/>
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>