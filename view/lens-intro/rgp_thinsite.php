<?php
    $theme = 'lens-intro thinsite';
    $title = 'Thinsite';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">C&amp;B 렌즈소개 > RGP 렌즈 ></span> <?=$title;?>
    </section>
    <section class="content-header clearfix">
        <div class="lens-header">
            <p>
                ART Optical Contact Lens, Inc.
            </p>
            <img class="lens-header-logo" src="../../static/img/lens/lens_thinsite_logo.png" alt=""/>
            <hr/>
            <h1>EASY &amp; FAST 처음 느껴본 부드러움</h1>
            <h3>
                하드렌즈는 딱딱하고 아프다는 관념을 완전히 바꾸어 놓은 Thinsite. <br/>
                최상의 착용감, 최고의 산소전달율, 최적의 습윤성, 탁월한 내구성을 갖춘 <br class="only-pc"/>
                새로운 개념의 부드럽고 가벼운 RGP렌즈입니다.<br class="only-pc"/>
                이제, 하드렌즈에 대한 고정관념을 떨쳐버리고 ThinSite만의 새로운 착용감을 느껴 보세요.
            </h3>
            <div class="lens-header-icons row">
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_thinsite_icon1.png" alt=""/>
                    <p>
                        얇고 가벼운 디자인
                    </p>
                </div>
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_thinsite_icon2.png" alt=""/>
                    <p>
                        Boston EQ 재질
                    </p>
                </div>
            </div>
            <img class="lens-header-lens" src="../../static/img/lens/lens_thinsite_lens.png" alt=""/>
        </div>
    </section>
    <section class="content-body clearfix">
        <section class="content-section content-section--bg clearfix">
            <h2>이런 분에게 <br class="only-pc"/>필요합니다.</h2>
            <div class="row">
                <div class="col col-xs-6">
                    • 딱딱하다는 인식 때문에 하드렌즈 착용을 꺼려하시거나 하드렌즈의 교체를 망설이시는 분 <br/>
                    • 하드렌즈적응이 어려워 착용을 중단하시는 분 <br/>
                    • 하드렌즈의 불편함 때문에 가끔 착용하시는 분
                </div>
                <div class="col col-xs-6">
                    • 두꺼운 고도 근시 렌즈로 착용이 불편하신 분 <br/>
                    • 안구건조로 렌즈착용이 불편하신 분 <br/>
                    • 렌즈를 처음 끼려고 하시는 분
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>디자인</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>일반렌즈 대비 30% 이상 얇은 중심두께, 48% 가벼운 무게</h3>
                    <p>
                        미국 ART Optical사의 특허기술로 만들어진
                        최초의 Thin Design Lens로서 일반렌즈보다
                        중심두께는 30%이상 얇고 무게도 48%나
                        가벼워졌습니다. 각막과 안검의 해부학적 구조를
                        고려하여 중심부에서 주변부의 경계면이 없이
                        일정하게 얇아진 양면 비구면 렌즈입니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_thinsite_ill1.png" alt=""/>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>장점</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>첨단 재질 Boston EO의 독특한 구조</h3>
                    <p>
                        Boston EO 재질이 가지고 있는 AERCOR
                        화학구조(AERCOR Chemical Architecture)는
                        재질의 실리콘 성분의 함량을 대폭 낮추어 습윤성을
                        증가시키면서도 높은 산소투과성과 내구성을 동시에
                        만족시킵니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_thinsite_ill2.png" alt=""/>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>얇고 부드러워 좋은 착용감</h3>
                    <p>THINSITE는 렌즈 중심부의 두께가 일반렌즈보다 30%
                        이상 얇고, 무게는 48%나 가볍습니다. 또한 중심부에서
                        주변부까지 일정하게 얇아진 독특한 디자인으로 각막에
                        압력이 골고루 분산되어 각막에 전혀 부담을 주지 않아
                        편안한 착용감을 처음부터 느낄 수 있습니다. 특히 눈을
                        깜박일 때 윗눈꺼풀에 주는 자극감도 없어 누구나 쉽고
                        빠르게 적응할 수 있는 부드러운 RGP렌즈입니다.</p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_thinsite_ill3.png" alt=""/>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>고도 근시에 이상적인 구조</h3>
                    <p>
                        렌즈의 편안함은 렌즈의 가장자리와 눈꺼플의 접촉에
                        따라 달라집니다.
                        일반적인 고도근시용렌즈는 중심부에 비해 주변부가
                        너무 두꺼워 눈을 깜박일 때마다 심한 이물감을 느낄 수
                        있으나 ThinSite는 중심부와 가장자리가 일정하게 얇아
                        착용감이 부드럽고 좋습니다.
                        이제까지 일반렌즈를 착용하여 고통을 받은 고도근시
                        환자에게 더 없는 편안함과 만족감을 드립니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_thinsite_ill4.png" alt=""/>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>원시 환자들에게도 효과적인 렌즈</h3>
                    <p>
                        일반 원시용 RGP렌즈는 근시용 렌즈보다 중심 두께가 2
                        배 이상 두꺼워 산소투과성이 떨어지며 렌즈가 무거워
                        중심에서 이탈하기 쉽습니다. 하지만, 원시용 ThinSite
                        Plus는 중심두께가 얇아 일반 원시용 RGP렌즈에 비해
                        산소투과성이 우수하고 가벼워 원시환자에게 효과적인
                        렌즈입니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>높은 산소 전달률로 눈을 건강하게</h3>
                    <p>렌즈의 산소전달률은 눈의 충혈과 건강에 직접적인
                        영향을 미칩니다. 산소전달률은 렌즈의 중심두께와
                        반비례하므로 중심두께가 얇을수록 산소전달률은
                        높아지게 됩니다.
                        THINSITE는 기존의 렌즈보다 중심두께가 30%이상
                        얇으며 또한, 산소투과성이 탁월한 Boston EO재질을
                        사용하여, 일반 렌즈에 비해 산소전달률이 우수합니다.</p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>건조한 안구에 특히 좋은 최적의 습윤성</h3>
                    <p>RGP렌즈 성분의 하나인 실리콘은 소수성(물을 싫어하는
                        성질)과 친지성(기름을 좋아하는 성질)을 가지고 있으며
                        렌즈의 습윤성은 실리콘의 함량에 의해 결정됩니다.
                        THINSITE는 실리콘 함량이 일반 렌즈의 1/4정도 밖에
                        되지 않아 기존렌즈 착용 시 느꼈던 건조함이
                        사라집니다.
                        또한, 중심부와 주변부의 경계면이 없는 특수한 비구면
                        디자인으로 눈물 순환을 방해하는 요인을 없애주어
                        안구건조의 문제점을 해결 하였습니다.</p>
                </div>
                <div class="col col-xs-6">
                    <h3>렌즈의 변형이 생기지 않는 탁월한 내구성</h3>
                    <p>
                        Boston EO 재질의 AERCOR Crosslinker 구조는
                        "산소투과성이 높으면 내구성이 약하다"는 기존의
                        개념을 깨고 말랑말랑하고 내구성이 강한 Thin Design
                        을 가능하게 했습니다.
                    </p>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>특징</h2>
            <table class="table-custom">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th class="color-theme">Thinsite</th>
                    <th>기존 RGP 렌즈</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>촉감</td>
                    <td class="td-theme">말랑말랑하다</td>
                    <td>딱딱하다</td>
                </tr>
                <tr>
                    <td>적응</td>
                    <td class="td-theme">쉽다</td>
                    <td>보통</td>
                </tr>
                <tr>
                    <td>초기착용감</td>
                    <td class="td-theme">편안하다</td>
                    <td>불편하다</td>
                </tr>
                <tr>
                    <td>장기착용시</td>
                    <td class="td-theme">아주 편안하다</td>
                    <td>편안하다</td>
                </tr>
                <tr>
                    <td>안구건조</td>
                    <td class="td-theme">아주 좋다</td>
                    <td>좋다</td>
                </tr>
                <tr>
                    <td>눈물순환</td>
                    <td class="td-theme">아주 좋다</td>
                    <td>좋다</td>
                </tr>
                <tr>
                    <td>산소전달율</td>
                    <td class="td-theme">아주 좋다</td>
                    <td>높다</td>
                </tr>
                </tbody>
            </table>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>다른 RGP 렌즈들에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="rgp_supervision.php">
                    <img src="../../static/img/lens/lens_supervision_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_envision.php">
                    <img src="../../static/img/lens/lens_envision_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_achievement.php">
                    <img src="../../static/img/lens/lens_achievement_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_supervisiont.php">
                    <img src="../../static/img/lens/lens_supervisiont_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_soclear.php">
                    <img src="../../static/img/lens/lens_soclear_big.png" alt=""/>
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>