<?php
    $theme = 'lens-intro contex';
    $title = '각막굴절교정술 더 알아보기';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">C&amp;B 렌즈소개 > 각막굴절 교정렌즈 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1>각막 굴절 교정술(Orthokeratology)이란?</h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <p>
                역기하 디자인의 특수 콘택트렌즈(Contex OK Lens)를 이용하여 수술 없이 각막의 형태를 변화시켜 근시와 난시의 진행을
                조정 또는 교정하는 획기적인 시술법으로 40년 이상의 역사를 가진 안전한 시술법입니다. <br/>
                Contex OK 렌즈로 원하는 도수만큼 각막중심부를 눌러줌으로써 근시 및 난시가 교정되며 시술이 완료되었을 시에는
                레이저를 이용한 각막굴절수술과 같은 효과가 나타납니다.
            </p>
        </section>
        <section class="content-section">
            <h2>각막 굴절<br class="only-pc"/>교정술의 역사</h2>
            <ul class="list-history">
                <li>
                    <div class="list-history__year">1950</div>
                    <div class="list-history__dot"></div>
                    <div class="list-history__line"></div>
                    <div class="list-history__desc">
                        <strong>1세대 각막 굴절 교정술 개발.</strong> <br/>
                        하드렌즈가 개발되었을 때 시작됨. <br/>
                        간단하고 값이 싼 방법이었으나 시간이 오래 걸리며 근시 교정 범위도 좁았음.
                    </div>
                </li>
                <li>
                    <div class="list-history__year">1996</div>
                    <div class="list-history__dot"></div>
                    <div class="list-history__line"></div>
                    <div class="list-history__desc">
                        <strong>2세대 역기하렌즈 개발.</strong> <br/>
                        (Reverse Geometry Lens, RGL) 개발. <br/>
                        보다 넓어진 근시 교정범위로 본격적으로 보급되기 시작함.
                    </div>
                </li>
                <li>
                    <div class="list-history__year">1998</div>
                    <div class="list-history__dot"></div>
                    <div class="list-history__line"></div>
                    <div class="list-history__desc">
                        <strong>3세대 무수술 시력교정 렌즈 개발.</strong> <br/>
                        (Custom Reverse Geometry Lens)개발. <br/>
                        -Dr. Richard Wlodgy, Nick Stoyan
                    </div>
                </li>
                <li>
                    <div class="list-history__year">1998</div>
                    <div class="list-history__dot"></div>
                    <div class="list-history__line"></div>
                    <div class="list-history__desc">
                        <strong>Contex OK렌즈 개발.</strong> <br/>
                        일명 꿈(Dream)의 렌즈라고 불림. <br/>
                        -Thomas Reim, Jim Day, Newtok K.Wesley, Nick Stoyan
                    </div>
                </li>
            </ul>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>C&amp;B의 다른 렌즈들에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="rgp.php">
                    <img src="../../static/img/lens/lens_1st_icon1.png" alt=""/>
                    RGP 렌즈
                </a>
            </li>
            <li class="btn-square">
                <a href="kerasoft.php">
                    <img src="../../static/img/lens/lens_1st_icon3.png" alt=""/>
                    맞춤형 소프트 렌즈
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>