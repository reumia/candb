<?php
    $theme = 'lens-intro supervision';
    $title = 'Supervision II';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">C&amp;B 렌즈소개 > RGP 렌즈 ></span> <?=$title;?>
    </section>
    <section class="content-header clearfix">
        <div class="lens-header">
            <p>
                ART Optical Contact Lens, Inc.
            </p>
            <img class="lens-header-logo" src="../../static/img/lens/lens_supervision_logo.png" alt=""/>
            <hr/>
            <h1>안정성, 기능성, 편안함, RGP렌즈의 삼위일체</h1>
            <h3>
                SupervisionII는 기존 Supervision의 디자인적 특성은 그대로 유지하면서도 국내 <br class="only-pc"/>
                최초로 산소투과성이 탁월하고 습윤성이 우수한 Boston XO2 재질을 사용하여 <br class="only-pc"/>
                장시간 건강하고 편안한 렌즈 착용을 가능하게 해줍니다.
            </h3>
            <div class="lens-header-icons row">
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_supervision_icon1.png" alt=""/>
                    <p>
                        구면 디자인과 <br class="only-pc"/>
                        비구면 디자인의 결합
                    </p>
                </div>
                <div class="col col-xs-3">
                    <img src="../../static/img/lens/lens_supervision_icon2.png" alt=""/>
                    <p>
                        Boston XO2 <br class="only-pc"/>
                        재질 사용
                    </p>
                </div>
            </div>
            <img class="lens-header-lens" src="../../static/img/lens/lens_supervision_lens.png" alt=""/>
        </div>
        <p>
            <small style="color: #44CFE0;">
                Boston Supervision은 Boston SupervisionII와 디자인적 특징은 동일하며<br class="only-pc"/>
                Boston SupervisionII이 Boston Supervision에 비해 산소투과율(DK)과 산소분압(EOP)이 다소 높습니다.<br/>
                (DK 100 - ISO/FATT, EOP - 18%)
            </small>
        </p>
    </section>
    <section class="content-body clearfix">
        <section class="content-section content-section--bg clearfix">
            <h2>이런 분에게 <br class="only-pc"/>필요합니다.</h2>
            <div class="row">
                <div class="col col-xs-6">
                    • 중등도 및 고도근시 <br/>
                    • 중등도 이하 난시 <br/>
                    • 시력에 민감한 분 <br/>
                    • 렌즈 피팅이 까다로운 경우
                </div>
                <div class="col col-xs-6">
                    • 근시진행이 빠른 학생 <br/>
                    • 소프트렌즈 착용에 실패한 분 <br/>
                    • 기존 RGP렌즈 착용에 실패한 분
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>디자인</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>구면 디자인과 비구면 디자인의 장점만을 결합한 한국인의 눈에 꼭 맞는 이상적인 디자인</h3>
                    <p>
                        렌즈 전면부와 후면 가장자리 부분은 경계면이 없는 비구면 처리로 눈꺼풀과 각막의 해부학
                        적 구조와 일치함으로써 편안한 착용감을 느끼게 하고 후면 중심부는 구면처리로 맞춤이
                        쉽고 깨끗하면서도 일정한 시력을 유지시켜줍니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_supervision_ill1.png" alt=""/>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>장점</h2>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>탁월한 투과성 - 눈에 안전한 렌즈입니다.</h3>
                    <p>
                        각막은 신체의 다른 부위와 달리 혈관이 없는 조직이므로 18 산소 공급이 가장 중요합니다.
                        Boston SuperVisionII는 산소 투과성(DK)이 탁월하며 (Hyper DK, DK 140 - ISO/FATT) 산소 분압(EOP)이 높은 (19.5%, 맨눈 일 때 EOP 21%) Boston XO2재질을 사용하여 각막이 필요
                        로 하는 산소를 완벽하게 공급해주므로 산소부족으로 인한 각막 질환 등이 생기지 않는 안전한 렌즈입니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_supervision_ill2.png" alt=""/>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>내구성 - 경제적인 렌즈입니다.</h3>
                    <p>
                        Boston SuperVisionII는 Boston 렌즈만의 독특한 구조로 탄력성이 매우 뛰어 납니다. 또한 탁월한 내구성으로 렌즈의 변형이 생기지 않아 렌즈의 수명이 다할 때까지 원래의 형태를 유지하는 안전한 렌즈입니다. 현저히 낮은 실리콘 함유로 렌즈의 흠이 잘 생기지 않으며 또한 친수성이 탁월하여 렌즈 표면에 건조가 생기지 않아 안구 건조증이 있는 경우에도 편안하게 착용할 수 있는 렌즈입니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>근시진행 억제효과</h3>
                    <p>
                        성장기 학생의 근시진행을 억제하기 위해서는 렌즈의 재질이 안정적이어서 재질의 변화가 생기지 않아야 하고 또한 산소공급이 완벽하여 장시간 착용이 가능할 뿐 아니라 어떠한 각막질환도 생기지 않아야 합니다.
                        Boston SuperVisionII는 이러한 조건을 모두 충족시키는 성장기 청소년에게 안전한 렌즈입니다.
                    </p>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>특징</h2>
            <table class="table-custom">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th class="color-theme">Boston Superision II</th>
                    <th>기존 RGP 렌즈</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>시력교정효과</td>
                    <td class="td-theme">탁월하다</td>
                    <td>좋다</td>
                </tr>
                <tr>
                    <td>산소투과성(DK)</td>
                    <td class="td-theme">아주 높다 (±200)</td>
                    <td>높다 (±50)</td>
                </tr>
                <tr>
                    <td>각막산소분압(EOP)</td>
                    <td class="td-theme">18% 이상</td>
                    <td>±10%</td>
                </tr>
                <tr>
                    <td>이물질침착</td>
                    <td class="td-theme">거의 안됨</td>
                    <td>잘 안됨</td>
                </tr>
                <tr>
                    <td>난시교정</td>
                    <td class="td-theme">완전</td>
                    <td>완전</td>
                </tr>
                <tr>
                    <td>근시진행억제</td>
                    <td class="td-theme">최상</td>
                    <td>좋다</td>
                </tr>
                </tbody>
            </table>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>다른 RGP 렌즈들에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="rgp_envision.php">
                    <img src="../../static/img/lens/lens_envision_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_thinsite.php">
                    <img src="../../static/img/lens/lens_thinsite_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_achievement.php">
                    <img src="../../static/img/lens/lens_achievement_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_supervisiont.php">
                    <img src="../../static/img/lens/lens_supervisiont_big.png" alt=""/>
                </a>
            </li>
            <li class="btn-square">
                <a href="rgp_soclear.php">
                    <img src="../../static/img/lens/lens_soclear_big.png" alt=""/>
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>