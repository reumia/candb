<?php
    $theme = 'lens-intro management';
    $title = '렌즈 관리 방법';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">C&amp;B 렌즈소개 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1><?=$title;?></h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h2>
                렌즈의 위치<br class="only-pc"/>
                맞추기
            </h2>
            <h3 class="color-default">
                렌즈를 끼고 빼는 동안 렌즈가 흰자위로 잘못 위치하는 경우가 생깁니다. 이런
                경우에는 당황하거나 렌즈를 빼려고 하지 말고 사진과 같이 거울을 보고
                손가락으로 눈을 크게 벌리고 렌즈가 있는 곳을 확인합니다. 눈동자를 이탈한
                렌즈의 위치를 바로 잡을 때에는 손가락을 렌즈에 직접 대지 말고 눈꺼풀을
                통해 눈동자 위로 위치시킵니다.
            </h3>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_care_ill1.png" alt=""/>
                    <h3>1. 렌즈가 위로 올라갔을 때</h3>
                    <p>
                        1. 거울을 눈위치보다 아래로 놓습니다. <br/>
                        2. 머리는 움직이지 않고 눈만 아래를 내려다 봅니다. <br/>
                        3. 검지 손가락을 위 눈꺼풀 위에 살며시 올려놓습니다. <br/>
                        4. 손가락으로 천천히 렌즈를 눈동자 위에 위치시킵니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_care_ill2.png" alt=""/>
                    <h3>2. 렌즈가 귀 쪽으로 갔을 때</h3>
                    <p>
                        1. 거울을 약간 코 쪽으로 놓습니다. <br/>
                        2. 머리는 움직이지 않고 눈만 거울을 쳐다봅니다. <br/>
                        3. 검지 손가락을 눈꼬리 위에 살며서 올려놓습니다. <br/>
                        4. 손가락으로 천천히 렌즈를 눈동자 위에 위치시킵니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_care_ill3.png" alt=""/>
                    <h3>3. 렌즈가 코 쪽로 올라갔을 때</h3>
                    <p>
                        1. 거울을 약간 귀 쪽으로 놓습니다. <br/>
                        2. 머리는 움직이지 않고 눈만 거울을 쳐다봅니다. <br/>
                        3. 검지손가락을 코 쪽 눈머리에 살며시 올려놓습니다. <br/>
                        4. 손가락으로 천천히 렌즈를 눈동자 위에 위치시킵니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <img src="../../static/img/lens/lens_care_ill4.png" alt=""/>
                    <h3>4. 렌즈가 아래로 내려갔을 때</h3>
                    <p>
                        1. 거울을 약간 위 쪽으로 놓습니다. <br/>
                        2. 머리는 움직이지 않고 눈만 거울을 쳐다봅니다. <br/>
                        3. 검지손가락을 아래 눈꺼풀 위에 살며시 올려 놓습니다. <br/>
                        4. 손가락으로 천천히 렌즈를 눈동자 위에 위치시킵니다.
                    </p>
                </div>
            </div>
        </section>
        <section class="content-section">
            <h2>
                렌즈 착용 시<br class="only-pc"/>
                주의사항
            </h2>
            <h3 class="color-default">
                콘택트렌즈 착용 인구가 급속도로 증가함에 따라 렌즈 착용으로 인한 부작용
                또한 증가 하고 있습니다. 부작용의 대부분은 소프트렌즈 때문인데 이의
                근본적인 원인은 렌즈 재질의 산소투과성이 매우 낮아 발생할 수 밖에 없는
                산소 결핍과 안구건조 때문입니다.
            </h3>
            <h3 class="color-default">
                이러한 부작용을 예방하기 위해서는 주기적으로 안과를 방문하여 눈과
                렌즈의 검사를 받아 보는 것이 무엇보다도 중요하며 정상적인 적응증상과
                비정상적인 상황을 판단하여 비정상적인 상황이라고 생각되면 즉시 렌즈를
                빼고 안과적인 검사와 조치를 받도록 해야만 합니다.
            </h3>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>정상적인 증상 (서서히 감소됨)</h3>
                    <p>
                        렌즈 착용 초기에는 렌즈가 이물로서 작용되기
                        때문에, 눈물의 양이 많아지고 자극감이 있을 수
                        있습니다. 그러나 착용시간이 증가되고
                        눈깜박임운동(Blinking Exercise)을 많이 하면
                        1~2주에 말끔히 사라집니다.
                    </p>
                </div>
                <div class="col col-xs-6">
                    <h3>비정상적인 증상 (갑자기 나타남)</h3>
                    <p>
                        위의 증상과는 달리 갑자기 나타나며, 대개
                        통증을 동반합니다. 즉시 내원하여 그 원인을
                        해결해야 하며 급하면 전화상담이라도 해야
                        합니다.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    * 눈물이 많이 난다. <br/>
                    * 경미한 자극감이 있다. <br/>
                    * 눈을 깜박일때 마다 시야가 흐려진다. <br/>
                    * 경미한 충혈이 있다.
                </div>
                <div class="col col-xs-6">
                    * 갑작스런 통증이 있다. <br/>
                    * 불빛 주위가 달무리처럼 보인다.<br/>
                    * 심한 충혈 및 자극감이 있다.<br/>
                    * 안경 착용 후 1시간 이상 흐리게 보인다.<br/>
                    * 분비물이 심하게 생긴다.<br/>
                    * 렌즈가 잘 움직이지 않는다.
                </div>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>렌즈를 바르고 안전하게 사용하는 방법에 대해 더 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="tip_usage.php">
                    <img src="../../static/img/lens/lens_1st_icon4.png" alt=""/>
                    렌즈 사용 방법
                </a>
            </li>
            <li class="btn-square">
                <a href="tip_exercise.php">
                    <img src="../../static/img/lens/lens_1st_icon6.png" alt=""/>
                    순목 운동
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>