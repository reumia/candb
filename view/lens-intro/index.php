<?php
    $theme = 'lens-intro';
    $title = 'C&B 렌즈소개';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <?=$title;?>
    </section>
    <section class="content-header clearfix">
        <img src="../../static/img/lens/lens_1st_top.png" alt=""/>
        <h1>콘택트 렌즈란?</h1>
        <h3>
            굴절이상(근시, 원시, 난시)의 대부분은 각막(검은 눈동자)의 중심부에서 일어납니다.
            <br/>
            이러한 원인을 제거하고, 보다 효과적인 시력을 얻으며 편리한 활동을 위하여 각막에<br class="only-pc"/> 부착하는 렌즈를 콘택트렌즈라 합니다.
        </h3>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h2>C&amp;B의 렌즈</h2>
            <h3>굴절이상으로 인한 시력이상 증상의 핵셜을 위한 3가지 해결책, <br class="only-pc"/>C&amp;B에서 만나 보세요.</h3>
            <p>
                콘택트렌즈는 재질 및 디자인에 따라 다양한 형태로 분류됩니다. C&amp;B에서는 이러한 다양한 종류의 콘택트렌즈를 가장 앞서
                국내에 소개함으로써 다양한 원인으로 인한 굴절이상의 해결을 위해 노력해오고 있습니다. C&B의 렌즈는 크게 RGP렌즈,
                각막굴절교정렌즈, 맞춤형 소프트 렌즈로 분류 됩니다.
            </p>
            <ul class="btn-wrap">
                <li class="btn-square">
                    <a href="rgp.php">
                        <img src="../../static/img/lens/lens_1st_icon1.png" alt=""/>
                        RGP 렌즈
                    </a>
                </li>
                <li class="btn-square">
                    <a href="contex.php">
                        <img src="../../static/img/lens/lens_1st_icon2.png" alt=""/>
                        각막굴절 교정렌즈
                    </a>
                </li>
                <li class="btn-square">
                    <a href="kerasoft.php">
                        <img src="../../static/img/lens/lens_1st_icon3.png" alt=""/>
                        맞춤형 소프트 렌즈
                    </a>
                </li>
            </ul>
        </section>
        <section class="content-section">
            <h2>렌즈를 바르고<br class="only-pc"/> 안전하게<br class="only-pc"/>사용하는 방법</h2>
            <h3>렌즈를 사용하는 것은 어렵지 않습니다.<br/>간단한 관리만으로 건강하게 렌즈를 사용할 수 있습니다.</h3>
            <p>
                콘택트렌즈를 사용하면 시력이상 증상의 해결은 물론 미용적인 부분에서도 많은 장점이 있습니다. 올바른 사용방법과
                관리방법을 습득하면 누구나 간단한 관리만으로도 렌즈의 수명을 연장시키고 시력을 보호할 수 있습니다. 렌즈를 처음
                착용하거나 자세한 렌즈 사용방법과 관리방법이 궁금하신 분, 렌즈 적응이 너무 힘들게 느껴지시는 분도 어렵지 않게
                콘택트렌즈를 사용할 수 있습니다.
            </p>
            <ul class="btn-wrap">
                <li class="btn-square">
                    <a href="tip_usage.php">
                        <img src="../../static/img/lens/lens_1st_icon4.png" alt=""/>
                        렌즈 사용 방법
                    </a>
                </li>
                <li class="btn-square">
                    <a href="tip_management.php">
                        <img src="../../static/img/lens/lens_1st_icon5.png" alt=""/>
                        렌즈 관리 방법
                    </a>
                </li>
                <li class="btn-square">
                    <a href="tip_exercise.php">
                        <img src="../../static/img/lens/lens_1st_icon6.png" alt=""/>
                        순목 운동
                    </a>
                </li>
            </ul>
        </section>
    </section>
</article>
<?php
    include_once '../inc/footer.php';
?>