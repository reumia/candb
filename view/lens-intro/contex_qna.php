<?php
    $theme = 'lens-intro contex';
    $title = '질문과 답변';
    include_once '../inc/header.php';
    include_once 'contex_qna_arr.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">C&amp;B 렌즈소개 > 각막굴절 교정렌즈 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1>Contex OK 렌즈에 대한 질문과 답변</h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h3 class="color-default">
                각막굴절 교정렌즈 Contex OK 렌즈에 대해서 많은 분들이 궁금해 하시는 사항들을 알려드립니다.
            </h3>
        </section>
        <section class="content-section">
            <h2>
                질문과<br class="only-pc"/>
                답변
            </h2>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php
                $i = 1;
                foreach( $qna_arr as $key => $value ):
                    ?>
                    <div class="panel panel-default panel-candb">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?=$i?>" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                    <?=$key;?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse_<?=$i?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <?=$value;?>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
                endforeach;
                ?>
            </div>
        </section>
        <section class="content-section">
            <h2>
                각막굴절<br class="only-pc"/>
                교정술과<br class="only-pc"/>
                굴절수술의<br class="only-pc"/>
                차이점
            </h2>
            <table class="table-custom">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th class="color-theme">
                        각막굴절교정술 <br/>
                        (Orthokeratology)
                    </th>
                    <th>
                        굴절수술 <br/>
                        (Refractive Surgery)
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>연령제한</td>
                    <td class="td-theme">제한이 없다</td>
                    <td>20세 이상</td>
                </tr>
                <tr>
                    <td>교정원리</td>
                    <td class="td-theme">각막상피가 눌러짐</td>
                    <td>각막이 깎여나감</td>
                </tr>
                <tr>
                    <td>조직침투</td>
                    <td class="td-theme">없다 (Non-invasive)</td>
                    <td>있다 (Invasive)</td>
                </tr>
                <tr>
                    <td>각막의 변화</td>
                    <td class="td-theme">복원될 수 있다</td>
                    <td>복원될 수 없다</td>
                </tr>
                <tr>
                    <td>안정성</td>
                    <td class="td-theme">안전하다</td>
                    <td>예기치 않는 합병증 가능</td>
                </tr>
                <tr>
                    <td>유지렌즈</td>
                    <td class="td-theme">착용해야 한다</td>
                    <td>필요없다</td>
                </tr>
                </tbody>
            </table>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>C&amp;B의 다른 렌즈들에 대해서도 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="rgp.php">
                    <img src="../../static/img/lens/lens_1st_icon1.png" alt=""/>
                    RGP 렌즈
                </a>
            </li>
            <li class="btn-square">
                <a href="kerasoft.php">
                    <img src="../../static/img/lens/lens_1st_icon3.png" alt=""/>
                    맞춤형 소프트 렌즈
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>