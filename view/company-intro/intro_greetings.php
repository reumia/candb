<?php
    $theme = 'company-intro greetings';
    $title = '인사말';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">회사소개 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1><?=$title;?></h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section greetings-txt">
            <p>
                저희 씨엔비는 1995년 창사 이래 눈 건강을 위하여 최상의 품질을 제공한다는 소명 하에 세계 최고의 렌즈제조사들과의 파트너쉽을 구축하여 “드림렌즈”의 효시인 Contex Ok Lens와 RGP Lens를 국내 최초로 안과전문의에게 보급함으로써 새롭고 건강한 근시교정 및 근시진행억제 방법을 널리 알리는데 앞장서 왔습니다.
            </p>
            <p>
                전문성 있는 지속적인 교육을 바탕으로 전세계의 RGP Lens와 OK Lens (Ortho-keratology) 를 선두하는 결과를 낳았고 렌즈의 트랜드를 한국이 주도적으로 바꿔 나가는데 저희 씨엔비가 함께 하였습니다.
            </p>
            <p>
                씨엔비는 이제 단순한 시력교정의 목적을 넘어 더 높은 사명을 위해 나아가려 합니다. 소프트렌즈나 안경으로 해결 불가능한 고도근시 및 난시, 원추각막 심지어 굴절수술 후의 시력문제 등 지금껏 불가능으로 여겨졌던 영역을 개척하고 선두 하여 명실공히 최고가 되도록 최선을 다하겠습니다.
            </p>
            <p>
                많은 관심과 성원 부탁 드리며 씨엔비를 찾아주시는 모든 분들께서 올바른 렌즈를 선택하시고 만족하실 수 있도록 항상 함께 하겠습니다.
            </p>
            <p>
                감사합니다.
            </p>
            <img src="../../static/img/brand/brand_grettings_ill1.png" alt="" class="greetings-img"/>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>Clean &amp; Bright 에 대해 더 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="intro_history.php">
                    <img src="../../static/img/brand/brand_1st_icon2.png" alt=""/>
                    연혁
                </a>
            </li>
            <li class="btn-square">
                <a href="intro_manufacturer.php">
                    <img src="../../static/img/brand/brand_1st_icon3.png" alt=""/>
                    렌즈 제조사 소개
                </a>
            </li>
            <li class="btn-square only-pc">
                <a href="intro_contact.php">
                    <img src="../../static/img/brand/brand_1st_icon4.png" alt=""/>
                    Contact Us
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>