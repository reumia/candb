<?php

$data_arr = array(
    '1995' =>
    array(
        '㈜씨엔비코퍼레이션 법인설립<br/>의료기기제조 및 수입판매업<br/>의료용구 판매업',
        'history_cnb.png',
        ''
    ),
    '1996' =>
    array(
        'Equalens, Boston Lens<br/>Care System 허가 및 판매<br/>한국시장에 “Boston” 최초소개',
        'history_boston.png',
        ''
    ),
    '1998' =>
    array(
        'Contex OK Lens 출시',
        'history_contex.png',
        ''
    ),
    '1999' =>
    array(
        'Boston Liquid Enzymatic Cleaner 출시',
        'history_liquid.png',
        ''
    ),
    '2001' =>
    array(
        'Boston Supervision 출시',
        'history_supervision.png',
        ''
    ),
    '2002' =>
    array(
        array(
            'Art optical THINSITE 출시',
            'Boston Envision 출시'
        ),
        array(
            'history_thinsite.png',
            'history_envision.png',
        ),
        array(
            '',
            ''
        )
    ),
    '2005' =>
    array(
        'Boston SIMPLUS 출시',
        'history_simplus.png',
        ''
    ),
    '2006' =>
    array(
        'Art optical Achievement 출시',
        'history_achievement.png',
        ''
    ),
    '2008' =>
    array(
        'Boston Supervision II 출시',
        'history_supervision2.png',
        ''
    ),
    '2011' =>
    array(
        'Boston Supervision T 출시',
        'history_supervisiont.png',
        ''
    ),
    '2012' =>
    array(
        'So2Clear 출시',
        'history_so2clear.png',
        ''
    ),
    '2014' =>
    array(
        'Kerasoft IC 출시',
        'history_kerasoft.png',
        ''
    )

);

