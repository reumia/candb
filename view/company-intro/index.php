<?php
    $theme = 'company-intro index';
    $title = '회사 소개';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <?=$title;?>
    </section>
    <section class="content-header clearfix" style="padding-top: 200px; padding-bottom: 200px;">
        <h1 style="margin-bottom: 20px;">
            Clean &amp; Bright Vision, Clean &amp; Bright Mind!
        </h1>
        <h3>
            C&B는 RGP Lens 전문회사로서 전국민의 맑고 건강한 눈, 밝은 세상을 위해<br class="only-pc"/>
            최상의 제품을 공급 하고자 끊임없이 노력하는 회사입니다.
        </h3>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h2><?=$title;?></h2>
            <h3>
                C&B는 RGP Lens 전문회사로 전국 1500여개 안과병원에 RGP Lens,
                전국 안경점과 Drug Store에 RGP Lens 전용 관리용액 Boston Care
                System을 공급하며 한국 RGP Lens 시장의 변화를 주도하고 있습니다.
            </h3>
            <h3 class="color-default">
                저희 C&B는 앞으로 RGP Lens 뿐만 아니라 세계적으로 새로이 출시되는
                다양한 Contact Lens를 가장 빠르게 보급함으로써 한국의 독자적
                트렌드와 세계적 트렌드를 모두 만족 시켜드리는 RGP Lens의
                선두주자로서의 역할을 다할 것을 약속합니다.
            </h3>
            <br/>
            <div class="img-wrap">
                <img src="../../static/img/brand/brand_1st_logo.png" alt=""/>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <table class="table" style="margin-bottom: 0">
                        <tbody>
                        <tr>
                            <th>회사명</th>
                            <td>(주)씨엔비코퍼레이션</td>
                        </tr>
                        <tr>
                            <th>설립일</th>
                            <td>1995년 4월 25일</td>
                        </tr>
                        <tr>
                            <th>대표이사</th>
                            <td>황정주</td>
                        </tr>
                        <tr>
                            <th>사업분야</th>
                            <td>RGP렌즈 및 관리용액</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <ul class="btn-wrap">
                <li class="btn-square">
                    <a href="intro_greetings.php">
                        <img src="../../static/img/brand/brand_1st_icon1.png" alt=""/>
                        인사말
                    </a>
                </li>
                <li class="btn-square">
                    <a href="intro_history.php">
                        <img src="../../static/img/brand/brand_1st_icon2.png" alt=""/>
                        연혁
                    </a>
                </li>
                <li class="btn-square">
                    <a href="intro_manufacturer.php">
                        <img src="../../static/img/brand/brand_1st_icon3.png" alt=""/>
                        렌즈 제조사 소개
                    </a>
                </li>
                <li class="btn-square only-pc">
                    <a href="intro_contact.php">
                        <img src="../../static/img/brand/brand_1st_icon4.png" alt=""/>
                        Contact Us
                    </a>
                </li>
            </ul>
        </section>
        <section class="content-section">
            <h2>Contact</h2>
            <h3 class="color-default">
                <span class="icon-wrap">
                    <img src="../../static/img/brand/brand_1st_smallicon1.png" alt=""/>
                </span>
                서울시 강남구 테헤란로623 삼성빌딩 9층
            </h3>
            <img src="http://maps.googleapis.com/maps/api/staticmap?center=테헤란로623,강남구,서울시&zoom=16&size=748x300&scale=2&maptype=roadmap
&markers=37.5099756,127.0656941&sensor=false" alt="" style="width: 748px; height: auto;"/>
            <div class="row">
                <div class="col col-xs-4">
                    <h3 class="color-default">
                        <span class="icon-wrap">
                            <img src="../../static/img/brand/brand_1st_smallicon2.png" alt=""/>
                        </span>
                        02 561 9972
                    </h3>
                </div>
                <div class="col col-xs-4">
                    <h3 class="color-default">
                        <span class="icon-wrap">
                            <img src="../../static/img/brand/brand_1st_smallicon3.png" alt=""/>
                        </span>
                        02 567 7971
                    </h3>
                </div>
                <div class="col col-xs-4">
                    <h3 class="color-default">
                        <span class="icon-wrap">
                            <img src="../../static/img/brand/brand_1st_smallicon4.png" alt=""/>
                        </span>
                        candb@candb.co.kr
                    </h3>
                </div>
            </div>
        </section>
    </section>
</article>
<?php
    include_once '../inc/footer.php';
?>