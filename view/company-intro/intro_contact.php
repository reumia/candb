<?php
    $theme = 'company-intro contact';
    $title = 'Contact Us';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">회사소개 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1><?=$title;?></h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h3 class="color-default">
                C&B 웹사이트에 방문해주셔서 감사합니다. 저희 C&B는 항상 마음과 귀를 열어두고 있습니다. <br/>
                <span class="color-theme">업무제휴나 서비스 제안 등 바라시는 점</span>이 있으시면 아래의 양식에 따라 입력,
                등록해 주시면 좀 더 노력하여 더 좋은 서비스로 찾아뵙겠습니다.
            </h3>
        </section>
        <section class="content-section">
            <form action="">
                <table class="table-form" style="width: 100%;">
                    <tbody>
                    <tr>
                        <th class="color-theme">구분</th>
                        <td colspan="3">
                            <label for="">
                                <input type="radio" name="category_radio"/> 개인
                            </label>&nbsp;&nbsp;
                            <label for="">
                                <input type="radio" name="category_radio"/> 회사
                            </label>&nbsp;&nbsp;
                            <label for="">
                                <input type="radio" name="category_radio"/> 안과
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th class="color-theme">이름 / 상호</th>
                        <td><input class="form-control form-m" type="text"/></td>
                        <th class="color-theme">담당자</th>
                        <td><input class="form-control form-m" type="text"/></td>
                    </tr>
                    <tr>
                        <th class="color-theme">연락처</th>
                        <td><input class="form-control form-m" type="text"/></td>
                        <th class="color-theme">이메일</th>
                        <td><input class="form-control form-m" type="email"/></td>
                    </tr>
                    <tr>
                        <th class="color-theme">질문 분류</th>
                        <td colspan="3">
                            <label for="">
                                <input type="radio" name="question_radio"/> 업무제휴
                            </label>&nbsp;&nbsp;
                            <label for="">
                                <input type="radio" name="question_radio"/> 서비스 제안
                            </label>&nbsp;&nbsp;
                            <label for="">
                                <input type="radio" name="question_radio"/> 쇼핑몰 제휴
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>&nbsp;</th>
                        <td colspan="3">
                            <textarea class="form-control" name="" id="" cols="30" rows="10" style="width: 573px;"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>&nbsp;</th>
                        <td colspan="3">
                            <button class="btn btn-default" type="submit">등록하기</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>Clean &amp; Bright 에 대해 더 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="intro_greetings.php">
                    <img src="../../static/img/brand/brand_1st_icon1.png" alt=""/>
                    인사말
                </a>
            </li><li class="btn-square">
                <a href="intro_history.php">
                    <img src="../../static/img/brand/brand_1st_icon2.png" alt=""/>
                    연혁
                </a>
            </li>
            <li class="btn-square">
                <a href="intro_manufacturer.php">
                    <img src="../../static/img/brand/brand_1st_icon3.png" alt=""/>
                    렌즈 제조사 소개
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>