<?php
    $theme = 'company-intro history';
    $title = '연혁';
    include_once '../inc/header.php';
    include_once 'intro_history_arr.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">회사소개 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1><?=$title;?></h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h3 class="color-default">
                C&B는 콘택트렌즈의 영역이 단순 시력교정을 위함이라는 관념을 넘어 다양한 목적을
                위해 사용될 수 있도록 새로운 장을 열어 줄 고도근시용렌즈, 토릭렌즈, 원추각막용렌즈,
                맞춤형 소프트콘택트렌즈 와 같이 다양한 제품을 소개, 공급하여 콘택트렌즈 영역의
                새로운 역사를 만들어가고 있습니다.
            </h3>
        </section>
        <section class="content-section">
            <ul class="list-history">
                <?php
                foreach( $data_arr as $key => $value ):
                    ?>
                    <li>
                        <div class="list-history__year"><?=$key;?></div>
                        <div class="list-history__dot"></div>
                        <div class="list-history__line"></div>
                        <div class="list-history__desc">
                            <div class="row">
                                <div class="col col-xs-6">
                                    <?php
                                    if( is_array($value[0]) ):
                                        foreach ( $value[0] as $data ):
                                            echo $data.'<br/>';
                                        endforeach;
                                    else :
                                        echo $value[0];
                                    endif;
                                    ?>
                                </div>
                                <div class="col col-xs-3">
                                    <?php
                                    if( is_array($value[1]) ):
                                        for ( $i=0; $i<sizeof($value[1]); $i++ ):
                                            ?>
                                            <a href="<?=$value[2][$i];?>" target="_blank">
                                                <img src="../../static/img/brand/brand_<?=$value[1][$i];?>" alt=""/>
                                            </a>
                                            <?php
                                        endfor;
                                    else :
                                        ?>
                                        <a href="<?=$value[2];?>" target="_blank">
                                            <img src="../../static/img/brand/brand_<?=$value[1];?>" alt=""/>
                                        </a>
                                        <?php
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php
                endforeach;
                ?>
            </ul>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>Clean &amp; Bright 에 대해 더 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="intro_greetings.php">
                    <img src="../../static/img/brand/brand_1st_icon1.png" alt=""/>
                    인사말
                </a>
            </li>
            <li class="btn-square">
                <a href="intro_manufacturer.php">
                    <img src="../../static/img/brand/brand_1st_icon3.png" alt=""/>
                    렌즈 제조사 소개
                </a>
            </li>
            <li class="btn-square only-pc">
                <a href="intro_contact.php">
                    <img src="../../static/img/brand/brand_1st_icon4.png" alt=""/>
                    Contact Us
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>