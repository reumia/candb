<?php
    $theme = 'company-intro manufacturer';
    $title = '렌즈 제조사 소개';
    include_once '../inc/header.php';
?>
<article class="site-content common-width">
    <section class="content-breadcrumb">
        <span class="inactive">회사소개 ></span> <?=$title;?>
    </section>
    <section class="content-header content-header--sub clearfix">
        <h1><?=$title;?></h1>
    </section>
    <section class="content-body clearfix">
        <section class="content-section">
            <h3 class="color-default">
                C&amp;B는 세계적으로 최고의 기술력을 인정받은 제조사인 <br class="only-pc"/>
                Contex Inc, Art optical, Bausch&amp;Lomb 사와 믿음과 신뢰를 바탕으로 함께합니다.
            </h3>
        </section>
        <section class="content-section">
            <img src="../../static/img/manufacturer_artoptical.png" alt="Art optical 로고"/>
            <hr/>
            <h3 style="margin-bottom: 0">Art optical Contact Lens, Inc</h3>
            <p>Grand Rapids, MI49501, USA</p>
            <p style="margin-bottom: 40px;">
                Art optical 사는 80년 이상의 오랜 역사를 가진 미국 최대의 RGP렌즈 전문제조회사로서 CLMA(미국 컨택트렌즈 제조자협회, 1994년 설립)로부터 19년 연속 최우수품질인증을 받은 최고 기술의 회사입니다.
            </p>
            <h2 class="text-right">연혁</h2>
            <p style="margin-bottom: 40px;">
                1931년 설립 <br/>
                RGP렌즈 전문 제조회사로서 미국 최초로 ISO 9001 인증 획득 <br/>
                CLMA(미국 컨택트렌즈 제조자협회)로부터 19년 연속 최우수품질인증 <br/>
                (Seal of Excellance) 획득
            </p>
            <h2 class="text-right">제품</h2>
            <ul class="list-images">
                <li><img src="../../static/img/brand/brand_lenscompany_icon1.png" alt="supervision"/></li>
                <li><img src="../../static/img/brand/brand_lenscompany_icon2.png" alt="envision"/></li>
                <li><img src="../../static/img/brand/brand_lenscompany_icon3.png" alt="thinsite"/></li>
                <li><img src="../../static/img/brand/brand_lenscompany_icon4.png" alt="achievement"/></li>
                <li><img src="../../static/img/brand/brand_lenscompany_icon5.png" alt="supervisiont"/></li>
                <li><img src="../../static/img/brand/brand_lenscompany_icon6.png" alt="so2clear"/></li>
                <li><img src="../../static/img/brand/brand_lenscompany_icon7.png" alt="kerasoft"/></li>
            </ul>
        </section>
        <section class="content-section">
            <img src="../../static/img/manufacturer_contex.png" alt="Contex 로고"/>
            <hr/>
            <h3 style="margin-bottom: 0">Contex, Inc</h3>
            <p>Sherman Oaks, CA91403, USA</p>
            <p style="margin-bottom: 40px;">
                Contex사는 50년 이상의 역사를 가진 세계적인 특수 콘택트렌즈 전문 제조회사입니다. <br/>
                Contex사는 무수술 시력 교정렌즈인 Contex OK 렌즈를 포함한 노안, 원추각막등의 특수 콘택트렌즈 전문 제조회사입니다. <br/>
                특히 2세대 렌즈인 역기하렌즈(Reverse Geometry Lens, RGL)를 최초로 개발하고 3세대 렌즈인 Contex OK 렌즈, 난시용 시력교정렌즈를 최초로 개발하여 무수술 시력교정렌즈 분야에서 가장 앞선 기술과 뛰어난 디자인으로 최고의 명성을 가지고 있는 회사입니다.
            </p>
            <h2 class="text-right">연혁</h2>
            <p style="margin-bottom: 40px;">
                1962년 설립 <br/>
                1989년 산소투과성 재질을 사용한 제 2세대 역기하렌즈(Reverse Geometry Lens, RGL) 개발<br/>
                1998년 Contex OK 렌즈 FDA 공인을 받음.
            </p>
            <h2 class="text-right">제품</h2>
            <div>
                <img src="../../static/img/brand/brand_lenscompany_icon8.png" alt="컨텍스 렌즈"/>
            </div>
        </section>
        <section class="content-section">
            <img src="../../static/img/manufacturer_bausch.png" alt="바슈롬 로고"/>
            <hr/>
            <h3 style="margin-bottom: 0">Bausch &amp; Lomb Incorporated</h3>
            <p>Rochester, NY14609, USA</p>
            <p style="margin-bottom: 40px;">
                Polymer Technology사(PTC)의 이름으로 설립된 후 Bausch &amp; Lomb사의 이름으로 RGP렌즈 Material과 RGP렌즈 전용 관리용액을 생산하는 회사입니다.
                <br/>
                끊임 없는 기술개발과 뛰어는 제품력으로 RGP렌즈 관련 시장을 주도하고 있는 Bausch &amp; Lomb사(이전 Polymer Technology Corp.)는 그 결과 미국 시장에서 "Boston"의 이름으로 RGP렌즈 Material의 50%, 렌즈 관리용액(Boston Care System)의 90%라는 놀라운 시장점유율을 보유하고 있으며 전 세계 50여 개국에서 사용되고 있습니다.
            </p>
            <h2 class="text-right">연혁</h2>
            <p style="margin-bottom: 40px;">
                1972년 설립 <br/>
                1983년 RGP렌즈 전용 관리용액인 Boston Original Solution 개발 <br/>
                1983년 Bausch &amp; Lomb사와 인수합병 <br/>
                1990년 Boston Advance Solution 개발 <br/>
                2003년 Boston Simplus 개발
            </p>
            <h2 class="text-right">제품</h2>
            <div>
                <img src="../../static/img/boston_care_system.png" alt="보스톤 케어 시스템"/>
            </div>
        </section>
    </section>
    <section class="content-footer clearfix">
        <h1>Clean &amp; Bright 에 대해 더 알아보세요!</h1>
        <ul class="btn-wrap">
            <li class="btn-square">
                <a href="intro_greetings.php">
                    <img src="../../static/img/brand/brand_1st_icon1.png" alt=""/>
                    인사말
                </a>
            </li>
            <li class="btn-square">
                <a href="intro_history.php">
                    <img src="../../static/img/brand/brand_1st_icon2.png" alt=""/>
                    연혁
                </a>
            </li>
            <li class="btn-square only-pc">
                <a href="intro_contact.php">
                    <img src="../../static/img/brand/brand_1st_icon4.png" alt=""/>
                    Contact Us
                </a>
            </li>
        </ul>
    </section>

</article>
<?php
    include_once '../inc/footer.php';
?>