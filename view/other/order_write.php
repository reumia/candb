<?php
$theme = 'other order';
$title = '수주장 작성하기';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <section class="content-breadcrumb">
            <?=$title;?>
        </section>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    수주장<br class="only-pc"/>작성하기
                </h2>
                <div class="btn-wrap">
                    <div class="row">
                        <div class="col col-xs-6">
                            <a class="btn btn-success" href="order_write.php">등록하기</a>
                            <a class="btn btn-default" href="order.php">목록</a>
                        </div>
                        <div class="col col-xs-6">
                            <div class="board-search text-right">
                                <select class="form-control" name="" id=""></select>
                                <input class="form-control" type="text"/>
                                <button class="btn btn-default">검색</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="alert alert-success">
                    <div class="row">
                        <div class="col col-xs-4">
                            <strong class="color-theme">SV2</strong> SuperVision2 ( R : Violet - L : Green ) <br/>
                            <strong class="color-theme">SV</strong> SuperVision ( R : Violet - L : Green ) <br/>
                            <strong class="color-theme">TS</strong> ThinSite ( R : Gray - L : Brown )
                        </div>
                        <div class="col col-xs-4">
                            <strong class="color-theme">SVT</strong> SuperVisionToric ( R : Violet - L : Green) <br/>
                            <strong class="color-theme">BE</strong> Boston Equalens ( R : Blue - L : Blue ) <br/>
                            <strong class="color-theme">EV</strong> Envision ( R : Violet - L : Green )
                        </div>
                        <div class="col col-xs-4">
                            <strong class="color-theme">OK</strong> Contex OK Lens <br/>
                            <strong class="color-theme">AV</strong> Achievement (R : Gray - L : Brown)
                        </div>
                    </div>
                </div>
                <div class="board">
                    <table class="board-table">
                        <colgroup>
                            <col style="width: 32px;"/>
                            <col style="width: 100px;"/>
                            <col span="7"/>
                            <col style="width: 140px;"/>
                        </colgroup>
                        <thead>
                        <tr>
                            <th class="color-theme">No.</th>
                            <th class="color-theme">품명</th>
                            <th class="color-theme">B.C</th>
                            <th class="color-theme">Power</th>
                            <th class="color-theme">DIA</th>
                            <th class="color-theme">수량</th>
                            <th class="color-theme">환자명</th>
                            <th class="color-theme">Color</th>
                            <th class="color-theme">구분</th>
                            <th class="color-theme">비고</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        for( $i = 0; $i < 10; $i++ ){
                            ?>
                            <tr>
                                <td><label for=""><input class="checkbox" type="checkbox"/><?=$i;?></label></td>
                                <td>
                                    <select class="form-control" name="" id="">
                                        <option value="0">선택</option>
                                    </select>
                                </td>
                                <td><input class="form-control" type="text" value="test테스트"/></td>
                                <td><input class="form-control" type="text"/></td>
                                <td><input class="form-control" type="text"/></td>
                                <td><input class="form-control" type="text"/></td>
                                <td><input class="form-control" type="text"/></td>
                                <td><input class="form-control" type="text"/></td>
                                <td><input class="form-control" type="text"/></td>
                                <td><input class="form-control" type="text"/></td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <ul class="board-pagination">
                        <li><a href="#">&lt;</a></li>
                        <li><a href="#" class="active">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">&gt;</a></li>
                    </ul>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>