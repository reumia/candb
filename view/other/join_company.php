<?php
$theme = 'other';
$title = '안과 회원 가입';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <section class="content-breadcrumb">
            <span class="inactive">회원가입 > </span><?=$title;?>
        </section>
        <section class="content-body clearfix">

            <!-- form 시작 -->
            <form action="">
                <section class="content-section">
                    <h2>
                        거래처<br class="only-pc"/>
                        인증절차
                    </h2>
                    <div class="table-wrap">
                        <table class="table-form">
                            <tbody>
                            <tr>
                                <th>안과명</th>
                                <td>
                                    <input class="form-control form-m" type="text" readonly/>
                                    <button class="btn btn-default">검색</button>
                                </td>
                            </tr>
                            <tr>
                                <th>사업자등록번호</th>
                                <td>
                                    <input type="text" class="form-control form-m">
                                    예) 123456789
                                </td>
                            </tr>
                            <tr>
                                <th>사업자 대표명</th>
                                <td><input type="password" class="form-control form-m"></td>
                            </tr>
                            <tr>
                                <th>사업장 주소지</th>
                                <td>
                                    <input class="form-control form-s" type="text" readonly/>
                                    <input class="form-control form-l" type="text" readonly/>
                                    <button class="btn btn-default">우편번호 찾기</button>
                                </td>
                            </tr>
                            <tr>
                                <th>물품 수령지</th>
                                <td>
                                    <input class="form-control form-s" type="text"/>
                                    <input class="form-control form-l" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <th>&nbsp;</th>
                                <td>
                                    <label for="">
                                        <input type="checkbox"/>
                                        사업자 주소지와 동일
                                    </label>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
                <section class="content-section">
                    <h2>
                        신규아이디와 <br class="only-pc"/>
                        비밀번호 등록
                    </h2>
                    <h3>
                        모든 항목이 필수항목입니다. 빠짐없이 입력해 주세요.
                    </h3>
                    <div class="table-wrap">
                        <table class="table-form">
                            <tbody>
                            <tr>
                                <th>아이디</th>
                                <td>
                                    <input type="text" class="form-control form-m">
                                    <button class="btn btn-default">중복확인</button>
                                    4~10자의 영문 및 숫자로 써주세요.
                                </td>
                            </tr>
                            <tr>
                                <th>비밀번호</th>
                                <td>
                                    <input type="password" class="form-control form-m">
                                    4자 이상으로 써 주세요.
                                </td>
                            </tr>
                            <tr>
                                <th>비밀번호 확인</th>
                                <td>
                                    <input type="password" class="form-control form-m">
                                </td>
                            </tr>
                            <tr>
                                <th>E-mail</th>
                                <td>
                                    <input class="form-control form-m" type="email"/>
                                    예) candb@candb.co.kr
                                </td>
                            </tr>
                            <tr>
                                <th>&nbsp;</th>
                                <td>한메일(hanmail.net)은 메일전달이 되지 않습니다. 가급적 다른 메일을 사용해 주세요.</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
                <section class="content-section">
                    <div class="btn-wrap">
                        <button class="btn btn-success" type="submit">확인</button>
                        <button class="btn btn-default" type="button">취소</button>
                    </div>
                </section>
            </form>
            <!-- form 끝 -->

        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>