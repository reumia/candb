<h5>
    개인정보 취급방침
</h5>
<p>(주)씨엔비코퍼레이션(www.candb.co.kr) 는 귀하의 정보를 소중하게 생각합니다.
    <br>
    귀하의 정보는 개인의 소중한 자산인 동시에 (주)씨엔비코퍼레이션(www.candb.co.kr)의
    중요한 자료가 됩니다. 그러므로 저희 (주)씨엔비코퍼레이션 (www.candb.co.kr)는
    운영상의 모든 과정에서 귀하의 개인정보를 보호하는데 최선의 노력을 다할 것을 약속드립니다.</p>
<p>이에 (주)씨엔비코퍼레이션(www.candb.co.kr)은 다음과 같은 개인 보호 정책을 고지 합니다.
    (주)씨엔비코퍼레이션(www.candb.co.kr)은 개인정보보호법 제30조, 정보통신망이용촉진 및 정보보호에 관한 법률
    제27조 2에 따라 고객의 개인정보 보호 및 권익을 보호하고 개인정보와 관련한 고객의 고충을 원할하게 처리할 수 있도록
    다음과 같은 처리방침을 두고 있습니다.
    개인정보보호법에 의거 하여 개인정보 정책 변경 시는 (주)씨엔비코퍼레이션(www.candb.co.kr) 사이트를 통하여 고지 합니다.

</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td class="pd-0-10-5-15">1. 개인정보의 수집 범위</td>
    </tr>
    <tr>
        <td class="pd-0-10-20-30">㈜씨엔비코퍼레이션의 회원제 서비스를 이용하시고자
            할 경우 개인회원가입 시 다음의 정보를 입력해주셔야 합니다.<br>
            단, 안과 회원은 오프라인에서 관리합니다.<br> <br> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody><tr valign="top">
                    <td width="20" align="center">-</td>
                    <td class="pd-bottom-8"> 수집방법 : 홈페이지내 회원 가입을 통한 방법
                        / 홈페이지내 Contact Us를 통한 방법</td>
                </tr>
                <tr valign="top">
                    <td width="20" align="center">-</td>
                    <td class="pd-bottom-8"> 필수항목 : 희망 ID, 비밀번호,
                        이름, 전화번호, 휴대폰 번호, 주소, 메일 주소, 뉴스레터
                        수신여부</td>
                </tr>
                <tr valign="top">
                    <td align="center">-</td>
                    <td class="pd-bottom-8">선택항목 : 방문경로, 렌즈착용여부,
                        구입지역, 구입처, 병력/신상정보</td>
                </tr>
                </tbody></table></td>
    </tr>
    </tbody></table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td class="pd-0-10-5-15">2. 개인정보의 수집목적 및 이용목적</td>
    </tr>
    <tr>
        <td class="pd-0-10-20-30">㈜씨엔비코퍼레이션에 제공해주신 귀하의 정보는
            일차적으로는 회원서비스 이용 구분이나 적립금 및 쿠폰이용 등에 필요한 정보로 이용되며
            이차적으로는 통계분석을 통한 마케팅 자료로서 보다 나은 서비스 지원에 이용됩니다<br>
            <br> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody><tr valign="top">
                    <td width="20" align="center">-</td>
                    <td class="pd-bottom-8"> 이름, 아이디,
                        비밀번호 : 회원제 서비스 이용에 따른 본인 식별 절차에 이용</td>
                </tr>
                <tr valign="top">
                    <td align="center">-</td>
                    <td class="pd-bottom-8"> E-mail주소, 전화번호, 휴대폰
                        번호, 뉴스레터 수신여부 : 고객문의 사항 전달, 본인 의사 확인, 불만
                        처리 등 원활한 의사소통 경로의 확보, 최신 정보의 소개 및 안내</td>
                </tr>
                <tr valign="top">
                    <td align="center">-</td>
                    <td class="pd-bottom-8"> 방문경로, 결혼여부, 자녀수,
                        자녀생년월일, 관심 도서 : 고객통계 및 마케팅 자료</td>
                </tr>
                </tbody></table></td>
    </tr>
    </tbody></table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td class="pd-0-10-5-15">3. 개인정보의 제 3자 제공 및 공유</td>
    </tr>
    <tr>
        <td class="pd-0-10-20-30">㈜씨엔비코퍼레이션은 어떠한 경우에도 제2조에
            고지한 수집목적 및 이용목적의 범위를 초과하여 귀하의 개인정보를 이용하지 않습니다.
            <br> <br> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody><tr>
                    <td class="pd-0-10-5-15"> 1) 제휴관계 :</td>
                </tr>
                <tr>
                    <td class="pd-0-10-20-30">제3의 회사와 제휴관계를 맺을
                        경우 반드시 사전에 제휴사명과 제휴목적, 제공되는 서비스의 내용, 공유하는
                        개인정보의 범위, 이용목적, 제휴기간 등에 대해 상세하게 고지할 것이며
                        반드시 이용자의 적극적인 동의(개인정보의 제 3자 제공 및 공유에 대한
                        의사를 직접 밝힘)에 의해서만 정보를 제공하거나 공유합니다. 제휴관계에
                        변화가 있거나 제휴관계가 종결될 때도 같은 절차에 의하여 고지하거나 동의를
                        구합니다. </td>
                </tr>
                <tr>
                    <td class="pd-0-10-5-15">2) 위탁 처리 : </td>
                </tr>
                <tr>
                    <td class="pd-0-10-20-30">원활한 업무 처리를 위해 이용자의
                        개인정보를 위탁 처리할 경우 반드시 사전에 위탁처리 업체명과 위탁 처리되는
                        개인정보의 범위, 업무위탁 목적, 위탁 처리되는 과정, 위탁관계 유지기간
                        등에 대해 상세하게 고지합니다.</td>
                </tr>
                <tr>
                    <td class="pd-0-10-5-15">3) 매각. 인수합병 등 : </td>
                </tr>
                <tr>
                    <td class="pd-0-10-20-30">서비스제공자의 권리와 의무가
                        완전 승계. 이전되는 경우 반드시 사전에 정당한 사유와 절차에 대해 상세하게
                        고지할 것이며 이용자의 개인정보에 대한 동의 철회의 선택권을 부여합니다.
                    </td>
                </tr>
                <tr>
                    <td class="pd-0-10-5-15">4) 고지 및 동의방법 : </td>
                </tr>
                <tr>
                    <td class="pd-0-10-20-30">사이트의 공지사항을 통해 최소
                        15일 이전부터 고지함과 동시에 E-mail 등을 이용하여 1회 이상 개별적으로
                        고지하고 3)에 대해서는 반드시 적극적인 동의 방법에 의해서만 절차를 진행합니다.
                    </td>
                </tr>
                <tr>
                    <td class="pd-0-10-5-15">5) 다음은 예외로 합니다. </td>
                </tr>
                <tr>
                    <td class="pd-0-10-20-30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr valign="top">
                                <td width="20" align="center">-</td>
                                <td class="pd-bottom-8"> 관계법령에 의하여 수사상의
                                    목적으로 관계기관으로부터의 요구가 있을 경우 </td>
                            </tr>
                            <tr valign="top">
                                <td align="center">-</td>
                                <td class="pd-bottom-8"> 통계작성. 학술연구나
                                    시장조사를 위하여 특정 개인을 식별할 수 없는 형태로 광고주.
                                    협력사나 연구단체 등에 제공하는 경우 </td>
                            </tr>
                            <tr valign="top">
                                <td align="center">-</td>
                                <td class="pd-bottom-8"> 기타 관계법령에서 정한
                                    절차에 따른 요청이 있는 경우 </td>
                            </tr>
                            <tr valign="top">
                                <td align="center">-</td>
                                <td class="pd-bottom-8">그러나 예외 사항에서도
                                    관계법령에 의하거나 수사기관의 요청에 의해 정보를 제공한 경우에는
                                    이를 당사자에게 고지하는 것을 원칙으로 운영하고 있습니다. 그러나
                                    법률상의 근거에 의해 부득이하게 고지를 하지 못할 수도 있습니다.
                                    본래의 수집목적 및 이용목적에 반하여 무분별하게 정보가 제공되지
                                    않도록 최대한 노력하겠습니다.</td>
                            </tr>
                            </tbody></table></td>
                </tr>
                </tbody></table></td>
    </tr>
    </tbody></table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td class="pd-0-10-5-15">4. 개인정보의 이용기간 및 보유기간</td>
    </tr>
    <tr>
        <td class="pd-0-10-20-30">귀하의 개인정보는 이용자가 탈퇴의사를 밝혀
            탈퇴 처리될 때, 기타 이유에 의해 강제 탈퇴 처리될 때의 경우, 수집목적 및 이용목적이
            완수된 시점과 동시에 파기됨을 원칙으로 합니다.<br>
            단, 다음은 예외로 합니다<br> <br> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody><tr valign="top">
                    <td width="20" align="center">-</td>
                    <td class="pd-bottom-8"> 소송이나 분쟁에 있어 근거자료로
                        보존할 필요성이 있는 경우 기타 개별적으로 이용자의 동의를 받은 경우 그러나
                        이 경우에도 정당한 근거와 함께 사전에 개인정보가 보유되는 기간을 정확하게
                        고지하고 이후 완전 파기합니다.</td>
                </tr>
                <tr valign="top">
                    <td align="center">-</td>
                    <td class="pd-bottom-8"> 회원탈퇴 후 미결제 금액이 있을
                        경우에는 탈퇴에 대해 제한이 있을 수 있습니다.</td>
                </tr>
                </tbody></table></td>
    </tr>
    </tbody></table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td class="pd-0-10-5-15">5. 개인정보의 열람, 수정 및 삭제</td>
    </tr>
    <tr>
        <td class="pd-0-10-20-30">귀하가 제공한 개인정보의 열람, 수정 및
            삭제에 대한 접근은 자유롭습니다. 개인정보를 열람, 수정 및 삭제 하고자 할 경우에는
            사이트 상단의 'my page' 의 ‘회원정보수정’에서 직접 진행하실 수 있습니다.
            이 밖에 개인정보관리책임자에게 전화나, 서면, E-mail로 연락하시면 즉각 조치하겠습니다.
            <br>
            회원탈퇴를 원하실 경우, 'My Page' 의 회원탈퇴메뉴에서 탈퇴신청을 하실 수
            있습니다.<br>
            귀하가 제공한 개인정보에 변동사항이 있을 경우 즉각 수정하시어 최신의 정보를 입력해주시기
            바랍니다. <br>
            ㈜씨엔비코퍼레이션은 귀하가 개인정보의 오류에 대한 정정을 요청한 경우, 정정을 완료하기
            전까지 당해 개인정보를 이용 및 제공을 하지 않습니다. <br>
            귀하가 개인정보의 삭제를 요구하는 즉시 ㈜씨엔비코퍼레이션이 보유하고 있던 귀하의 모든
            데이터는 영구히 재생할 수 없는 형태로 완전 파기 됩니다. </td>
    </tr>
    </tbody></table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td class="pd-0-10-5-15">6. 쿠키의 운용</td>
    </tr>
    <tr>
        <td class="pd-0-10-20-30"><p>쿠키란 웹사이트가 사용자의 하드디스크(hard
                disk)에 전송하는 특별한 텍스트 파일(text file)을 말합니다. 웹사이트의
                서버(server)에서만 읽어드릴 수 있는 형태로 전송되며 개인이 사용하는 브라우저(browser)의
                디렉토리(directory) 하위에 저장됩니다</p>
            <p>쿠키는 웹사이트가 사용자에 관하여 무엇인가를 기억할 수 있도록 하기 위해 만들어졌습니다.
                즉, 이용자의 편의를 돕고 개별 맞춤 서비스를 제공하는 등 웹사이트와 방문자 간의
                원활한 의사소통에 이용되며, 쿠키를 통해 수집된 정보만으로는 개인을 식별할 수
                없습니다</p>
            <p>귀하의 웹 브라우저 상단의 도구/ 인터넷 옵션 탭(option tab)에서 모든
                쿠키의 허용, 동의를 통한 쿠키의 허용, 모든 쿠키의 차단을 스스로 결정하실 수
                있습니다</p></td>
    </tr>
    </tbody></table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td class="pd-0-10-5-15">7. 개인정보 보호를 위한 기술 및 관리적
            대책</td>
    </tr>
    <tr>
        <td class="pd-0-10-20-30">㈜씨엔비코퍼레이션은 회원 개인정보의 상실,
            유출, 변조, 훼손 등으로부터 보호하기 위하여 다음과 같은 대책을 마련하고 있습니다.
            <br> <br> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody><tr>
                    <td class="pd-0-10-5-15"> 1) 기술적 대책</td>
                </tr>
                <tr>
                    <td class="pd-0-10-20-30">귀하의 개인정보는 입력하시는
                        비밀번호에 의해 보호됩니다. 파일 및 각종 데이터는 암호화나 파일 잠금
                        기능(Lock)을 통해 전송되며 중요한 데이터는 별도의 보안 장치를 통해
                        보호되고 있습니다.</td>
                </tr>
                <tr>
                    <td class="pd-0-10-5-15">2) 관리적 대책</td>
                </tr>
                <tr>
                    <td class="pd-0-10-20-30"><p>개인정보관리책임자와 최소한의
                            인원으로 구성된 별도의 부서를 마련하여 개인정보에 대한 접근권한을 제한하고
                            있습니다. 개인정보 관련 취급자의 업무 인수인계는 보안이 유지된 상태에서
                            철저하게 이뤄지고 있으며 입사 및 퇴사 후 개인정보 사고에 대한 책임은
                            명확화하고 있습니다. 개인정보와 일반 데이터를 혼합하여 보관하지 않고
                            별도의 서버를 통해 분리하여 보관하고 있습니다.</p>
                        <p>㈜씨엔비코퍼레이션은 귀하의 개인정보를 보호하기 위하여 최선의 노력을
                            다하고 있지만 인터넷상의 데이터 전송은 완벽한 안전을 보장받을 수 없으며
                            이러한 위험은 (주)씨엔비코퍼레이션과 귀하가 함께 부담해야 합니다<br>
                            (주)씨엔비코퍼레이션는 이용자 개인의 실수나 기본적인 인터넷의 위험성
                            때문에 일어나는 일들에 대해 책임을 지지 않습니다. 회원 개개인이 본인의
                            개인정보를 보호하기 위해서 자신의 ID 와 비밀번호를 적절하게 관리하고
                            여기에 대한 책임을 져야 합니다. <br>
                            그 외 내부 관리자의 실수나 기술관리 상의 사고로 인해 개인정보의 상실,
                            유출, 변조, 훼손이 유발될 경우 ㈜씨엔비코퍼레이션은 즉각 귀하께 사실을
                            알리고 적절한 대책과 보상을 강구할 것입니다.</p></td>
                </tr>
                </tbody></table></td>
    </tr>
    </tbody></table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td class="pd-0-10-5-15">8. 링크</td>
    </tr>
    <tr>
        <td class="pd-0-10-20-30">㈜씨엔비코페이션의 페이지는 다양한 배너와
            링크(link)를 포함하고 있습니다. 많은 경우 이 타 사이트의 페이지와 연결되어
            있으며 이는 광고주와의 계약관계에 의하거나 제공받은 컨텐츠의 출처를 밝히기 위한 조치입니다<br>
            ㈜씨엔비코퍼레이션이 포함하고 있는 링크를 클릭(click)하여 타 사이트(site)의
            페이지로 옮겨갈 경우 해당 사이트의 개인정보보호정책은 ㈜씨엔비코퍼레이션과 무관하므로
            새로 방문한 사이트의 정책을 검토해 보시기 바랍니다.</td>
    </tr>
    </tbody></table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td class="pd-0-10-5-15">9. 게시물</td>
    </tr>
    <tr>
        <td class="pd-0-10-20-30">㈜씨엔비코페이션은 귀하의 게시물을 소중하게
            생각하며 변조, 훼손, 삭제되지 않도록 최선을 다하여 보호합니다. 그러나 스팸(spam)성
            게시물, 타인을 비방할 목적으로 허위 사실을 유포하여 타인의 명예를 훼손하는 글 동의
            없는 타인의 신상공개, (주)씨엔비코퍼레이션의 저작권, 제3자의 저작권 등 기타 권리를
            침해하는 내용, 기타 게시판 주제와 다른 내용의 게시물 등 (주)씨엔비코퍼레이션은
            바람직한 게시판 문화를 활성화하기 위하여 동의 없는 타인의 신상공개 시 특정 부분을
            삭제하거나 기호 등으로 수정하여 게시할 수 있습니다. 그러나 근본적으로 게시물에 관련된
            제반 권리와 책임은 작성자 개인에게 있습니다. 또 게시물을 통해 자발적으로 공개된
            정보는 보호받기 어려우므로 정보 공개 전에 심사숙고 하시기 바랍니다.</td>
    </tr>
    </tbody></table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td class="pd-0-10-5-15">10. 아동의 개인정보 보호</td>
    </tr>
    <tr>
        <td class="pd-0-10-20-30">(주)씨엔비코퍼레이션은 온라인 결제의 안전성
            및 보호를 위하여 만14세 미만 아동의 회원가입을 허용하지 않습니다.</td>
    </tr>
    </tbody></table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td class="pd-0-10-5-15">11. 비회원의 개인정보 보호</td>
    </tr>
    <tr>
        <td class="pd-0-10-20-30">(주)씨엔비코퍼레이션은 회원 가입을 하지
            않아도 컨텐츠를 자유롭게 이용할 수 있습니다. 그러나 비회원은 제품구매와 자료다운로드,
            이벤트 참여 등에 제한을 받을 수 있습니다.</td>
    </tr>
    </tbody></table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td class="pd-0-10-5-15">12. 이용자의 권리와 의무</td>
    </tr>
    <tr>
        <td class="pd-0-10-20-30">귀하의 개인정보를 최신의 상태로 정확하게
            입력하여 불의의 사고를 예방해 주시기 바랍니다. 이용자가 입력한 부정확한 정보로 인해
            발생하는 사고의 책임은 이용자 자신에게 있으며 타인 정보의 도용 등 허위정보를 입력할
            경우 회원자격이 상실될 수 있습니다.<br>
            귀하는 개인정보를 보호받을 권리와 함께 스스로를 보호하고 타인의 정보를 침해하지 않을
            의무도 가지고 있습니다. 비밀번호를 포함한 귀하의 개인정보가 유출되지 않도록 조심하시고
            게시물을 포함한 타인의 개인정보를 훼손하지 않도록 유의해 주십시오. <br>
            만약 이 같은 책임을 다하지 못하고 타인의 정보 및 존엄성을 훼손할 시에는 『정보통신망
            이용촉진 및 정보보호 등에 관한 법률』 등에 의해 처벌받을 수 있습니다.</td>
    </tr>
    </tbody></table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td class="pd-0-10-5-15">13. 관리자의 책임</td>
    </tr>
    <tr>
        <td class="pd-0-10-20-30"><p>(주)씨엔비코퍼레이션은 귀하가 좋은
                정보를 안전하게 이용할 수 있도록 최선을 다하고 있습니다. 개인정보를 보호하는데
                있어 귀하께 고지한 사항들에 반하는 사고가 발생할 시에 (주)씨엔비코퍼레이션이
                모든 책임을 집니다. 그러나 기술적인 보완조치를 했음에도 불구하고, 해킹 등 기본적인
                네트워크상의 위험성에 의해 발생하는 예기치 못한 사고로 인한 정보의 훼손 및 방문자가
                작성한 게시물에 의한 각종 분쟁에 관해서는 책임이 없습니다.</p>
            <p>귀하의 개인정보를 취급하는 책임자는 다음과 같으며 개인정보 관련 문의사항에 신속하고
                성실하게 답변해드리고 있습니다</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody><tr>
                    <td class="pd-0-10-20-30">개인정보 관리책임자 개인정보
                        관리담당자 <br>
                        - 성명: 임인화 <br>
                        - 전화번호: 02-561-9972<br>
                        - e-Mail: ih21c@candb.co.kr</td>
                </tr>
                </tbody></table></td>
    </tr>
    </tbody></table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td class="pd-0-10-5-15">14. 의견수렴 및 불만처리</td>
    </tr>
    <tr>
        <td class="pd-0-10-20-30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody><tr valign="top">
                    <td width="20" align="center">1.</td>
                    <td class="pd-bottom-8"> (주)씨엔비코퍼레이션은 귀하의
                        의견을 소중하게 생각하며, 귀하는 의문사항으로부터 언제나 성실한 답변을
                        받을 권리가 있습니다. </td>
                </tr>
                <tr valign="top">
                    <td align="center">2.</td>
                    <td class="pd-bottom-8"> (주)씨엔비코퍼레이션은 귀하와의
                        원활환 의사소통을 위해 고객지원센터를 운영하고 있습니다. 고객지원센터의
                        연락처는 다음과 같습니다. <br> <br> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr valign="top">
                                <td width="20" align="center">&nbsp;</td>
                                <td class="pd-bottom-8">  【 (주)씨엔비코퍼레이션 고객지원센터】
                                </td>
                            </tr>
                            <tr valign="top">
                                <td align="center">-</td>
                                <td class="pd-bottom-8"> 전자우편 : ih21c@candb.co.kr</td>
                            </tr>
                            <tr valign="top">
                                <td align="center">-</td>
                                <td class="pd-bottom-8"> 전화번호 : 02-561-9972</td>
                            </tr>
                            <tr valign="top">
                                <td align="center">-</td>
                                <td class="pd-bottom-8">팩스번호 : 02-567-7971</td>
                            </tr>
                            <tr valign="top">
                                <td align="center">-</td>
                                <td class="pd-bottom-8">주 소 : 서울시 강남구 테헤란로 623 삼성빌딩 9층</td>
                            </tr>
                            </tbody></table></td>
                </tr>
                <tr valign="top">
                    <td align="center">3.</td>
                    <td class="pd-bottom-8"> 전화상담은 평일 오전 9:00
                        ~ 오후 6:00에만 가능합니다. 단, 주말과 공휴일에는 전화상담을 진행하지
                        않습니다. 온라인상담을 이용해주세요.</td>
                </tr>
                <tr valign="top">
                    <td align="center">4.</td>
                    <td class="pd-bottom-8">전자우편이나 게시판을 이용한 상담은
                        접수 후 24시간 내에 성실하게 답변 드리겠습니다. 다만, 근무시간 이후
                        또는 주말 및 공휴일에는 익일 처리하는 것을 원칙으로 합니다.</td>
                </tr>
                <tr valign="top">
                    <td align="center">5.</td>
                    <td class="pd-bottom-8">기타 개인정보에 관한 상담이 필요한
                        경우에는 개인정보침해신고센터, 정보보호마크 인증위원회, 대검찰청 인터넷범죄수사센터,
                        경찰청 사이버범죄수사대, 통신위원회 등으로 문의하실 수 있습니다.<br>
                        <br> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr valign="top">
                                <td width="20" align="center">&nbsp;</td>
                                <td class="pd-bottom-8"> * 개인정보침해신고센터
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody><tr valign="top">
                                            <td width="20" align="center">-</td>
                                            <td class="pd-bottom-8"> 전화 :
                                                1336 </td>
                                        </tr>
                                        <tr valign="top">
                                            <td align="center">-</td>
                                            <td class="pd-bottom-8"> URL :
                                                http://www.cyberprivacy.or.kr
                                            </td>
                                        </tr>
                                        </tbody></table></td>
                            </tr>
                            <tr valign="top">
                                <td align="center">&nbsp;</td>
                                <td class="pd-bottom-8"> * 정보보호마크 인증위원회
                                    <br> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody><tr valign="top">
                                            <td width="20" align="center">-</td>
                                            <td class="pd-bottom-8"> 전화 :
                                                02-580-0533 </td>
                                        </tr>
                                        <tr valign="top">
                                            <td align="center">-</td>
                                            <td class="pd-bottom-8"> URL :
                                                http://www.privacymark.or.kr
                                            </td>
                                        </tr>
                                        </tbody></table></td>
                            </tr>
                            <tr valign="top">
                                <td align="center">&nbsp;</td>
                                <td class="pd-bottom-8"> * 대검찰청 인터넷범죄수사센터
                                    <br> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody><tr valign="top">
                                            <td width="20" align="center">-</td>
                                            <td class="pd-bottom-8"> 전화 :
                                                02-3480-3600 </td>
                                        </tr>
                                        <tr valign="top">
                                            <td align="center">-</td>
                                            <td class="pd-bottom-8"> URL :
                                                http://icic.sppo.go.kr </td>
                                        </tr>
                                        </tbody></table></td>
                            </tr>
                            <tr valign="top">
                                <td align="center">&nbsp;</td>
                                <td class="pd-bottom-8">* 경찰청 사이버범죄수사대<br>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody><tr valign="top">
                                            <td width="20" align="center">-</td>
                                            <td class="pd-bottom-8"> URL :
                                                http://www.police.go.kr/cybercenter
                                            </td>
                                        </tr>
                                        </tbody></table></td>
                            </tr>
                            <tr valign="top">
                                <td align="center">&nbsp;</td>
                                <td class="pd-bottom-8">* 통신위원회 <br>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody><tr valign="top">
                                            <td width="20" align="center">-</td>
                                            <td class="pd-bottom-8"> 전화 :
                                                02-750-1788 </td>
                                        </tr>
                                        <tr valign="top">
                                            <td align="center">-</td>
                                            <td class="pd-bottom-8"> URL :
                                                http://www.kcc.go.kr </td>
                                        </tr>
                                        </tbody></table></td>
                            </tr>
                            </tbody></table></td>
                </tr>
                </tbody></table></td>
    </tr>
    </tbody></table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td class="pd-0-10-5-15">15. 고지의 의무</td>
    </tr>
    <tr>
        <td class="pd-0-10-20-30">현 개인정보보호정책은 2005년 9월 1일에
            제정되었으며 정부의 정책 또는 보안기술의 변경에 따라 내용의 추가. 삭제 및 수정이
            있을 시에는 개정 최소 30일 전부터 사이트의 '공지사항'을 통해 고지합니다.</td>
    </tr>
    </tbody></table>
