<?php
$theme = 'other';
$title = '이용약관';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <section class="content-breadcrumb">
            <?=$title;?>
        </section>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    <?=$title;?>
                </h2>
                <div class="scroll-box">
                    <?php
                    include_once 'insurance_usage_txt.php';
                    ?>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>