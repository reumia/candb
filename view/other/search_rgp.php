<?php
$theme = 'other';
$title = 'RGP 안과 찾기';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <section class="content-breadcrumb">
            <?=$title;?>
        </section>
        <section class="content-body clearfix">

            <section class="content-section">
                <h2>
                    RGP <br class="only-pc"/>
                    안과 찾기
                </h2>
                <h3 class="color-default">지도를 클릭하시거나, 원하는 지역을 검색하시면 당사의 <span class="color-theme">RGP렌즈 취급 안과</span>를 알려드립니다.</h3>
                <hr/>
                <!--form 시작-->
                <form action="">
                    <div class="row">
                        <div class="col col-xs-7">
                            <div class="map-canvas">
                                <img src="../../static/img/others/findhospital_map_00.png" alt="" usemap="#Map" />
                                <map name="Map" id="Map">
                                    <area alt="경기도" title="경기도" href="#" shape="rect" coords="0,21,51,46" data-no="01"/>
                                    <area alt="인천" title="인천" href="#" shape="rect" coords="0,49,32,69" data-no="02"/>
                                    <area alt="서울" title="서울" href="#" shape="rect" coords="0,74,30,88" data-no="03"/>
                                    <area alt="충청북도" title="충청북도" href="#" shape="rect" coords="0,119,60,136" data-no="04"/>
                                    <area alt="충청남도" title="충청남도" href="#" shape="rect" coords="0,147,62,164" data-no="05"/>
                                    <area alt="대전" title="대전" href="#" shape="rect" coords="0,172,32,189" data-no="12"/>
                                    <area alt="전라북도" title="전라북도" href="#" shape="rect" coords="0,212,57,231" data-no="13"/>
                                    <area alt="전라남도" title="전라남도" href="#" shape="rect" coords="0,241,58,258" data-no="14"/>
                                    <area alt="광주" title="광주" href="#" shape="rect" coords="0,264,29,283" data-no="15"/>
                                    <area alt="제주도" title="제주도" href="#" shape="rect" coords="113,345,160,366" data-no="16"/>
                                    <area alt="부산" title="부산" href="#" shape="rect" coords="293,281,324,301" data-no="07"/>
                                    <area alt="울산" title="울산" href="#" shape="rect" coords="289,256,323,274" data-no="11"/>
                                    <area alt="경상남도" title="경상남도" href="#" shape="rect" coords="292,231,352,248" data-no="17"/>
                                    <area alt="대구" title="대구" href="#" shape="rect" coords="291,161,322,181" data-no="10"/>
                                    <area alt="경상북도" title="경상북도" href="#" shape="rect" coords="292,129,354,148" data-no="09"/>
                                    <area alt="울릉도" title="울릉도" href="#" shape="rect" coords="289,49,338,67" data-no="08"/>
                                    <area alt="강원도" title="강원도" href="#" shape="rect" coords="292,25,337,41" data-no="06"/>
                                </map>
                            </div>
                        </div>
                        <div class="col col-xs-5">
                            <h3 class="color-default">지역검색</h3>
                            <table class="table-form">
                                <tbody>
                                <tr>
                                    <td>
                                        <select class="form-control form-m" name="" id="">
                                            <?php
                                            $data_arr = array('지역 선택', '경기도', '인천광역시', '대전광역시', '대구광역시', '광주광역시', '울산광역시', '서울', '부산광역시', '제주도', '강원도', '경상남도', '전라남도', '경상북도', '전라북도', '충청남도', '충청북도' );
                                            for( $i=0; $i<sizeof($data_arr); $i++ ){
                                                ?>
                                                <option value="<?=$i?>"><?=$data_arr[$i]?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select class="form-control form-m" name="" id="">
                                            <option value="">구 선택</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input class="form-control form-m" type="text" placeholder="지역명"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input class="form-control form-m" type="text" placeholder="안과명"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <button class="btn btn-default form-m" type="submit">검색</button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </form>
                <!--form 끝-->
            </section>

        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>