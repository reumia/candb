<?php
$theme = 'other';
$title = 'OK렌즈 전문 시술 클리닉 찾기';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <section class="content-breadcrumb">
            <?=$title;?>
        </section>
        <section class="content-body clearfix">

            <section class="content-section">
                <h2>
                    OK렌즈 전문<br class="only-pc"/>
                    시술 클리닉
                </h2>
                <h3 class="color-default">지역검색</h3>
                <!--form 시작-->
                <form action="">
                    <table class="table-form">
                        <tbody>
                        <tr>
                            <td>
                                <select class="form-control form-m" name="" id="">
                                    <?php
                                    $data_arr = array('지역 선택', '경기도', '인천광역시', '대전광역시', '대구광역시', '광주광역시', '울산광역시', '서울', '부산광역시', '제주도', '강원도', '경상남도', '전라남도', '경상북도', '전라북도', '충청남도', '충청북도' );
                                    for( $i=0; $i<sizeof($data_arr); $i++ ){
                                        ?>
                                        <option value="<?=$i?>"><?=$data_arr[$i]?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <select class="form-control form-m" name="" id="">
                                    <option value="">구 선택</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input class="form-control form-m" type="text" placeholder="지역명"/>
                                <input class="form-control form-m" type="text" placeholder="안과명"/>
                                <button class="btn btn-default" type="submit">검색</button>
                            </td>
                        </tr>
                    </table>
                </form>
                <!--form 끝-->
                <br/>
                <!--게시판 시작-->
                <div class="board">
                    <table class="board-table">
                        <thead>
                        <tr>
                            <th class="color-theme">안과명</th>
                            <th class="color-theme">소재지 (주소)</th>
                            <th class="color-theme">대표자</th>
                            <th class="color-theme">연락처</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        for ( $i=0; $i<10; $i++ ){
                            ?>
                            <tr>
                                <td><a href="">EOS안과</a></td>
                                <td>서울시 강남구 삼성동 141-35 경남빌딩 1502</td>
                                <td>문준웅</td>
                                <td>02-345-0052</td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <ul class="board-pagination">
                        <li><a href="#">&lt;</a></li>
                        <li><a href="#" class="active">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">&gt;</a></li>
                    </ul>
                </div>
                <hr/>
                <div class="row">
                    <div class="col col-xs-6">
                        &nbsp;
                    </div>
                    <div class="col col-xs-6">
                        <div class="board-search text-right">
                            <select class="form-control" name="" id=""></select>
                            <input class="form-control" type="text"/>
                            <button class="btn btn-default">검색</button>
                        </div>
                    </div>
                </div>
                <!--게시판 끝-->
            </section>

        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>