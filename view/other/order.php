<?php
$theme = 'other order';
$title = '수주장';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <section class="content-breadcrumb">
            <?=$title;?>
        </section>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    <?=$title;?>
                </h2>
                <div class="btn-wrap">
                    <div class="row">
                        <div class="col col-xs-6">
                            <a class="btn btn-success" href="order_write.php">등록하기</a>
                            <a class="btn btn-default" href="#">엑셀받기</a>
                        </div>
                        <div class="col col-xs-6">
                            <div class="text-right">
                                <input class="form-control datepicker" type="text"/>
                                ~
                                <input class="form-control datepicker" type="text"/>
                                <button class="btn btn-default" type="submit">조회</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-section">
                <div class="board">
                    <table class="board-table">
                        <thead>
                        <tr>
                            <th class="color-theme">No.</th>
                            <th class="color-theme">거래처명</th>
                            <th class="color-theme">품명</th>
                            <th class="color-theme">B.C</th>
                            <th class="color-theme">Power</th>
                            <th class="color-theme">DIA</th>
                            <th class="color-theme">수량</th>
                            <th class="color-theme">환자명</th>
                            <th class="color-theme">Color</th>
                            <th class="color-theme">구분</th>
                            <th class="color-theme">비고</th>
                            <th class="color-theme">처리유무</th>
                            <th class="color-theme">처리시간</th>
                            <th class="color-theme">입력시간</th>
                            <th class="color-theme">수정</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        for( $i = 10; $i > 0; $i-- ){
                            ?>
                            <tr>
                                <td><?=$i;?></td>
                                <td>테스트입니다</td>
                                <td>0000.00.00</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                <td><a href="#">수정</a></td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <ul class="board-pagination">
                        <li><a href="#">&lt;</a></li>
                        <li><a href="#" class="active">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">&gt;</a></li>
                    </ul>
                </div>
            </section>
            <section class="content-section">
                <div class="board">
                    <table class="board-table">
                        <colgroup>
                            <col style="width: 32px;"/>
                            <col style="width: 100px;"/>
                            <col span="7"/>
                            <col style="width: 140px;"/>
                        </colgroup>
                        <thead>
                        <tr>
                            <th class="color-theme">No.</th>
                            <th class="color-theme">품명</th>
                            <th class="color-theme">B.C</th>
                            <th class="color-theme">Power</th>
                            <th class="color-theme">DIA</th>
                            <th class="color-theme">수량</th>
                            <th class="color-theme">환자명</th>
                            <th class="color-theme">Color</th>
                            <th class="color-theme">구분</th>
                            <th class="color-theme">비고</th>
                            <th class="color-theme">확인</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><label for=""><input class="checkbox" type="checkbox"/><?=$i;?></label></td>
                                <td>
                                    <select class="form-control" name="" id="">
                                        <option value="0">선택</option>
                                    </select>
                                </td>
                                <td><input class="form-control" type="text" value="test테스트"/></td>
                                <td><input class="form-control" type="text"/></td>
                                <td><input class="form-control" type="text"/></td>
                                <td><input class="form-control" type="text"/></td>
                                <td><input class="form-control" type="text"/></td>
                                <td><input class="form-control" type="text"/></td>
                                <td><input class="form-control" type="text"/></td>
                                <td><input class="form-control" type="text"/></td>
                                <td><a href="#">확인</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>