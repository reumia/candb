<?php
$theme = 'other';
$title = '회원본인인증';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <section class="content-breadcrumb">
            <span class="inactive">회원가입 > </span><?=$title;?>
        </section>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    국내거주자
                </h2>
                <div class="row">
                    <div class="col col-xs-6">
                        <div class="btn-wrap">
                            <a class="btn btn-success btn-block btn-lg" href="join_personal.php">휴대폰으로 인증</a>
                        </div>
                        <p>
                            고객님 명의의 <span class="color-theme">휴대폰을 이용하여</span> 본인인증 과정을 거치게 됩니다.
                        </p>
                    </div>
                    <div class="col col-xs-6">
                        <div class="btn-wrap">
                            <a class="btn btn-success btn-block btn-lg" href="join_personal.php">아이핀으로 인증</a>
                        </div>
                        <h3>아이핀(I-PIN)이란?</h3>
                        <p>
                            인터넷 개인식별번호(Internet Personal Identification Number)
                            대면확인이 불가능한 인터넷상에서 주민등록번호를 대신하여 본인임을 확인
                            받을 수 있는 사이버 신원확인 번호입니다.
                        </p>
                    </div>
                </div>
            </section>
            <section class="content-section">
                <h2>
                    외국인 / <br class="only-pc"/>
                    해외거주자
                </h2>
                <div class="row">
                    <div class="col col-xs-6">
                        <h3 class="color-default">이메일 인증</h3>
                        <input class="form-control form-m" type="email" placeholder="candb@candb.co.kr"/>
                        <button class="btn btn-success">확인</button>
                    </div>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>