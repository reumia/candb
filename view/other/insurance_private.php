<?php
$theme = 'other';
$title = '개인정보취급방침';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <section class="content-breadcrumb">
            <?=$title;?>
        </section>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    개인정보 <br class="only-pc"/>취급방침
                </h2>
                <div class="scroll-box">
                    <?php
                    include_once 'insurance_private_txt.php'
                    ?>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>