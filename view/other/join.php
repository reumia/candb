<?php
$theme = 'other';
$title = '회원가입';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <section class="content-breadcrumb">
            <?=$title;?>
        </section>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    <?=$title;?>
                </h2>
                <h3>
                    C&amp;B에 오신 것을 환영합니다!<br/>
                    C&amp;B는 무료회원제로 운영되며, 회원은 개인회원과 안과회원으로 구분됩니다.
                </h3>
            </section>
            <section class="content-section">
                <div class="row">
                    <div class="col col-xs-6">
                        <h3>개인회원</h3>
                        <p>
                            개인회원은 회원가입 후 온라인 1:1상담과 쇼핑몰을 통한 제품구매등의 혜택을 누리실 수 있습니다.
                        </p>
                    </div>
                    <div class="col col-xs-6">
                        <h3>안과회원</h3>
                        <p>
                            안과회원은 온라인 수주와 제품담당자와의 직접상담, 문의,
                            관리를 받으실 수 있습니다. 단, 안과 회원으로 등록하시려면
                            거래처 등록이 되어 있어야만 합니다.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-xs-6">
                        <div class="btn-square">
                            <a href="join_check.php">
                                <img src="../../static/img/others/join_icon1.png" alt=""/>
                                개인 회원 가입
                            </a>
                        </div>
                    </div>
                    <div class="col col-xs-6">
                        <div class="btn-square">
                            <a href="join_company.php">
                                <img src="../../static/img/others/join_icon2.png" alt=""/>
                                안과 회원 가입
                            </a>
                        </div>
                        <p class="clearboth">
                            아직 거래처로 등록하지 않으신 업체에서는 <br class="only-pc"/> 관리담당자에게 문의해 주세요.
                        </p>
                        <table class="table table-none-width">
                            <tbody>
                            <tr>
                                <th>담당자</th>
                                <td>서혜진</td>
                            </tr>
                            <tr>
                                <th>전화</th>
                                <td>02-561-9972</td>
                            </tr>
                            <tr>
                                <th>이메일</th>
                                <td>ih21c@candb.co.kr</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>