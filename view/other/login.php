<?php
$theme = 'other';
$title = '로그인';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <section class="content-breadcrumb">
            <?=$title;?>
        </section>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    <?=$title;?>
                </h2>
                <h3>
                    C&B를 방문해 주셔서 감사합니다. <br/>
                    <small class="color-theme">회원 로그인을 하신 후 서비스를 이용하시면 좀 더 유용하게 즐기실 수 있습니다.</small>
                </h3>
            </section>
            <section class="content-section">
                <form action="">
                    <table class="table-form">
                        <tbody>
                        <tr>
                            <th>아이디</th>
                            <td><input class="form-control form-m" type="text"/></td>
                        </tr>
                        <tr>
                            <th>비밀번호</th>
                            <td><input class="form-control form-m" type="password"/></td>
                        </tr>
                        <tr>
                            <th>&nbsp;</th>
                            <td><button type="submit" class="btn btn-success btn-block">로그인</button></td>
                        </tr>
                        <tr>
                            <th>&nbsp;</th>
                            <td><a href="join.php" class="btn btn-default btn-block">회원가입</a></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </section>
            <section class="content-section">
                <p>
                    아직 회원이 아니시라구요? <br/>
                    회원가입으로 다음과 같은 혜택을 누려보세요!
                </p>
                <ul class="btn-wrap" style="margin-top: 0;">
                    <li class="btn-square disabled">
                        <a href="#">
                            <h1>1</h1>
                            무료 가입에 <br/>
                            상품 구매시 <br/>
                            쌓이는 적립금
                        </a>
                    </li>
                    <li class="btn-square disabled">
                        <a href="#">
                            <h1>2</h1>
                            전 제품 무료 배송
                        </a>
                    </li>
                </ul>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>