<?php
$theme = 'other';
$title = 'Site Map';
include_once '../inc/header.php';
?>
    <article class="site-content common-width">
        <section class="content-breadcrumb">
            <?=$title;?>
        </section>
        <section class="content-body clearfix">
            <section class="content-section">
                <h2>
                    <?=$title;?>
                </h2>
                <div class="row">
                    <div class="col col-xs-6">
                        <ul class="sitemap-box">
                            <li><h3><a href="../lens-intro">C&amp;B 렌즈소개</a></h3></li>
                            <li>
                                <a href="../lens-intro/rgp.php">RGP 렌즈</a>
                                <ul>
                                    <li><a href="../lens-intro/rgp_about.php">RGP 렌즈 더 알아보기</a></li>
                                    <li><a href="../lens-intro/rgp_supervision.php">Supervision II</a></li>
                                    <li><a href="../lens-intro/rgp_envision.php">Boston Envision</a></li>
                                    <li><a href="../lens-intro/rgp_thinsite.php">Thinsite</a></li>
                                    <li><a href="../lens-intro/rgp_achievement.php">Achievement</a></li>
                                    <li><a href="../lens-intro/rgp_supervisiont.php">Supervision T</a></li>
                                    <li><a href="../lens-intro/rgp_soclear.php">So Clear</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="../lens-intro/contex.php">Contex OK 렌즈</a>
                                <ul>
                                    <li><a href="../lens-intro/contex_about.php">각막 굴절 교정술이란?</a></li>
                                    <li><a href="../lens-intro/contex_qna.php">Contex OK 렌즈에 대한 질문과 답변</a></li>
                                </ul>
                            </li>
                            <li><a href="../lens-intro/kerasoft.php">Kerasoft IC</a></li>
                            <li><a href="../lens-intro/tip_management.php">렌즈 관리 방법</a></li>
                            <li><a href="../lens-intro/tip_usage.php">렌즈 사용 방법</a></li>
                            <li><a href="../lens-intro/tip_exercise.php">순목운동</a></li>
                        </ul>
                        <ul class="sitemap-box">
                            <li><h3><a href="../company-intro">회사 소개</a></h3></li>
                            <li><a href="../company-intro/intro_greetings.php">인사말</a></li>
                            <li><a href="../company-intro/intro_history.php">연혁</a></li>
                            <li><a href="../company-intro/intro_manufacturer.php">렌즈 제조사 소개</a></li>
                        </ul>
                        <ul class="sitemap-box">
                            <li><h3><a href="../mypage">마이 페이지</a></h3></li>
                            <li><a href="../mypage/asked.php">나의 상담내역</a></li>
                            <li><a href="../mypage/info.php">나의 정보관리</a></li>
                        </ul>
                    </div>
                    <div class="col col-xs-6">
                        <ul class="sitemap-box">
                            <li><h3><a href="../liquid">렌즈관리 용액</a></h3></li>
                            <li><a href="../liquid/cleaner.php">보스톤 세척액</a></li>
                            <li><a href="../liquid/soaking-solution.php">보스톤 보존액</a></li>
                            <li><a href="../liquid/multi-function.php">보스톤 다목적 용액</a></li>
                            <li><a href="../liquid/protein-removal.php">보스톤 단백질 제거제</a></li>
                        </ul>
                        <ul class="sitemap-box">
                            <li><h3><a href="../about-eyes">궁금한 우리눈</a></h3></li>
                            <li><a href="../about-eyes/eye_function.php">눈의 기능</a></li>
                            <li><a href="../about-eyes/eye_sight.php">시력에 대해서</a></li>
                            <li><a href="../about-eyes/eye_curve.php">굴절이상이란?</a></li>
                            <li><a href="../about-eyes/lens_keeping.php">렌즈 보관법</a></li>
                            <li><a href="../about-eyes/lens_makeup.php">안전한 화장법</a></li>
                            <li><a href="../about-eyes/lens_warning.php">주의해야할 안질환</a></li>
                            <li><a href="../about-eyes/lens_qna.php">궁금증과 오해</a></li>
                        </ul>
                        <ul class="sitemap-box">
                            <li><h3><a href="../cscenter">고객센터</a></h3></li>
                            <li><a href="../cscenter/notice.php">공지사항 &amp; 이벤트</a></li>
                            <li><a href="../cscenter/qna.php">고객상담</a></li>
                            <li><a href="../cscenter/faq.php">FAQ</a></li>
                        </ul>
                        <ul class="sitemap-box">
                            <li><h3><a href="../shop">쇼핑몰</a></h3></li>
                            <li><a href="../shop/list.php">제품 안내</a></li>
                            <li><a href="../shop/cart.php">장바구니</a></li>
                            <li><a href="../shop/history.php">구매내역 / 배송조회</a></li>
                            <li><a href="../shop/guide.php">쇼핑몰 이용 안내</a></li>
                        </ul>
                        <ul class="sitemap-box">
                            <li><h3><a href="#">기타</a></h3></li>
                            <li><a href="../other/sitemap.php">사이트맵</a></li>
                            <li><a href="../other/insurance_usage.php">이용약관</a></li>
                            <li><a href="../other/insurance_private.php">개인정보취급방침</a></li>
                            <li><a href="../other/search_rgp.php">RGP 안과 찾기</a></li>
                            <li><a href="../other/search_ok.php">OK렌즈 전문 시술 클리닉</a></li>
                        </ul>
                    </div>
                </div>
            </section>
        </section>
    </article>
<?php
include_once '../inc/footer.php';
?>