<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$title;?> | candb.co.kr</title>

    <link href="../../static/css/candb.css" rel="stylesheet">
    <link href="../../static/lib/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="site clearfix <?=$theme;?>">

<header class="site-header">
    <section class="site-top-wrap">
        <div class="site-top common-width clearfix">
            <div class="row">
                <div class="col col-xs-6">
                    <a href="../index/">
                        <img src="../../static/img/brand/brand_1st_logo.png" alt="" class="logo-img"/>
                    </a>
                </div>
                <div class="col col-xs-6 text-right only-pc">
                    <a href="../other/login.php" class="btn">로그인</a>
                    <a href="../other/join.php" class="btn">회원가입</a>
                </div>
            </div>
        </div>
    </section>
    <!--

    슬라이더 시작

        - 슬라이더는 메인페이지에서만 나타납니다.
        - 슬라이더의 사이즈는 고정되어 있습니다.
        - 배경이미지를 <li> 의 style attribute로 작성해주시면 됩니다.
        - <li> 안의 내용은 각 요소들이 일정한 간격으로 차례차례 배치됩니다.
    -->
    <section class="site-slider common-width">
        <ul>
            <li style="background-image: url(../../static/img/index/slider_2_bg.jpg); text-align: center;">
                <img src="../../static/img/index/slider_2_img.png" alt="" style="display: inline-block; margin: 40px 0;"/>
                <p style="color: #279FD9; margin-bottom: 40px;">
                    변함없는 성원에 힘입어 맞이한 창립 20주년, <br class="only-pc"/>
                    이를 맞아 새롭게 단정한 사이트와 <br class="only-pc"/>
                    씨엔비코퍼레이션이 추구하는 가치를 담은 영상을 선보입니다.
                </p>
                <div class="btn-wrap">
                    <a class="btn btn-default" href="https://youtu.be/CnYEqpEvEsQ" target="_blank">브랜드 영상보기</a>
                    <a class="btn btn-default" href="#" target="_blank">연혁 영상보기</a>
                </div>
            </li>
            <li style="background-image: url(../../static/img/index/slider_1_bg.jpg);">
                <img src="../../static/img/index/slider_1_logo.png" alt=""/>
                <p>
                    교정렌즈의 효시,<br/>
                    수면착용 시력교정렌즈
                </p>
                <img src="../../static/img/index/slider_1_icon.png" alt=""/>
                <a class="btn btn-default" href="../lens-intro/contex.php">더 알아보기</a>
            </li>
        </ul>
    </section>
    <!--슬라이더 끝-->
    <section class="site-navigation common-width">
        <ul class="clearfix">
            <li class="lens-intro">
                <a href="../lens-intro">
                    <img src="../../static/img/index/main_icon_1.png" alt=""/>
                    C&amp;B 렌즈소개
                </a>
            </li>
            <li class="liquid">
                <a href="../liquid">
                    <img src="../../static/img/index/main_icon_2.png" alt=""/>
                    렌즈관리용액
                </a>
            </li>
            <li class="company-intro">
                <a href="../company-intro">
                    <img src="../../static/img/index/main_icon_3.png" alt=""/>
                    회사 소개
                </a>
            </li>
            <li class="about-eyes">
                <a href="../about-eyes">
                    <img src="../../static/img/index/main_icon_4.png" alt=""/>
                    궁금한 우리눈
                </a>
            </li>
            <li class="shop only-pc">
                <a href="../shop">
                    <img src="../../static/img/index/main_icon_5.png" alt=""/>
                    쇼핑몰
                </a>
            </li>
            <li class="mypage only-pc">
                <a href="../mypage">
                    <img src="../../static/img/index/main_icon_6.png" alt=""/>
                    마이 페이지
                </a>
            </li>
            <li class="cscenter only-pc">
                <a href="../cscenter">
                    <img src="../../static/img/index/main_icon_7.png" alt=""/>
                    고객 센터
                </a>
            </li>
        </ul>
    </section>
</header>