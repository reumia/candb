<footer class="site-footer">
    <div class="common-width clearfix">
        <div class="row">
            <div class="col col-xs-3 only-pc">
                <img src="../../static/img/footer_logo.png" alt="" style="width: 100%;"/>
            </div>
            <div class="col col-xs-7">
                <dl class="site-info clearfix">
                    <dt>상호</dt>
                    <dd>(주)씨엔비코퍼레이션</dd>
                    <dt>주소</dt>
                    <dd>서울 특별시 강남구 테헤란로 623 삼성빌딩 9층</dd>
                    <dt>대표이사</dt>
                    <dd>황정주</dd>
                    <dt>사업자등록번호</dt>
                    <dd>120-81-49602</dd>
                    <dt>통신판매업신고</dt>
                    <dd>강남-2135호</dd>
                    <dt>개인정보관리책임자</dt>
                    <dd>임인화</dd>
                    <dt style="clear: both;">고객센터</dt>
                    <dd>Tel 02-561-9972 | Fax 02-567-7971</dd>
                </dl>
                <div class="site-copyright">
                    Copyright 2000-2014 C&amp;B co.Ltd. All right reserved.
                </div>
            </div>
            <div class="col col-xs-2 text-right only-pc">
                <ul class="">
                    <li><a href="../other/sitemap.php">사이트맵</a></li>
                    <li><a href="../other/insurance_usage.php">이용약관</a></li>
                    <li><a href="../other/insurance_private.php">개인정보취급방침</a></li>
                    <li><a href="../company-intro/intro_contact.php">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<script src="../../static/lib/jquery-1.11.2.min.js"></script>
<script src="../../static/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="../../static/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../../static/js/common.js"></script>
</body>
</html>