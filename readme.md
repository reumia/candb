# 안녕하세요. 퍼블리싱 담당자 조현석입니다.

개발간에 참고해주실 부분들 기록합니다. 참고해 주시고 문의할 내용이 있으시면 메일이나 전화 부탁드리겠습니다. 전화는 못받을 때도 많습니다. 메일 늘 확인하므로 되도록 메일로 연락주시는 것이 좋습니다.

- 전화 : 010.5021.6644
- 이메일 : reumia@gmail.com

--------------------------

# 참고사항

### 페이지별 테마세팅
- 각 페이지의 가장 상단에 해당 페이지의 색상을 결정하는 theme class 를 지정하는 php 변수가 있습니다.
- 해당 변수를 받아서 html 태그에 클래스로 삽입합니다.
- theme.css 에서 그 클래스를 받아 색상을 세팅합니다.

### 모바일관련
- 모바일에서는 고객센터, 쇼핑몰, 마이페이지, 회원기능 등을 이용하지 않는 것으로 CANDB 측과 얘기되었습니다.
- 프로그램이 들어가지 않는 단순한 소개페이지만이 모바일에서 접속할 수 있도록 제작하였습니다.
- 미디어쿼리 이용해서 특정 사이즈 이하에서 해당 링크를 제공하지 않는 것으로 처리하겠습니다.

### 게시판
- 게시판 화면 목록은 모두 제작해 두었습니다.
- 글읽기, 글쓰기 화면은 고객상담 게시판에 작성되어 있습니다. (view/cscenter/qna.php)

### 스크립트
- 쇼핑몰 > 제품안내 > 상세 > 썸네일 마우스 오버시 메인 이미지 교체
- 쇼핑몰 > 제품안내 > 상세 > 썸네일에 이미지 없을 시 반투명클래스 추가
- RGP 안과 찾기 > 지도 > 마우스 오버시에 이미지 교체

--------------------------

# 작업요망

### 프로그램 연동
- 회사소개 > Contact Us
    - view/company-intro/intro_contact.php
    - 메일링 설치 필요
- 궁금한 우리눈 : C&B가 알려드리는 눈 이야기
    - view/about-eyes/story.php
    - view/about-eyes/index.php 에는 최근글 두개 이미지와 함께 출력.
    - 기존의 *궁금한 우리눈 -> 뉴스 & 칼럼 게시판* 이라고 생각하면 됩니다.
- 고객센터 : 공지사항&이벤트, 고객상담, FAQ
- 쇼핑몰 : 제품안내, 장바구니, 구매내역/배송조회
- 마이페이지 : 나의 상담내역, 나의 정보관리
- 의사전용페이지 : 게시판 2개, 메일링 한개
- 기타 : 로그인, 회원가입, RGP안과찾기, OK렌즈전문 시술클리닉

### 각종 팝업
- 반품관련
    - 쇼핑몰 > 구매내역 > 주문번호
    - 상단에 위치한 버튼 세개
    - view/shop/history_detail.php
- 아이디 중복, 우편번호 검색
    - 회원가입, 개인정보 수정 등 몇 군데에 있음.
    - 개인회원가입 폼에 팝업버튼 제작해 두었음.
    - view/other/join_personal.php

### tab 클래스에 active 클래스 추가
- 고객센터, 쇼핑몰, 마이 페이지 에는 하위 카테고리로 접근하기 위한 tabs 모듈이 있습니다. 상황에 따라 tab 클래스에 active 클래스를 추가해 주셔야 합니다.
- 제가 js로 작업했다가 게시판 설치할 때에 또 작업하셔야할 부분이라 php로 작업부탁드리는 바입니다.

### site-navigation li 에 active 클래스 추가
- 각 카테고리 별로 해당 메뉴에 active 클래스 추가해주시면 됩니다.
   
--------------------------

# 잔여작업
- 회사소개 > 연혁 : 링크
- 메인페이지 > 슬라이드1 : 링크
