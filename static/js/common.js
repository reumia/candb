$(function(){

    // 쇼핑몰 디테일 화면에 필요한 함수들 실행
    thumb_init();
    $('.shop-detail-thumb li:not(".inactive")').mouseenter(hover_thumb);

    // datepicker 실행
    $( ".datepicker" ).datepicker({format: 'yyyy-mm-dd'});

    // 지도 UI 실행
    hover_map();

    // 메인페이지 슬라이더 실행
    set_slider();

});

/*
* 쇼핑몰 > 제품안내
* 썸네일 이미지 없을 시에 inactive 함수 추가
* */
function thumb_init(){
    $('.shop-detail-thumb li').each(function(){
        if( $(this).find('img').length == 0 ){
            $(this).addClass('inactive');
        }
    });
}

/*
* 쇼핑몰 > 제품안내
* 썸네일 마우스 오버 시에 메인 썸네일 교체
* */
function hover_thumb(){
    var img_src = $(this).find('img').attr('src');
    $('.media-object').attr('src', img_src);
}

/*
* 기타 > RGP 안과 찾기
* 지도 마우스 오버 동작
* */
function hover_map(){

    var map_no;
    $('.map-canvas area').each(function () {
        $(this)
            .mouseenter(function(){
                map_no = $(this).data('no');
                $('.map-canvas img').attr('src', '../../static/img/others/findhospital_map_'+map_no+'.png');
            })
            .mouseout(function(){
                $('.map-canvas img').attr('src', '../../static/img/others/findhospital_map_00.png');
            });
    });

}

/*
* 새창 열기
* */
function open_pop(target, width, height){

    var src, window_w, window_h;
    src = target;
    window_w = width || 500;
    window_h = height || 350;

    window.open(
        src,
        "네이버새창",
        "width="+window_w+", height="+window_h+", toolbar=no, menubar=no, scrollbars=no, resizable=yes"
    );
}

/*
* 메인 슬라이더
* */
function set_slider(){

    // 변수 세팅
    var $canvas;
    $canvas = $('.site-slider');

    if( $canvas.is(":visible") ){

        // 변수 세팅
        var $indicators, $inner, $item, $slides, no_of_slides, item_contents;
        $slides = $('.site-slider ul li');
        no_of_slides = $slides.length;

        // canvas 초기화
        $canvas
            .addClass('carousel')
            .addClass('slide')
            .attr({
                'data-ride' : 'carousel',
                'data-interval' : 3000, // 슬라이드 속도 설정. 1000 = 1초
                'id' : 'slider'
            });

        // indicators 삽입
        $indicators = $('<ol>', { 'class' : 'carousel-indicators' });
        for ( var i = 0; i < no_of_slides; i++ ){
            $('<li>', { 'data-target' : '#slider', 'data-slide-to' : i }).appendTo($indicators);
        }
        $indicators.appendTo($canvas);
        $indicators.find('li:first').addClass('active');

        // items 삽입
        $inner = $('<div>', { 'class' : 'carousel-inner', 'role' : 'list-box' });
        for ( var i = 0; i < no_of_slides; i++ ){
            item_contents = $slides.eq(i).html();
            $item = $('<div>', {
                'class' : 'item',
                'html' : item_contents,
                'style' : $slides.eq(i).attr('style')
            });
            if( i == 0 ){
                $item.addClass('active');
            }
            $item.appendTo($inner);
        }
        $inner.appendTo($canvas);

        // 기존 데이터 제거
        $canvas.find('ul').remove();

    }

}

